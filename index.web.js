import {AppRegistry} from 'react-native'

import App from './app/index'

let app
if (process.env.CONFIG === 'fb') {
  app = require('./app2')
} else {
  app = require('./app')
}
const { displayName } = app


AppRegistry.registerComponent(displayName, () => App);
AppRegistry.runApplication(displayName, {rootTag: document.getElementById('react-app')});

AppRegistry.registerComponent('App', () => renderApp);

if (module.hot) {
  // $FlowFixMe
  module.hot.accept()

  // App registration and rendering
  AppRegistry.registerComponent('App', () => renderApp);
}