import i18n from 'i18next'
import { reactI18nextModule } from 'react-i18next'
import locale2 from 'locale2'

import resources from './resources'

let locale = process.env.NODE_ENV === 'production' ? locale2 : 'en'

const getLocale = () => process.env.CONFIG === 'fb' ? locale : 'en'

const languageDetector = {
  type: 'languageDetector',
  async: true, // flags below detection to be async
  detect: callback => {
    callback(getLocale().replace('_', '-'))
  },
  init: () => {},
  cacheUserLanguage: () => {}
}

i18n
  .use(languageDetector)
  .use(reactI18nextModule)
  .init({
    fallbackLng: 'en',

    resources,

    // have a common namespace used around the full app
    ns: ['common'],
    defaultNS: 'common',

    debug: true,

    // cache: {
    //   enabled: true
    // },

    interpolation: {
      escapeValue: false, // not needed for react as it does escape per default to prevent xss!
    }
  })


export default i18n