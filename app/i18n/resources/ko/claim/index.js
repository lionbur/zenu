import body from './body.txt'

export default {
  body,
  button: '큰! 시작하자!',
  wait: '메뉴 만들기 ...',
  missing: '귀하의 언어를 선택하십시오.',
}