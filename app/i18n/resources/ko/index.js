import home from './home'
import _new from './new'
import claim from './claim'
import restaurant from './restaurant'

export default {
  home,
  'new': _new,
  claim,
  restaurant,
}