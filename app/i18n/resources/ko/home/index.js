import promotional from './promotional.txt'

export default {
  promotional,
  signUp: '지금 무료로 신청하십시오!',
  signIn: '나는 이미 계정을 갖고있다',
  english: 'I don\'t understand this language',
  help: '도움을 받으려면 여기를 클릭하십시오',
}