import help from './help.txt'

export default {
  help,
  linkWillBe: '메뉴의 링크는 다음과 같습니다:',
  create: '내 메뉴 만들기!',
  alreadyTaken: '⚠ 죄송합니다! 그러나이 링크는 이미 사용 중입니다!',
  language: '모국어',
}