export default {
  admin: '귀하는 현재 관리인입니다. 이 메뉴를 편집 할 수 있습니다. 클릭하고 입력하십시오.',
  guest: 'Y이제 손님이되었습니다. 볼 수만 있습니다.',
  adminButton: '관리자 모드로 전환',
  guestButton: '게스트 모드로 전환',
  email: '이메일 주소를 입력 할 수 있습니다',
  phone: '전화 번호를 입력 할 수 있습니다',
  logo: '로고 추가',
  category: '카테고리 이름',
  item: '상품명',
  description: '설명을 입력 할 수 있습니다',
  price: '가격?',
}
