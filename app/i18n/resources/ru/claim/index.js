import body from './body.txt'

export default {
  body,
  button: 'Отлично! Давайте начнем!',
  wait: 'Создание вашего меню ...',
  missing: 'Выберите язык',
}