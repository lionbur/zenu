import promotional from './promotional.txt'

export default {
  promotional,
  signUp: 'Зарегистрируйтесь сейчас бесплатно!',
  signIn: 'у меня уже есть аккаунт',
  english: 'I don\'t understand this langauge',
  help: 'Нажмите здесь для получения справки',
}