import help from './help.txt'

export default {
  help,
  linkWillBe: 'будущая ссылка вашего меню:',
  create: 'Создать мое меню!',
  alreadyTaken: '⚠ Извините! Но эта ссылка уже взята!',
  language: 'язык',
}