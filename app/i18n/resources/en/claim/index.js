import body from './body.txt'

export default {
  body,
  button: 'Great! Let\'s start!',
  wait: 'Creating your menu...',
  missing: 'Please select your language',
}