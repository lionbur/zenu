export default {
  admin: 'You are now an ADMINISTRATOR. You can edit this menu. Just click and type.',
  guest: 'You are now a GUEST. You can only view.',
  adminButton: 'Switch to Administrator Mode',
  guestButton: 'Switch to Guest Mode',
  email: 'Email address',
  phone: 'Phone number',
  logo: 'Logo',
  category: 'Category name',
  item: 'item name',
  description: 'Description',
  price: 'Price?',
}
