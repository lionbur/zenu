import help from './help.txt'

export default {
  help,
  linkWillBe: 'The link of your menu will be:',
  create: 'Create My Menu!',
  alreadyTaken: '⚠ Sorry! But this link is already taken!',
  language: 'Native language',
}