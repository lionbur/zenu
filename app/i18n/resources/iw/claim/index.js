import body from './body.txt'

export default {
  body,
  button: 'מצוין! בוא נתחיל!',
  wait: 'יוצר את התפריט שלך...',
  missing: 'נא בחר בשפה שלך',
}