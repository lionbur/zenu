import promotional from './promotional.txt'
import promotional2 from './promotional2.txt'

export default {
  promotional: process.env.config === 'fb' ? promotional2 : promotional,
  signUp: 'Sign Up for Free Now!',
  signIn: 'I already have an account',
  english: '',
}