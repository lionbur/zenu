import home from './home'
import _new from './new'
import claim from './claim'

export default {
  home,
  'new': _new,
  claim,
}