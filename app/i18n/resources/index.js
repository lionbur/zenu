import en from './en'
import ru from './ru'
import iw from './iw'
import ko from './ko'

export default {
  en,
  ru,
  iw,
  ko,
}