import {auth} from 'firebase'
import {observable} from 'mobx'
import {pick} from 'lodash'

export const session = observable.object({
  user: null,
  isLoggedIn: null,
}, {}, { deep: false })

export const initAuth = () => {
  auth().onAuthStateChanged(user => {
    if (!user) {
      session.user = null
      session.isLoggedIn = false
    } else {
      session.user = pick(user, [ 'uid', 'displayName', 'email', 'photoURL' ])
    }
  })
}