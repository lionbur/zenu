import firebase from 'firebase'
import 'firebase/firestore'
import init from './app'

init()
export default firebase.firestore()