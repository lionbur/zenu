import {initializeApp} from 'firebase'

export default () => {
  let config
  if (process.env.CONFIG === 'fb') {
    config = require('./config2')
  } else {
    config = require('./config')
  }

  initializeApp(config)
}