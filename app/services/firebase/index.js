export db from './db'

import { initAuth } from "./auth"

initAuth()

export { session } from './auth'
export * from './firestorter'