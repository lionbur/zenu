import { initFirestorter, Collection, Document } from 'firestorter'
import firebase from 'firebase'
import './db'

initFirestorter({ firebase })

export {
  Collection,
  Document,
}