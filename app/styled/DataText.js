import React from 'react'
import { Text as NativeText } from 'react-native'
import { get, omit } from 'lodash'
import styled from 'styled-components/native'

const _Text = props => <NativeText {...omit(props, 'isPlaceholder', 'innerRef')}/>

const Text = styled(_Text)`
`

export default ({
  store,
  name,
  numberOfLines,
  ...rest
}) => (store
  ? <Text
      {...{numberOfLines}}
      {...rest}>
        {get(store, name)}
    </Text>
  : null
)
