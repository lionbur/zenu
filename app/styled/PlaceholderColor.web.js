import React from 'react'

export default class extends React.Component {
  static defaultProps = {
    selector: '',
    css: `
      color: rgba(34,34,34,.4);
      font-weight: 100;
      font-style: italic;
    `,
  }

  render() {
    const { css, selector } = this.props
    return (
      <style>
        {`
[contenteditable="true"]:active, [contenteditable="true"]:focus{
   outline: rgba(0,0,0,.5) dashed 3px;
}

.editable {
  position: relative;
}

.editable .editor {
  min-height: 1em;
  transition: 0.25s;
  transition-timing-function: ease-out;
}

.editable .placeholder,
.editable .placeholder {
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  transition: 1s;
  transition-timing-function: ease-out;
  pointer-events: none;
  font-style: italic;
  font-weight: 100;
  opacity: 0;
}

.editable .placeholder.active {
  opacity: 0.3;
}

          ${selector}::-webkit-input-placeholder { ${css} }
          ${selector}::-moz-placeholder { ${css} }
          ${selector}:-ms-input-placeholder { ${css} }
          ${selector}:-moz-placeholder { ${css} }
        `}
      </style>
    )
  }
}