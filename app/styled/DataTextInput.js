import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import styled from 'styled-components/native'
import { get } from 'lodash'

const Container = styled.View`
  ${({fill}) => fill ? 'flex: 1' : ''}
`

const TextInput = styled.TextInput`
`

const MeasureText = styled.Text`
  opacity: 0;
  z-index: -1;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
`

const MultiLineHiddenText = MeasureText.extend`
`

const SingleLineHiddenText = MeasureText.extend`
  white-space: nowrap;
  overflow: hidden;
`

const parseBoolean = text => typeof text === 'string' ? /true/i.test(text) : text

const unity = value => value

const parseByTypeof = {
  number: parseFloat,
  string: unity,
  boolean: parseBoolean,
  'null': unity,
  undefined: unity,
}

export default @observer class extends Component {
  isSaved = true
  @observable value = null
  @observable multiLineHeight = null
  @observable singleLineHeight = null

  notifyChange = value => {
    this.isSaved = false
    this.value = value

    if (this.props.rapidUpdate) {
      this.saveChange()
    }
  }

  saveChange = () => {
    if (!this.isSaved) {
      const {
        store,
        name,
      } = this.props

      store[name] = parseByTypeof[typeof store[name]](this.value)
      this.isSaved = true
      this.value = null
    }
  }

  handleMultiLineLayout = event => this.multiLineHeight = event.nativeEvent.layout.height
  handleSingleLineLayout = event => this.singleLineHeight = event.nativeEvent.layout.height

  render() {
    const {
      store,
      name,
      multiline,
      placeholder,
      numberOfLines,
      style,
      fill,
      ...rest
    } = this.props
    const value = typeof this.value === 'string' ? this.value : get(store, name) || ''
    const shouldMeasureText = multiline && !numberOfLines

    return (store
      ? <Container {...{fill}}>
          <TextInput
            {...{style}}
            value={String(value)}
            onChangeText={this.notifyChange}
            onBlur={this.saveChange}
            onEndEditing={this.saveChange}
            placeholder={placeholder}
            multiline={multiline || numberOfLines > 1}
            numberOfLines={numberOfLines
              ? numberOfLines
              : (this.singleLineHeight > 0 && this.multiLineHeight > 0)
                ? Math.ceil(this.multiLineHeight / this.singleLineHeight)
                : undefined}
            {...rest}
          />
          {shouldMeasureText && <MultiLineHiddenText
            onLayout={this.handleMultiLineLayout}
            {...rest}
          >{
            value
          }</MultiLineHiddenText>}
          {shouldMeasureText && <SingleLineHiddenText
            onLayout={this.handleSingleLineLayout}
            {...rest}
          >{
            value
          }</SingleLineHiddenText>}
        </Container>
      : null)
  }
}
