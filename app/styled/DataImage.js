import React, { Component } from 'react'
import { Image } from 'react-native'

export default class extends Component {
  render () {
    const { store, name, ...rest } = this.props
    const uri = store[name]

    return uri
      ? <Image
          {...rest}
          source={{ uri }}
        />
      : null
  }
}