import styled from 'styled-components/native'

export Screen from './Screen'
import _Field from './Field.web'
export PleaseWait from './PleaseWait'

export const Field = styled(_Field)`
  font-family: HelveticaNeue;
  color: #222;
`

export PlaceholderColor from './PlaceholderColor'
export GlobalStyles from './GlobalStyles'
export Label from './Label'

export ImageField from './ImageField'
export DataText from './DataText'
export DataTextInput from './DataTextInput'
export styled from './styled'
export Link from './Link'