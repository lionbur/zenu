import React from 'react'
import styled from 'styled-components/native'

const Screen = styled.View`
  flex: 1;
  height: 100vh;
`

export default ({t, children, ...props}) => <Screen {...props}>
  {children}
</Screen>
