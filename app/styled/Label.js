import React, { Component, Children, cloneElement } from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  flex-direction: row;
  align-items: stretch;
`

const LabelContainer = styled.TouchableOpacity`
  margin-right: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
`

const LabelText = styled.Text`
  flex: 1;
  color: rgba(34,34,34,.75);
`

const Content = styled.View`
  flex: 1;
`

export default class extends Component {
  labelRef = null
  handleLabelRef = ref => {
    this.labelRef = ref
  }

  childRef = null
  handleChildRef = ref => {
    this.childRef = ref
  }

  handlePress = () => {
    this.childRef && this.childRef.focus()
  }

  render() {
    const {style, label, children} = this.props

    return (
      <Container {...{style}}>
        <LabelContainer
          innerRef={this.handleLabelRef}
          onPress={this.handlePress}
        >
          <LabelText>{label}</LabelText>
        </LabelContainer>
        <Content>
          {cloneElement(
            Children.only(children),
            {
              ...children.props,
              innerRef: this.handleChildRef,
            })}
        </Content>
      </Container>
    )
  }
}