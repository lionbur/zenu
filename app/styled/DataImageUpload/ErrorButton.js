import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  position: absolute;
  background-color: rgba(255,192,192,.5);
  top: 10px;
  left: 10px;
  right: 10px;
  border: 2px solid rgba(255,0,0,.75);
  border-radius: 5px;
  box-shadow: 0px 0px 5px;
  padding: 5px;
`

const ErrorText = styled.Text`
  font-size: 16px;
  line-height: 20px;
  color: red;
  user-select: none;
`

const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: flex-end;
`

const ButtonContainer = styled.TouchableOpacity``

const ButtonText = styled.Text`
  font-size: 16px;
  user-select: none;
  padding: 2px 5px;
`

const CancelButtonText = ButtonText.extend`
  color: red;
`

const RetryButtonText = ButtonText.extend`
  color: green;
`

export default ({ children, onCancel, onRetry, ...props }) => (
  <Container {...props}>
    <ErrorText>{children}</ErrorText>
    <ButtonsContainer>
      <ButtonContainer onPress={onCancel}>
        <CancelButtonText>Cancel</CancelButtonText>
      </ButtonContainer>
      <ButtonContainer onPress={onRetry}>
        <RetryButtonText>Retry</RetryButtonText>
      </ButtonContainer>
    </ButtonsContainer>
  </Container>
)
