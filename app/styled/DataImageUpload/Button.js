import React from 'react'
import styled from 'styled-components/native'
import { TouchableOpacity } from 'react-native'

import ButtonText from './ButtonText'

export default styled(({ title, ...props }) =>
  <TouchableOpacity {...props}>
    <ButtonText>{title}</ButtonText>
  </TouchableOpacity>
)
