import React, { Component } from 'react'
import styled from 'styled-components/native'
import { observer } from 'mobx-react'
import readAndCompressImage from 'browser-image-resizer'
import { get } from 'lodash'

import CaptureButton from './CaptureButton'
import BrowseButton from './BrowseButton'
import ButtonBar from './ButtonBar'
import DeleteButton from './DeleteButton'
import PauseButton from './PauseButton'
import ResumeButton from './ResumeButton'
import CancelButton from './CancelButton'
import ErrorButton from './ErrorButton'
import ProgressBar from './ProgressBar'

const Container = styled.View`
  position: relative;
  overflow: hidden;
`

const Image = styled.Image`
  flex: 1;
`

const Placeholder = styled.Text`
  flex: 1;
  color: white;
  text-shadow: 0 0 5px black;
  text-align: center;
`

export default @observer class extends Component {
  originalValue = null
  layout = null

  handleSelected = async files => {
    const { store, name } = this.props

    this.originalValue = store[name]

    const { width, height } = this.layout
    const pixelRatio = typeof window === 'undefined'
      ? window.devicePixelRatio || 4
      : 4 // TODO: mobile native
    const maxDim = Math.max(width, height) * 4

    const resizeImage = /\.png$/.test(files[0].name)
      ? files[0]
      : await readAndCompressImage(files[0], {
        quality: 0.75,
        maxWidth: maxDim,
        maxHeight: maxDim,
        autoRotate: true,
      })

    store[name] = {
      file: files[0],
      blob: resizeImage,
    }
  }

  handleErrorDismiss = () => {
    const { store, name } = this.props
    store._progress[name] = null
    store[name] = this.originalValue
    this.originalValue = null
  }

  handleRetry = () => {
    const { store, name } = this.props

    store._progress[name] = null

    const value = store[name]
    store[name] = this.originalValue
    store[name] = value
  }

  handleLayout = event => this.layout = event.nativeEvent.layout

  render () {
    const { store, name, style, placeholder, ...rest } = this.props
    const isEditable = store._progress != null
    const progress = get(store._progress, name)

    return (<Container {...{style}}>
      <Image
        {...rest}
        onLayout={this.handleLayout}
        source={{ uri: (progress && progress.dataUrl) || store[name] }}
      />
      {!store[name] && placeholder && <Placeholder>{placeholder}</Placeholder>}
      {progress && progress.error && <ErrorButton
        onCancel={this.handleErrorDismiss}
        onRetry={this.handleRetry}
      >{
          progress.error.message
        }</ErrorButton>
      }
      <ButtonBar>
        {isEditable && !progress && <CaptureButton onSelected={this.handleSelected}/>}
        {isEditable && !progress && <BrowseButton onSelected={this.handleSelected}/>}
        {progress && !progress.error && <ProgressBar
          value={progress.bytesTransferred / progress.totalBytes}>{
            `${progress.bytesTransferred >> 10}KB`
        }</ProgressBar>}
        {progress && !progress.error && !progress.isPaused && <PauseButton onPress={progress.pause}/>}
        {progress && !progress.error && progress.isPaused && <ResumeButton onPress={progress.resume}/>}
        {progress && !progress.error && <CancelButton onPress={() => {
          if (progress.isPaused) {
            progress.resume()
          }
          progress.cancel()
        }}/>}
      </ButtonBar>
    </Container>)
  }
}

//        {isEditable && !progress && <DeleteButton/>}
