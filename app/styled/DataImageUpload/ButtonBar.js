import styled from 'styled-components/native'

export default styled.View`
  position: absolute;
  left: 10px;
  right: 10px;
  top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`