import styled from 'styled-components/native'

export const css = `
  background-color: rgba(255,255,255,.1);
  width: 20px;
  text-align: center;
  border-radius: 5px;
  border: 1px solid rgba(255,255,255,.2);
  font-size: 12px;
  line-height: 20px;
  box-shadow: 0px 0px 5px;
  user-select: none;
`

export default styled.Text`
  ${css}
`
