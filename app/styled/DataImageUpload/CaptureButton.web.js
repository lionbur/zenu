import React from 'react'
import styled from 'styled-components'

import { css } from './ButtonText'

export function isSupported() {
  const input = document.createElement('input')
  input.setAttribute('capture', true)
  return !!input.capture
}

const Input = styled.input
  .attrs({
    type: 'file',
    accept: 'image/*',
    capture: 'environment',
  })`
  width: 0;
  height: 0;
  opacity: 0;
`

const Button = ({ onSelected, multiple, ...props }) => (
  isSupported()
    ? <label {...props}>
        {'📷'}
        <Input
          {...{multiple}}
          onChange={({ target }) => onSelected(target.files)}
        />
      </label>
    : null
)


export default styled(Button)`
  ${css}
`