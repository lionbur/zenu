import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  position: absolute;
  left: 20px;
  right: 20px;
  top: 20px;
  border: 1px solid rgba(0,0,0,.5);
  border-radius: 7px;
  box-shadow: 0px 0px 5px;
`

const Bar = styled.View`
  border: 2px solid rgba(0,0,0,.5);
  border-radius: 5px;
  background-color: rgba(128,128,128,.5);
  width: ${({ value }) => value * 100}%;
  height: 20px;
  align-items: center;
  justify-content: flex-end;
  overflow: hidden;
`

const Text = styled.Text`
  font-size: 12px;
`

export default ({ value = 0.5, children }) => (
  <Container>
    <Bar {...{value}}>
      <Text>{children}</Text>
    </Bar>
  </Container>
)
