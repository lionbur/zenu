import React from 'react'
import styled from 'styled-components'

import { css } from './ButtonText'
import { isSupported } from "./CaptureButton"

const Input = styled.input
  .attrs({
    type: 'file',
    accept: 'image/*',
  })`
  width: 0;
  height: 0;
  opacity: 0;
`

const Button = ({ onSelected, multiple, ...props }) => (
  <label {...props}>
    {isSupported() ? '📎' : '📂'}
    <Input
      {...{multiple}}
      onChange={({ target }) => onSelected(target.files)}
    />
  </label>
)


export default styled(Button)`
  ${css}
`