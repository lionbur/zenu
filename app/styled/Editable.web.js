import React, { Component } from 'react'
import styled from 'styled-components'
import { get } from 'lodash'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import directionByLanguage from './directionByLanguage'

const parseBoolean = text => typeof text === 'string' ? /true/i.test(text) : text

const unity = value => value

const parseByTypeof = {
  number: parseFloat,
  string: unity,
  boolean: parseBoolean,
  'null': unity,
  undefined: unity,
}


const Placeholder = styled.span`
  opacity: 0.3;
  position: relative;
  direction: ${({language}) => directionByLanguage(language)};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 100%;
`

export default @observer class extends Component {
  saveChange = value => {
    if (!this.isSaved) {
      const {
        store,
        name,
      } = this.props

      store[name] = parseByTypeof[typeof store[name]](value)
    }
  }

  @observable isEditing = false
  originalValue = null

  handleBlur = event => {
    this.isEditing = false
    this.saveChange(this.editorRef.innerText)
    if (this.props.store.order === 1000000) {
      this.editorRef.innerText = ''
    }
  }

  editorRef = null

  handleEditorRef = ref => {
    this.editorRef = ref
  }
  
  handleKeyPress = event => {
    if (!this.props.multiline) {
      if (event.charCode === 13) {
        this.editorRef.blur()
      }
    }
  }

  handleClick = () => {
    this.isEditing = true
    this.originalValue = this.props.store[this.props.name]
  }

  render() {
    const {
      isAdmin,
      store,
      name,
      placeholder,
      className = '',
      language,
      isAutoTranslated,
      t,
      ...rest
    } = this.props
    const value = store[name] || ''

    if (!store) {
      return null
    }

    return (
      (isAdmin || value)
      ? <div
        className={['editable', className].join(' ')}
        onClick={this.handleClick}
        {...rest}
        >
          <div
            key="editor"
            tabIndex="0"
            className="editor"
            ref={this.handleEditorRef}
            contentEditable={isAdmin}
            suppressContentEditableWarning
            onInput={this.handleInput}
            onBlur={this.handleBlur}
            onKeyPress={this.handleKeyPress}
            style={{
              whiteSpace: 'pre-wrap',
              direction: directionByLanguage(language),
            }}
          >{
            value
          }</div>
          {isAdmin && !value && <Placeholder
            className={`placeholder${!this.isEditing ? ' active' :''}`}
            {...{language}}
          >{
            this.originalValue
              ? 'Deleting...'
              : placeholder
          }</Placeholder>}
        </div>
      : null
    )
  }
}
