import React from 'react'
import { inject, observer } from 'mobx-react'
import { get } from 'lodash'

import DataImageUpload from './DataImageUpload/'
import DataImage from './DataImage'

export default inject('params')(observer(({
  params,
  ...rest
}) => get(params, 'isAdmin')
  ? <DataImageUpload {...rest}/>
  : <DataImage {...rest}/>))
