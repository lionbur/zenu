import React from 'react'
import styled from 'styled-components/native'

const Text = styled.Text``

export default props => (
  <Text {...props}>
    Please wait...
  </Text>
)