import React from 'react'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import {get} from 'lodash'
import { TouchableOpacity, Linking } from 'react-native'

import { Field, styled } from 'styled'

const TouchableLink = ({store, name, baseHref, ...rest}) => (
  <TouchableOpacity
    onPress={() => Linking.openURL(`${baseHref}:${store[name]}`)}>
    <Field {...{store, name}} {...rest}/>
  </TouchableOpacity>)

const Link = ({isAdmin, ...rest}) => (
  isAdmin
    ? <Field {...rest}/>
    : <TouchableLink {...rest}/>)

const enhance = compose(
  inject(store => ({
    isAdmin: get(store, 'params.isAdmin')
  })),
  observer,
)

export default enhance(Link)
