import React from 'react'
import { inject, observer } from 'mobx-react'
import { get } from 'lodash'
import {compose} from 'recompose'

import Editable from './Editable.web'

const enhance = compose(
  inject(({
    params: {
      isAdmin,
      translationLanguage,
    },
    shop: {
      data: {
        language: shopLanguage,
      },
    },
  }) => ({
    isAdmin,
    language: translationLanguage !== '_' ? translationLanguage : shopLanguage,
  })),
  observer,
)

export default enhance(Editable)