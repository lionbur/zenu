const rtlLanguages = [ 'ar', 'he' ]
export default language => rtlLanguages.includes(language) ? 'rtl' : 'ltr'
