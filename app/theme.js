import {css} from 'styled-components'

const minSizes = {
  desktop: 993,
  largeTablet: 769,
  tablet: 377,
}

export const media = Object.keys(minSizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${(minSizes[label]) / 16}em) {
      ${css(...args)}
    }
  `

  return acc
}, {})

const palette = [
  'white',
  '#a02',
  'black',
  '#a02',
]

const getTheme = (template, config) => (
  typeof template === 'object'
    ? Object.entries(template).reduce(
      (result, [key, value]) => ({
        ...result,
        [key]: getTheme(value, config),
      }),
    {})
    : typeof template === 'number'
      ? config.palette[template]
      : template
)

const template = {
  main: {
    color: 0,
    backgroundColor: 2,
  },
  button: {
    backgroundColor: 1,
  },
  header: {
    backgroundColor: 0,
  },
  restaurant: {
    name: {
      color: 0,
    },
  },
  category: {
    css: `
      padding: 5px 40px;
    `,
    name: {
      color: 1,
      borderBottom: '',
    },
    description: {
      color: 0,
      backgroundColor: 1,
    },
    originalName: {
      color: 3,
    }
  },
  item: {
    name: {
      color: 0,
    },
    originalName: {
      color: 3,
    },
  },
}

export default getTheme(template, {palette})