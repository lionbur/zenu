import { StackRouter, createNavigator } from 'react-navigation'

export default config => createNavigator(
  StackRouter(config)
)