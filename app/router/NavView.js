import React from 'react'
import { addNavigationHelpers } from 'react-navigation'

export default ({ navigation, router }) => {
  const { state } = navigation
  const Component = router.getComponentForState(state)
  return (
    <Component
      navigation={addNavigationHelpers({
        ...navigation,
        state: state.routes[state.index],
      })}
    />
  )
}