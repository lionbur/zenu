import React, { Component } from 'react'
import { addNavigationHelpers } from 'react-navigation'
import { translate } from 'react-i18next'

import { Screen, PlaceholderColor } from 'styled'
import i18n from '../i18n'

export default class extends Component {
  componentWillReceiveProps(props) {
    if (this.props.navigation.state !== props.navigation.state) {
      this.setState({ isMobileMenuOpen: false })
      window && window.scrollTo(0, 0)
    }
  }

  render() {
    const { navigation, router } = this.props
    const { state } = navigation
    const ScreenView = router.getComponentForRouteName(
      state.routes[state.index].routeName
    )

    const childNavigation = addNavigationHelpers({
      ...navigation,
      state: navigation.state.routes[navigation.state.index],
    })
    const t = i18n.getFixedT()
    t.i18n = i18n

    const ReloadAppOnLanguageChange = translate('common', {
      bindI18n: 'languageChanged',
      bindStore: false
    })(Screen)

    return (
      <ReloadAppOnLanguageChange>
        <PlaceholderColor/>
        <ScreenView navigation={childNavigation} {...{t}} />
      </ReloadAppOnLanguageChange>
    )
  }
}