import {get, find} from 'lodash'

import { AUTO_NUMBER, STORAGE_URL } from './helpers'
import locale2 from 'locale2'
import {getLanguages} from 'translate'

const storedLanguage = localStorage.getItem('language')
const storedCurrencyCode = localStorage.getItem('currencyCode')

const language = new URL(window.location.href).searchParams.get('l')
const currencyCode = new URL(window.location.href).searchParams.get('c')
/*
if (!storedLanguage && language) {
  localStorage.setItem('language', language)
}
if (!storedCurrencyCode && currencyCode) {
  localStorage.setItem('language', language)
}
*/
const defaultLocale = language || storedLanguage || locale2.split(/_|-/)[0]
let defaultCurrency = currencyCode || storedCurrencyCode || get(find(getLanguages(), {code: defaultLocale}), 'currencyCode') || 'USD'

const initialParams = {
  translationLanguage: defaultLocale,
  translationLanguageRequest: defaultLocale,
  userId: '_',
  overrideIsAdmin: null,
  isAdmin: false,
  currencyCode: defaultCurrency,
}

export default {
  initialParams,
  profile: 'users/:userId',
  shop: {
    path: 'restaurants/:slug',
    defaults: {
      name: '',
      address: '',
      email: '',
      language: 'en',
      headerImage: STORAGE_URL,
      logo: STORAGE_URL,
      currencyCode: defaultCurrency,
    },
    sub: {
      gallery: 'gallery',
      phones: {
        path: 'phones',
        defaults: {
          phoneNumber: '',
          order: AUTO_NUMBER,
        },
      },
    }
  },
  shops: {
    path: 'restaurants',
    defaults: {
      name: '',
      currencyCode: defaultCurrency,
    },
  },
  translatedCategories: {
    path: 'restaurants/:slug/translations/:translationLanguage/categories',
    sub: {
      items: 'items',
    },
  },
  categories: {
    path: 'restaurants/:slug/categories',
    defaults: {
      name: '',
      order: AUTO_NUMBER,
    },
    sub: {
      items: {
        path: 'items',
        defaults: {
          name: '',
          description: '',
          price: 0,
          order: AUTO_NUMBER,
          thumbnail: STORAGE_URL,
          shares: {},
        },
      },
    },
  },
}