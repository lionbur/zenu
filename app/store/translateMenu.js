import store from 'store'
import isValidLanguage from './isValidLanguage'
import { firestore } from 'firebase'
import { Collection } from 'services'
import { omit, intersection, escapeRegExp, pick } from 'lodash'

import { translate as __translate } from 'translate'

const _translate = async (text, config) => {
  const exp = /\((.*?)\)/g
  let matches

  while (matches = exp.exec(text)) {
    text = text.replace(matches[1],  await __translate(matches[1], config))
  }

  return __translate(text, config)
}

const onTheMenu = {
  en: 'Restaurant Menu',
}

const sourceTranslations = [
  { from: /том\s+ям/ig, to: 'том-ям' },
  { from: /(\d+,\d{1,2})(?![\s\d])/ig, to: (match, p1) => `${p1.replace(',', '.')} ` },
  { from: /(\s0,\d{3,3})(?![\s\d])/ig, to: (match, p1) => `${p1.replace(',', '.')} ` },
  { from: /(\d+)(?![\s\d.,\/])/ig, to: '$1 ' },
  { from: /\s(\d+,\d{1,2})\s/ig, to: (match, p1) => ` ${p1.replace(',', '.')} ` },
]

const lowercaseWords = [
  /^with$/i,
  /^in$/i,
  /^on$/i,
  /^at$/i,
  /^the$/i,
  /^for$/i,
  /^of$/i,
  /^a$/i,
  /^to$/i,
  /^from$/i,
  /^and$/i,
  /^g(?!\w)/i,
  /^gr(?!\w)/i,
  /^cm(?!\w)/i,
  /^ml(?!\w)/i,
  /^mg(?!\w)/i,
  /^pc(?!\w)/i,
  /^grams*(?!\w)/i,
]

const simplified = [
  { from: /dressed\s+with/gi, to: 'with sauce of' },
  { from: /dressing/gi, to: 'sauce' },
  { from: /pizzetta/gi, to: 'a small pizza'},
  { from: /1\s*pc\.*/gi, to: '1 piece'},
  { from: /(\w*)berry/gi, to: '$1berries'},
  { from: /meat\s+broth/gi, to: 'meat soup'},
  { from: /beer\s+draft/gi, to: 'draft beer' },
  { from: /marcuya/gi, to: 'passionfruit' },
  { from: /bbq/gi, to: 'barbecue' },
  { from: /(?![.,])\s+([\d.]+\s\w{1,2}\.*)$/ig, to: '. $1' },
  { from: /(?![.,])\s+([\d.]+\sgrams*\.*)$/ig, to: '. $1' },
  { from: /(\d+)\sL\s/ig, to: '$1 Litre ' },
  { from: /(\d+)\sL$/ig, to: '$1 Litre' },
  { from: /(\d+)\/\d+\s(\w{1,2})\s\(M\)$/g, to: '$1 $2' },
  { from: /\d+\/(\d+)\s(\w{1,2})\s\(L\)$/g, to: '$1 $2' },
  { from: /(\w)-(\w)/g, to: '$1 - $2'},
  { from: /&/g, to: ' and ' },
//  { from: /\((.*?)\)/g, to: '. $1. ' },
  { from: /\sgr|(?!\w)g(?!\w)/g, to: ' gram' },
  { from: /mix\s+(\w)(?!of)/gi, to: 'mix of $1' },
  { from: /\sVAT(?!\w)/g, to: ' Value Added Tax' },
  { from: /bottled\sbeer/gi, to: 'Beer in bottle' },
  { from: /(?!\w)dairy(?!\w)/gi, to: 'Dairy' },
  { from: /(?!\w)comb(?!\w)/gi, to: 'combination'},
  { from: /(?!\w)vegi|veggie|vegan(?!\w)/gi, to: 'vegeterian'},
  { from: /\(M\)/g, to: '(medium)'},
  { from: /\(L\)/g, to: '(large)'},
  { from: /\(S\)/g, to: '(small)'},
  { from: /\(XL\)/g, to: '(extra large)'},
  { from: /(?!\w)inside out(?!\w)/ig, to: '"inside out"'},
  { from: /\((\d+) pcs\)/ig, to: '($1 pieces)'},
  { from: /\((\d+) pc\)/ig, to: '($1 pieces)'},
]

const capitalizeWord = (text, index) =>
  index && lowercaseWords.some(exp => exp.test(text))
    ? text.toLowerCase()
    : text.toLowerCase().replace(/([a-z])/i, (match, p1) => p1.toUpperCase())

const capitalize = text => text
  .split(/\s+/)
  .map(capitalizeWord)
  .join(' ')

const simplify = text => simplified
  .reduce((result, {from, to}) => result.replace(from, to), text)

const wordsInText = text => text
  .toLowerCase()
  .split(/\s+/)
  .filter(word => !/[\d.-=:;,()*&^%$#@!`\[\]`~\/\\<>|]/.test(word))

const findCommonWords = (text1, text2) =>
  intersection(wordsInText(text1), wordsInText(text2))

const haveCommonWords = (text1, text2) => findCommonWords(text1, text2).length > 0

const translate = async (text, config) => {
  let prefix

  if (!(config.from in onTheMenu)) {
    onTheMenu[config.from] = await _translate(onTheMenu.en, {
      from: 'en',
      to: config.from,
    })
  }
  prefix = onTheMenu[config.from] + '. '

  const translatedParts = await Promise.all(
    text
      .split(/\s*[\\:;]\s*|[,.]\s|\s+[-|]\s+|\/(?!\d)/) // update also below
      .map(async part => {
        for (const {from,to} of sourceTranslations) {
          part = part.replace(from, to)
        }

        const simple = simplify(part)
        const translatedBruto = await _translate(prefix + simple, config)
        const match = /\.\s*(.*)/.exec(translatedBruto)
        const translated = match ? match[1] : translatedBruto

        if (!match) console.log('weird case', simple, translatedBruto)

        if (config.to !== 'en') {
          const commonWords = findCommonWords(simple, translated)
          console.log('common words', simple, translated, commonWords)

          if (commonWords.length) {
            let part2 = simple

            for (const word of commonWords) {
              if (word && !/[+?/\\*()[\]]/.test(word)) {
                part2 = part2.replace(new RegExp(escapeRegExp(word), "i"), capitalizeWord(word, 1))
              }
            }

            let translated2 = await _translate(part2, config)
            const commonWords2 = findCommonWords(part2, translated2)
            console.log('common words 2', part2, translated2, commonWords2)

            for (const word of commonWords2) {
              const translatedWord = (await _translate(`"${capitalizeWord(word, 1)}`, config))
                .replace(/^[""]/, '')

              if (translatedWord !== word) {
                translated2 = translated2.replace(new RegExp(word, "i"), `${translatedWord}`)
              }
            }

            return translated2
          } else {
            return translated
          }
        }

        const reserveConfig = {
          from: config.to,
          to: config.from,
        }

        const reversedTranslation = await _translate(translated, reserveConfig)

        if (haveCommonWords(translated, reversedTranslation)) {
          console.log('common words', findCommonWords(translated, reversedTranslation))
          const translated2 = await _translate(simplify(part), config)

          return translated2
          /*
          const reversedTranslation2 = await _translate(translated2, reserveConfig)
          if (haveCommonWords(translated2, reversedTranslation2)) {
            const exp = /([a-z]+)/gi
            let match
            let translated3 = translated

            while (match = exp.exec(reversedTranslation)) {
              translated3 = translated3.replace(match[1], `"${match[1]}"`)
            }

            console.log('Translation improvement', translated3)
            return translated3
          } else {
            return translated2
          }*/
        } else {
          return translated
        }
      })
  )

  const delimiters = []
  const delimiter = /(\s*[\\:;]\s*|[,.]\s|\s+[-|]\s+|\/(?!\d))/g

  let match
  while (match = delimiter.exec(text)) {
    delimiters.push(match[1])
  }

  let result = translatedParts[0]

  for (let i = 1; i < translatedParts.length; i++) {
    let translatedPart = translatedParts[i]
    const words = wordsInText(translatedPart)
    const commonWords = findCommonWords(result, translatedParts[i])
    if (commonWords.length > 1 && commonWords.length >= words.length / 2) {
      commonWords.forEach(
        word => translatedPart = translatedPart
          .replace(word, '')
      )

      if (!translatedPart) {
        continue
      }
    }

    result += delimiters[i - 1] + translatedPart
  }

  return result
}

const translateDocument = async ({ original, translations, translationConfig }) => {
  const translated = translations
    .docs
    .find(({id}) => id === original.id)

  if (translated) {
    await translated.ready()
  }

  const originalTime = original.data.timeLastModified || original.data.timeCreated
  const translatedTime = (translated
    && (translated.data.timeLastModified || translated.data.timeCreated))
    || 0


  if (originalTime > translatedTime) {
    const translation = true || !translated || !translated.data.timeLastModified
      ? {
        name: await translate(original.data.name, translationConfig),
      }
      : {}

    if (original.data.description
      && (!translated || !translated.data.timeLastModified)) {
      translation.description = await translate(original.data.description, translationConfig)
    }

    if (translated) {
      await translated.update({
        timeCreated: firestore.FieldValue.serverTimestamp(),
        ...translation,
      })

      console.log(original.data.name, originalTime, translatedTime, translated.data, translated.path)
    } else {
      await translations.ref.doc(original.id).set({
        timeCreated: firestore.FieldValue.serverTimestamp(),
        ...translation,
      })
    }
  }

  return translated
}

export default async function translateMenu(translationLanguage) {
  if (!store) {
    return
  }
  if (store.params.slug === '_') {
    return
  }

  console.log({
    langauge: store.shop.data.language,
    translationLanguage
  })

  if (!isValidLanguage(translationLanguage) || !store.shop.data.language) {
    store.params.translationLanguage = translationLanguage
    return
  }

  const steps = store.shop.data.language === 'en'
    ? [
      { from: 'en', to: translationLanguage, isTranslatedSource: false, insFinal: true }
    ]
    : translationLanguage === 'en'
      ? [
        { from: store.shop.data.language, to: 'en', isTranslatedSource: false, isFinal: true },
      ]
      : [
        { from: store.shop.data.language, to: 'en', isTranslatedSource: false, isFinal: false },
        { from: 'en', to: translationLanguage, isTranslatedSource: true, isFinal: true },
      ]

  console.log(steps)

  for (const translationConfig of steps) {
    const categories = translationConfig.isTranslatedSource
      ? new Collection(
        `restaurants/${store.params.slug}/translations/${translationConfig.from}/categories`,
        {mode: 'on'}
      )
      : new Collection(
        `restaurants/${store.params.slug}/categories`,
        {mode: 'on'}
      )

    await categories.ready()

    const translatedCategories = new Collection(
      `restaurants/${store.params.slug}/translations/${translationConfig.to}/categories`,
      {mode: 'on'}
    )

    await translatedCategories.ready()

    if (!categories.active) {
      await categories.fetch()
    }

    let categoriesLeft = categories.docs.length

    await Promise.all(categories
      .docs
      .map(async category => {
        try {
          translateDocument({
            original: category,
            translations: translatedCategories,
            translationConfig,
          })

          const items = new Collection(
            [categories.path, category.id, 'items'].join('/'),
            {mode: 'on'}
          )

          await items.ready()

          const translatedItems = new Collection(
            [translatedCategories.path, category.id, 'items'].join('/'),
            {mode: 'on'}
          )

          await translatedItems.ready()

          await Promise.all(items
            .docs
            .map(item =>
              translateDocument({
                original: item,
                translations: translatedItems,
                translationConfig,
              })
            ))

          if (translationConfig.isFinal) {
            if (--categoriesLeft === 0) {
              if (store.params.translationLanguage !== translationLanguage) {
                store.translatedCategories._docLookup = {}
                store.translatedCategories.docs.replace([])
                store.params.translationLanguage = translationLanguage
              }
            }
          }
        } catch (error) {
          console.log('error', error)
        }
      }))
  }
}