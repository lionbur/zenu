import { observe } from 'mobx'
import translateMenu from './translateMenu'

export default store => {
  observe(store.params, 'translationLanguageRequest', ({newValue}) => {
    translateMenu(newValue)
    localStorage.setItem('language', newValue)
  })

  observe(store.params, 'currencyCode', ({newValue}) => {
    localStorage.setItem('currencyCode', newValue)
  })
}