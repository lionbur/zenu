import {observable, autorun, toJS} from 'mobx'

const STORAGE_KEY = 'persistentData'
const initlialState = {
  affiliate: '',
  player: null,
}

const persist = observable({
  ...initlialState,
  ...JSON.parse(
    localStorage.getItem(STORAGE_KEY)
  )
})

console.log('persist', toJS(persist))

autorun(
  () => {
    console.log(STORAGE_KEY, toJS(persist))
    localStorage.setItem(STORAGE_KEY, JSON.stringify(toJS(persist)))
  }
)

const url = new URL(location.href)
const affiliate = url.searchParams.get('aff')
if (affiliate) {
  persist.affiliate = affiliate
}

export default persist
