import { Document } from 'services'
import makeSmartPath from './makeSmartPath'
import { observe, observable } from 'mobx'
import { storage } from 'firebase'

import makeAutoUpdate from './makeAutoUpdate'
import makeCollection from './makeCollection'

const fileToDataUrl = file => new Promise(resolve => {
  const reader = new FileReader()

  reader.onload = ({ target: { result } }) => {
    resolve(result)
  }

  reader.readAsDataURL(file)
})

const handleUpload = (doc, { name, data: { file, blob } }) => new Promise(async resolve => {
  const dataUrl = await fileToDataUrl(file)

  const upload = storage()
    .ref(doc.path)
    .child(name)
    .put(blob, {
      contentType: blob.type,
      customMetadata: {
        name: file.name,
        size: file.size,
        lastModified: file.lastModifiedDate.toISOString(),
      },
    })

  upload
    .on('state_changed', ({ bytesTransferred, totalBytes, state, task }) => {
      doc.data._progress[name] = {
        dataUrl,
        bytesTransferred,
        totalBytes,
        isPaused: state === storage.TaskState.PAUSED,
        pause() { task.pause() },
        resume() { task.resume() },
        cancel() { task.cancel() },
      }
    }, error => {
      doc.data._progress[name] = {
        error,
      }
      resolve(null)
    }, () => {
      doc.data._progress[name] = null
      resolve(upload.snapshot.downloadURL)
    })
})

export default function (path, params, doc) {
  const result = doc || new Document(makeSmartPath(path.path || path, params))

  const callMakeAutoUpdate = target =>
    makeAutoUpdate({
      target,
      defaults: path.defaults,
      onUpdate: (...args) => result.update(...args),
      onDelete: (...args) => result.delete(...args),
      onUpload: (...args) => handleUpload(result, ...args),
    })

  observe(result._data, ({ newValue }) => callMakeAutoUpdate(newValue))

  if (doc) {
    callMakeAutoUpdate(doc.data)
  }

  if (path.sub) {
    result.sub = observable(Object
      .entries(path.sub)
      .reduce((sub, [key, subPath]) => ({
        ...sub,
        [key]: makeCollection({
          ...(typeof subPath === 'object' ? subPath :null),
          path: `${path.path}/${subPath.path || subPath}`
        }, params),
      }), {}))
  }

  return result
}