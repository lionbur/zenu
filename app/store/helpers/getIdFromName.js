export default name => name
  .trim()
  .toLowerCase()
  .replace(/["'?!()[\]{}@#$%^&*<>/\\]/g, '')
  .replace(/\s*[-:;,]\s*/g, ' ')
  .replace(/\s+/g, '-')
