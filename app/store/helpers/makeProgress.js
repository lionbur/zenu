import { observable } from 'mobx'

import {STORAGE_URL} from "./consts"

export default (target, defaults) => {
  if (!defaults) {
    return
  }
  
  const progress = Object
    .entries(defaults)
    .filter(([_, value]) => value === STORAGE_URL)
    .reduce((result, [key]) => ({
      ...result,
      [key]: null,
    }), undefined)

  if (progress) {
    target._progress = observable(progress)
  }
}