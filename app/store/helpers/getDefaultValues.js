import { firestore } from 'firebase'
import {AUTO_NUMBER, SERVER_TIMESTAMP, STORAGE_URL} from "./consts"

const extractValue = (value, autoNumberBase) => value === SERVER_TIMESTAMP
  ? firestore.FieldValue.serverTimestamp()
  : value === AUTO_NUMBER
    ? autoNumberBase || 0
    : value === STORAGE_URL
      ? ''
      : value

export default (defaults, excludeList, autoNumberBase) => {
  const newDefaultValueEntries = Object
    .entries(defaults)
    .filter(([propertyName]) => !excludeList.includes(propertyName))

  if (newDefaultValueEntries.length) {
    const defaultValuesUpdate = newDefaultValueEntries
      .reduce((result, [propertyName, defaultValue]) => ({
        ...result,
        [propertyName]: extractValue(defaultValue, autoNumberBase)
      }), {})

    return defaultValuesUpdate
  }

  return null
}