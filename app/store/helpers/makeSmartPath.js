export const variableEx = /^:(.*)$/
export const cleanName = name => name.match(variableEx)[1]

export default function (path ,params) {
  const parts = path.split('/')
  const variableNames = parts
    .filter(part => variableEx.test(part))
    .map(part => cleanName(part))

  if (variableNames.length) {
    const cleanParts = parts
      .map(part => variableEx.test(part)
        ? cleanName(part)
        : part)

    return () => {
      const result = cleanParts
        .map(part => params[part] || part)
        .join('/')
      console.log('smartPath', result)
      return result
    }
  }

  return path
}