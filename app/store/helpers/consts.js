export const SERVER_TIMESTAMP = { type: 'serverTimestamp' }
export const AUTO_NUMBER = { type: 'autoNumber' }
export const STORAGE_URL = { type: 'storageUrl' }
export const MAX_FILE_SIZE = 1 << 20 // 1 MB
