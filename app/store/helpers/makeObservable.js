import makeDocument from './makeDocument'
import makeCollection from './makeCollection'

export default (path, params) => ((path.path || path).split('/').length % 2 === 0)
  ? makeDocument(path, params)
  : makeCollection(path, params)