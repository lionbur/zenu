import { observable, observe, runInAction } from 'mobx'

import { Collection } from 'services'
import makeSmartPath from './makeSmartPath'
import getIdFromName from './getIdFromName'
import getDefaultValues from './getDefaultValues'
import { firestore } from 'firebase'
import makeDocument from "./makeDocument"
import makeProgress from './makeProgress'

export default function makeCollection(path, params) {
  const result = new Collection(makeSmartPath(path.path || path, params))

  if (path.defaults) {
    const newDocument = observable({
      ...path.defaults,
      order: 1000000,
    })
    const newDocumentNode = {
      data: newDocument,
      path: `${result.path}/$new`,
    }
    makeProgress(newDocument, path.defaults)

    const mainField = ('name' in path.defaults)
      ? 'name'
      : Object.keys(path.defaults)[0]

    if (mainField in path.defaults) {
      observe(newDocument, mainField, ({oldValue, newValue}) => {
        if (!oldValue) {
          const id = mainField === 'name'
            ? getIdFromName(newValue)
            : null

          runInAction(() => {
            id
              ? result.ref.doc(id).set({
                [mainField]: newValue,
                ...getDefaultValues(path.defaults, [mainField], result.docs.length + 1),
                timeCreated: firestore.FieldValue.serverTimestamp(),
              })
              : result.add({
                [mainField]: newValue,
                ...getDefaultValues(path.defaults, [mainField], result.docs.length + 1),
                timeCreated: firestore.FieldValue.serverTimestamp(),
              })
          })
          newDocument[mainField] = ''
        }
      })

      Object.defineProperty(result, 'docsWithNew', {
        get() {
          return observable([
            ...this.docs,
            newDocumentNode,
          ])
        }
      })
    }
  }

  if (path.orderBy) {
    result.query = result.ref.orderBy(path.orderBy, 'asc')
  }

  const makeSubDocument = doc => {
    makeDocument({
      ...path,
      path: [(path.path || path), doc.id].join('/'),
    }, params, doc)
  }

  result
    .ready()
    .then(() => result
      .docs
      .forEach(makeSubDocument))

  observe(result.docs, change => {
    switch(change.type) {
      case 'splice':
        change.added && change
          .added
          .filter(doc => !change.removed.includes(doc))
          .forEach(makeSubDocument)
        break

      case 'update':
        makeSubDocument(change.newValue)
        break

      default:
    }
  })

  return result
}