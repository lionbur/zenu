import { firestore } from 'firebase'
import { throttle } from 'lodash'
import { observe } from 'mobx'
import { Alert } from 'react-native-extended'

import getDefaultValues from './getDefaultValues'
import {STORAGE_URL} from "./consts"
import makeProgress from './makeProgress'

export default ({ target, defaults, onUpdate, onDelete, onUpload }) => {
  const keys = Object.keys(target)

  if (defaults) {
    const defaultValues = getDefaultValues(defaults, keys)

    if (defaultValues) {
      onUpdate({
        ...defaultValues,
        timeLastModified: firestore.FieldValue.serverTimestamp(),
      })
    }
  }

  let oldValues = {}
  let nextUpdate = {}
  const throttledFlush = throttle(async () => {
    if (defaults) {
      const uploads = await Promise.all(
        Object
          .keys(nextUpdate)
          .filter(name => name in defaults)
          .filter(name => defaults[name] === STORAGE_URL)
          .map(name =>
            onUpload({name, data: nextUpdate[name]})
              .then(uri => ({name, uri}))
          )
      )

      uploads
        .filter(({uri}) => uri)
        .forEach(({name, uri}) => nextUpdate[name] = uri)
    }

    const update = {
      ...nextUpdate,
      timeLastModified: firestore.FieldValue.serverTimestamp(),
    }
    nextUpdate = {}

    const mainField = (!defaults || 'name' in defaults)
      ? 'name'
      : Object.keys(defaults)[0]

    if (update[mainField] === '') {
      const oldName = oldValues[mainField]

      Alert.alert(
        `Deletion of ${oldName}`,
        'Are you sure?',
        [
          {
            text: 'Cancel',
            style: 'cancel',
            onPress() {
              target[mainField] = oldName
            },
          },
          {
            text: 'Delete',
            onPress() {
              try {
                onDelete()
              } catch (error) {
                alert(error)
              }
            },
          },
        ],
      )
    } else {
      onUpdate(update)
    }

    oldValues = {
      ...oldValues,
      ...update
    }
  }, 2000)

  keys
    .forEach(propertyName => observe(target, propertyName, change => {
      oldValues[propertyName] = change.oldValue
      nextUpdate[propertyName] = change.newValue

      throttledFlush()
    }))

  makeProgress(target, defaults)
}