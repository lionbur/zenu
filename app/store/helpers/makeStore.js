import { observable } from 'mobx'
import makeObservable from './makeObservable'
import { variableEx, cleanName } from "./makeSmartPath"

export function makeStore (config) {
  const { initialParams = {}, ...stores } = config

  const params = observable(Object
    .values(stores)
    .map(value => typeof value === 'string'
      ? value.split('/')
      : value.path.split('/'))
    .reduce((result, value) => result.concat(value))
    .filter(value => variableEx.test(value))
    .map(cleanName)
    .reduce((result, key) => ({
      ...result,
      [key]: initialParams[key] || '_',
    }), initialParams))

  const result = {}

  Object
    .entries(stores)
    .forEach(([key, path]) => {
      result[key] = makeObservable(path, params, result, key)
    })
  result.params = params

  return result
}