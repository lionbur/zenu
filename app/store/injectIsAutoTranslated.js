import { inject } from 'mobx-react'
import { isValidLanguage } from "store"

export default inject(({ params }, { data }) => ({
  isAutoTranslated: params.isAdmin && !data.timeLastModified && isValidLanguage(params.translationLanguage),
}))