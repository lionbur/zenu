
import { makeStore } from './helpers'
import config from './config'
import translate from './translate'
import {session} from 'services'
import handleProfile from './profile'
import persist from './persist'

const store = {
  ...makeStore(config),
  session,
  persist,
}

translate(store)
handleProfile(store)

export default store
export isValidLanguage from './isValidLanguage'
export injectIsAutoTranslated from './injectIsAutoTranslated'