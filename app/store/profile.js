import {observe, when, autorun} from 'mobx'
import {Document} from 'services'
import {firestore} from 'firebase'

import {session} from 'services'
import {getRates} from 'fixer'
import translateMenu from "./translateMenu"

export default store => {
  when(
    () => store.session.user,
    async () => {
      const {uid} = store.session.user
      const user = new Document(['users', uid].join('/'), {mode: 'on'})
      await user.ready()

      if (!user.data) {
        user.set({
          timeCreated: firestore.FieldValue.serverTimestamp(),
          isAdmin: true,
        })
      }

      store.params.userId = uid
      store.session.isLoggedIn = !!uid

      if (!store.profile.active) {
        await store.profile.fetch()
      }
      store.params.isAdmin = evaluateIsAdmin()
    }
  )

  function evaluateIsAdmin() {
    if (typeof store.params.overrideIsAdmin === 'boolean') {
      return store.params.overrideIsAdmin
    }

    if (store.profile.data && store.profile.data.isGod) {
      return true
    }

    if (!store.profile.data || !store.profile.data.adminOf) {
      return false
    }

    return store.profile.data.adminOf.includes(store.params.slug)
  }

  when(
    () => store.profile.data,
    () => {
      store.params.isAdmin = evaluateIsAdmin()
    }
  )

  when(
    () => store.shop.data && store.shop.data.currencyCode,
    () => getRates(store.shop.data.currencyCode)
  )

  autorun(
    async () => {
      if (store.params.slug !== '_') {
        await store.profile.ready()
        await store.shop.ready()

        if (!store.profile.data) {
          return
        }

        store.params.isAdmin = evaluateIsAdmin()
        if (store.shop.data.language === store.params.translationLanguageRequest) {
          store.params.translationLanguage = '_'
          store.params.translationLanguageRequest = '_'
        } else {
          translateMenu(store.params.translationLanguage)
        }
      }
    }
  )
}