import React from 'react'

import Container from './Container'
import CopyrightText from './CopyrightText'
import Link from '../Link'

export default () => (
  <Container>
    <CopyrightText>(c) Copyright Translated Menu</CopyrightText>
    <Link
      href="mailto:lion@translated.menu?subject=Translated Menu"
      color="#ccc"
    >Contact Us</Link>
  </Container>
)
