import styled from 'styled-components/native'

export default styled.Text`
  z-index: 1;
  color: #ddd;
  font-family: arial;
  font-size: 12px;
`