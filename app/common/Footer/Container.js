import styled from 'styled-components/native'

export default styled.View`
  background-color: #222;
  height: 30px;
  padding: 5px 20px;
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
`