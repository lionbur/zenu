import styled from 'styled-components/native'

export default styled.Image
  .attrs({
    source: require('../../../images/flags.jpg'),
  })`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`