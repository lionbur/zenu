import styled from 'styled-components/native'

export default styled.Text`
  z-index: 1;
  color: #820;
  font-family: arial;
  font-weight: 600;
  font-size: 48px;
  align-self: center;
  color: white;
  text-shadow-color: black;
  text-shadow-radius: 50px;
  position: absolute;
  top: 0px;
`