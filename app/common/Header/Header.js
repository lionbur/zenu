import React from 'react'

import Container from './Container'
import Image from './Image'
import Title from './Title'

export default () => (
  <Container>
    <Image/>
    <Title>Translated Menu</Title>
  </Container>
)
