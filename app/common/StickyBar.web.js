import React, {Children, cloneElement} from 'react'
import {View} from 'react-native'
import styled from 'styled-components'

const Container = styled.div`
  position: sticky;
  top: 0;
  left: 0;
  right: 0;
  background-color: rgba(255,255,255,0.9);
  box-shadow: 0 0 5px;
  padding: 5px 10px;
  z-index: 1000;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 500px;
  align-self: center;
`

const firstChildStyle = {
  flex: 1,
  marginRight: 2,
}

const childStyle = {
  marginLeft: 2,
  marginRight: 2,
}

export default ({children, ...props}) => (
  <Container>
    {Children.map(children, (child, index) =>
      <View key={index} style={index ? childStyle :firstChildStyle}>
        {child}
      </View>)}
  </Container>
)