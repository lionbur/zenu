import React from 'react'

export default ({ href, color, children, ...rest}) => (
  <a style={{color}} {...{href}} target="_blank" {...rest}>{children}</a>
)