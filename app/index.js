import React from 'react'
import { Provider } from 'mobx-react'
//import DevTools from 'mobx-react-devtools'
import { ThemeProvider } from 'styled-components'

import { AppContainer } from './router'
import App from './App'

import store from "store"
import theme from './theme'

const ClientApp = AppContainer(App)

export default () => (
  <Provider {...store}>
    <ThemeProvider {...{theme}}>
      <ClientApp/>
    </ThemeProvider>
  </Provider>
)
