import React, {Component} from 'react'

import Util from './util'
import Layout from './layout'
import Depot from './depot'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      images: this.props.images,
      figures: Layout[this.props.layout].figures(this.props.width, this.props.images, 0),
      rotationY: 0
    }
  }

  componentWillMount() {
    this.depot = Depot(this.state, this.props, this.setState.bind(this))
    this.onRotate = this.depot.onRotate.bind(this)
    this.autoPlay()
  }

  componentWillReceiveProps (nextProps) {
    this.depot.onNextProps(nextProps)
    this.autoPlay()
  }

  interval = null
  autoPlay() {
    const angle = (2 * Math.PI) / this.state.figures.length

    clearInterval(this.interval)
    this.interval = setInterval(() => this.onRotate(-angle), 3000)
  }

  render() {
    const angle = (2 * Math.PI) / this.state.figures.length
    const translateZ = -Layout[this.props.layout].distance(this.props.width,
      this.state.figures.length)
    const translateY = -0.5 * this.props.height
    const figures = this.state.figures.map((d, i) =>
      (<figure key={i} style={Util.figureStyle(d)}>
          <img src={d.image} alt={i} height="100%" width="100%"/>
      </figure>)
    )

    return (
      <section className='react-3d-carousel'>
        <div
          className='carousel'
          style={{transform: `translateZ(${translateZ}px) translateY(${translateY}px)`}}>
          {figures}
        </div>
      </section>
    )
  }
}

/*
        <div className='prev' onClick={Util.partial(this.onRotate,+angle)}></div>
        <div className='next' onClick={Util.partial(this.onRotate,-angle)}></div>

 */