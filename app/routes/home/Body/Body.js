import React from 'react'

import Container from './Container'
import Text from './Text'
import EnglishButton from './EnglishButton'
import SignUpButton from './SignUpButton'
import SignInButton from './SignInButton'
import Link from '../../../common/Link'

export default ({ onSignUpPress, onEnglishPress, t }) => (
  <Container>
    <EnglishButton {...{t}} onPress={onEnglishPress}/>
    <Link
      href="mailto:lion@translated.menu?subject=Translated Menu"
      color="blue"
    >{t('home:help')}</Link>
    <Text>{t('home:promotional')}</Text>
    <SignUpButton {...{t}} onPress={onSignUpPress}/>
    <SignInButton {...{t}} onPress={onSignUpPress}/>
  </Container>
)
