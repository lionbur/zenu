import styled from 'styled-components/native'

export default styled.Text`
  z-index: 1;
  color: #444;
  font-family: arial;
  font-size: 15px;
  align-self: flex-start;
  padding: 10px 20px;
`