import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  padding-top: 10px;
`

const Button = styled.Button
  .attrs({
    title: ({ t }) => `  ${t('home:signUp')}  `,
  })`
`

export default ({onPress, t}) => (
  <Container>
    <Button {...{onPress, t}}/>
  </Container>
)