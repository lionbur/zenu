import styled from 'styled-components/native'

export default styled.View`
  flex: 1;

  background-color: #eee;
  padding 5px;
  
  align-items: center;
  overflow-y: auto;
  padding-bottom: 20px;
`