import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  padding-top: 10px;
  ${({ t }) => t('home:english') ? '' : 'display: none;'}
`

const Button = styled.Button
  .attrs({
    title: ({ t }) => `${t('home:english')}`,
  })``

export default ({onPress, t}) => (
  <Container {...{t}}>
    <Button {...{onPress, t}}/>
  </Container>
)