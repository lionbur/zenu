import React from 'react'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'

export default ({ navigation: { navigate }, t }) => (
  <Container>
    <Header/>
    <Body
      onSignUpPress={() => navigate('SignUp')} {...{t}}
      onEnglishPress={() => t.i18n.changeLanguage('en')}
    />
    <Footer/>
  </Container>
)