
import { makeStore } from 'store/helpers'
import config from './config'

const store = makeStore(config)

export default store
