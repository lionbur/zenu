import {get, find} from 'lodash'

//import { AUTO_NUMBER, STORAGE_URL } from 'store/helpers'

const initialParams = {
  slug: '_',
}

export default {
  initialParams,
  rooms: {
    path: 'games/word/wheel/:slug/rooms',
    defaults: {
      name: '',
    },
    sub: {
      members: {
        path: 'members',
        defaults: {
          uid: '',
          name: '',
        },
      },
    }
  },
}