import uuid from 'uuid/v4'

export default () => ({
  uid: uuid(),
  displayName: `Guest${Math.round(Math.random() * 1000)}`,
})
