import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'

import Container from './Container'
import PauseButton from './PauseButton'
import LeaveButton from './LeaveButton'

export default @observer class extends Component {
  handlePause = () => {

  }

  handleLeave = () => {

  }

  render () {
    return (
      <Container>
        <PauseButton onPress={this.handlePause}>Pause</PauseButton>
        <LeaveButton onPress={this.handleLeave}>Leave</LeaveButton>
      </Container>
    )
  }
}
