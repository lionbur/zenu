import styled from 'styled-components/native'

export default styled.View`
  padding: 10px 30px;
  background-color: #ccc;
  border-top-width: 1px;
  border-top-style: solid;
  border-top-color: rgba(0,0,0,.5);
  align-items: stretch;
  flex-direction: row;
`