import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  flex: 1;
  margin: 0 5px;
`

const Button = styled.Button`
`

export default ({children, ...rest}) => (
  <Container>
    <Button title={children} {...rest}/>
  </Container>
)
