import styled from 'styled-components/native'

export default styled.Text`
  margin-left: 0.5em;
  font-size: 1.2em;
  
  font-weight: 600;
  align-self: center;
  
  color: rgba(255,255,255,.9);
  text-shadow-offset: 0px 3px;
  text-shadow-radius: 5px;
  text-shadow-color: rgba(0,0,0,.9);
`
