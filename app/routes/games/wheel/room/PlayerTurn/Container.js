import styled from 'styled-components/native'

export default styled.View`
  padding: 1px 30px;
  background-color: ${({isYourTurn}) => isYourTurn ? '#a8a' : '#888' };
  flex-direction: row;
  justify-content: center;
`