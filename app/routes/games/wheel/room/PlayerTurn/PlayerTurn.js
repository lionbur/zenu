import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { get } from 'lodash'

import Container from './Container'
import Title from './Title'
import Player from './Player'

export default @inject('shop') @observer class extends Component {
  render () {
    const { document, members, member } = this.props
    const playerTurn = document && document.data.playerTurn
    const isYourTurn =  playerTurn === get(member, 'id')
    const player = isYourTurn
      ? 'YOURS!'
      : playerTurn && members
        && get(
            members.docs.find(({id}) => id === playerTurn),
            'data.displayName'
           )

    return (
      <Container {...{isYourTurn}}>
        <Title>Who's turn: </Title>
        <Player>{player}</Player>
      </Container>
    )
  }
}
