import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { get } from 'lodash'

import Container from './Container'
import Title from './Title'
import Name from './Name'
import Level from './Level'

export default @inject('shop') @observer class extends Component {
  render () {
    const name = get(this.props.shop, 'data.name')
    const players = get(this.props.members, 'docs.length') || 1
    const step = get(this.props.document, 'data.step')

    return (
      <Container>
        <Title>The Wheel of Fortune</Title>
        <Name>{name}</Name>
        <Level>{players} Player{players > 1 ?'s' : ''} | Level 1 | Step {step}</Level>
      </Container>
    )
  }
}
