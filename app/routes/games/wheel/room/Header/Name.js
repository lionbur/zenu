import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.0em;
  
  font-weight: 600;
  align-self: center;
  
  color: rgba(0,0,0,.75);
  text-shadow-offset: 0px 1px;
  text-shadow-radius: 3px;
  text-shadow-color: rgba(0,0,0,.9);
`
