import styled from 'styled-components/native'

export default styled.Text`
  font-size: 0.75em;
  
  align-self: center;
  
  color: rgba(0,0,0,.5);
  text-shadow-offset: 0px 1px;
  text-shadow-radius: 2px;
  text-shadow-color: rgba(0,0,0,.5);
`
