import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.5em;
  
  font-weight: 600;
  align-self: center;
  
  color: rgba(255,255,255,.75);
  text-shadow-offset: 0px 2px;
  text-shadow-radius: 5px;
  text-shadow-color: rgba(0,0,0,.9);
`
