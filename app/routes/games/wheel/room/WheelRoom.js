import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import { NavigationActions } from 'react-navigation'
import { get, omit, uniq, pick, find } from 'lodash'
import {firestore} from 'firebase'
import {Document, Collection} from 'services'
import newGuest from '../newGuest'
import Modal from 'react-native-web-modal'
import {ActivityIndicator} from 'react-native'

import Container from './Container'
import Header from './Header'
import SubHeader from './SubHeader'
import PlayerTurn from './PlayerTurn'
import Body from './Body'
import Footer from './Footer'
import Join from './Join'

import levels, {flattenDecks} from './data'

const gameName = 'Wheel of Fortune'

export default
@inject('shop')
@inject('params')
@inject('session')
@inject('persist')
@observer
class extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params: { slug } } } = navigation

    return {
      title: `${slug} | ${gameName}`,
    }
  }

  @observable isReady = false
  @observable document = null
  @observable members = null
  @observable member = null

  async componentWillMount() {
    const { navigation:{ state:{params: { slug, room }}}} = this.props
    const { params, shop } = this.props

    params.slug = decodeURIComponent(slug)

    if (shop.active) {
      await shop.ready()
    } else {
      await shop.fetch()
    }

    if (typeof document !== 'undefined') {
      document.title = this.props.shop.data
        ?`${this.props.shop.data.name} | ${gameName}`
        : gameName
    }

    let player = get(this.props.persist, 'player')
    if (!player) {
      player = newGuest()
      this.props.persist.player = player
    }

    const roomDoc = new Document(
      ['games', 'word', 'wheel', params.slug, 'rooms', room].join('/')
    )
    await roomDoc.fetch()

    if (!roomDoc.snapshot.exists) {
      this.props.navigation.navigate('Wheel', { slug })
    }

    this.document = roomDoc

    const members = new Collection(
      [roomDoc.path, 'members'].join('/')
    )
    await members.fetch()

    console.log('player', player)
    this.member = members
      .docs
      .find(({id}) => id === player.uid)

    this.members = members

    if (this.member) {
      this.startPing()

      if (!this.member.data.timeLastPlayed) {
        await this.updateTimeLastPlayed()
      }
    }

    this.isReady = true
  }

  handleJoin = async () => {
    const { player } = this.props.persist
    const member = new Document([this.document.path, 'members', player.uid].join('/'), 'on')

    await member.set({
      timeJoined: firestore.FieldValue.serverTimestamp(),
      ...omit(player, 'uid'),
    })

    this.member = member
    this.startPing()
    await this.updateTimeLastPlayed()
  }

  pingInterval = null

  startPing() {
    this.ping()
    this.pingInterval = setInterval(this.ping, 10 * 1000)
  }

  stopPing() {
    clearInterval(this.pingInterval)
    this.pingInterval = null
  }

  componentWillUnmount() {
    this.stopPing()
  }

  ping = async () => {
    const pingCount = (get(this.document, 'data.pingCount') || 0) + 1

    await this.member.update({
      pingCount,
    })

    await this.keepAlive(pingCount)
    await this.manageGame()
  }

  async keepAlive(pingCount) {
    await this.members.ready()

    const disconnected = this.members
      .docs
      .filter(({data}) => !data.pingCount
        || (pingCount - data.pingCount > 5))
    const connected = this.members
      .docs
      .filter(doc => !disconnected.includes(doc))
      .sort((a, b) => a.data.timeJoined - b.data.timeJoined)

    if ((!this.document.data.operator
        || disconnected.find(({id}) => id === this.document.data.operator))
      && connected[0].id === this.member.id) {
      this.document.update({
        operator: this.member.id
      })
    }

    await Promise.all(
      disconnected
        .map(doc => doc.delete())
    )
  }

  async componentWillReact() {
    await this.manageGame()
  }

  async manageGame() {
    if (!this.document) {
      return
    }
    if (!this.member) {
      return
    }
    if (this.document.data.operator !== this.member.id) {
      return
    }

    await this.managePing()
    await this.managePlayerTurn()

    if (!this.document.data.step) {
      await this.manageStep()
    }
  }

  async managePlayerTurn() {
    const player = this.members.docs.find(({id}) => id === this.document.data.playerTurn)

    if (!player
      || (player.data.pingCount - (player.data.pingCountOnTurnStarted || 0) > 5))
    {
      await this.manageTurnPassed()
    }
  }

  async manageTurnPassed() {
    const members = this.members
      .docs
      .sort((a, b) => a.data.timeLastPlayed - b.data.timeLastPlayed)
    const nextPlayer = members[0]

    await nextPlayer.update({
      pingCountOnTurnStarted: nextPlayer.data.pingCount
    })
    await this.document.update({
      playerTurn: nextPlayer.id,
    })
  }

  async managePing() {
    const pingCount = (get(this.document, 'data.pingCount') || 0) + 1

    await this.document.update({
      pingCount,
    })
  }

  level = 1
  manageStep = async () => {
    const step = (this.document.data.step || 0) + 1
    const {steps, hand} = levels[this.level - 1]
    const {english, decks, numMissingCards, handSize} = steps[step - 1]
    const cards = flattenDecks(decks)

    const pool = new Array(cards.length)
      .fill(null)
      .map((_, index) => index)
    const missingCardsIndices = new Array(numMissingCards)
      .fill(null)
      .map(_ => pool.splice(Math.round(Math.random() * pool.length), 1)[0])

    const set = cards
      .map(({text, subText}, index) => ({
        text,
        subText,
        isFlipped: missingCardsIndices.includes(index),
      }))
    const setTexts = set
      .map(({text}) => text)
    const setSubTexts = set
      .map(({subText}) => subText)

    const fullHand = uniq(hand
      .filter(({subText}) => !setSubTexts.includes(subText))
      .filter(({text}) => !setTexts.includes(text))
      .map(({text, subText}) => ({
        text,
        subText,
        isFlipped: false,
      })))
      .slice(0, handSize - 1)

    missingCardsIndices
      .map(index => cards[index])
      .forEach(card => fullHand.splice(
        Math.round(Math.random() * (fullHand.length + 1)),
        0,
        {
          ...pick(card, ['text', 'subText']),
          isFlipped: false,
        },
      ))

    let cardIndex = 0
    const decksSet = decks
      .map(deck => ({
        cards: deck
          .map(_ => set[cardIndex++])
      }))

    console.log(fullHand, pool, decksSet)

    await this.document.update({
      step,
      hint: english,
      decks: decksSet,
      hand: fullHand,
    })

    await this.manageTurnPassed()
  }

  updateTimeLastPlayed = async () => {
    await this.member.update({
      timeLastPlayed: firestore.FieldValue.serverTimestamp(),
    })
  }

  handleAction = async () => {
    await this.updateTimeLastPlayed()
  }

  render () {
    const { document, members, member } = this

    return (
      <Container>
        <Modal
          visible={!this.isReady}
        >
          <ActivityIndicator/>
        </Modal>
        <Modal
          visible={this.isReady && !this.member}
        >
          <Join {...{document}} onJoin={this.handleJoin}/>
        </Modal>
        <Header {...{members, document}} />
        {member && <SubHeader {...{document}}/>}
        <PlayerTurn
          {...{document, members, member}}
        />
        <Body
          onManageStep={this.manageStep}
          onAction={this.handleAction}
          {...{document}}
        />
      </Container>
    )
  }
}
