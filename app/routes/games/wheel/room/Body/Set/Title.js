import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.5em;
  align-self: center;  
  color: rgba(255,255,255,.9);
  background-color: rgba(0,0,0,.25);
  width: 100%;
  text-align: center;
  line-height: 2em;
  margin-bottom: 10px;
  border-radius: 5px;
`
