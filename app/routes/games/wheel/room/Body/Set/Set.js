import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import {get, isEqual} from 'lodash'

import Container from './Container'
import Title from './Title'
import Deck from '../Deck.web'
import Card from '../Card/index'

import levels from '../../data/index'

const sleep = msec => new Promise(resolve =>
  setTimeout(resolve, msec))

export default @observer class extends Component {
  @observable flipOverrides = []
  @observable glowingCards = []
  step = 0

  update() {
    const decks = get(this.props.document, 'data.decks')

    if (decks && this.props.document.data.step > this.step) {
      this.flipOverrides.length = decks
        .reduce((result, deck) => result + deck.cards.length, 0)

      this.flipOverrides
        .fill(true)

      this.glowingCards.length = this.flipOverrides.length
      this.glowingCards
        .fill(false)

      this.step = this.props.document.data.step

      this.startReveal()
    }
  }

  revealInterval = null
  startReveal = () => {
    this.isStepStarted = true
    this.stopReveal()
    this.lastRevealedCardIndex = -1
    this.revealInterval = setInterval(this.revealNextCard, 500)
  }

  stopReveal = () => {
    clearInterval(this.revealInterval)
    this.revealInterval = null
  }

  lastRevealedCardIndex = -1
  revealNextCard = () => {
    this.lastRevealedCardIndex++

    if (this.lastRevealedCardIndex >= this.flipOverrides.length) {
      this.stopReveal()
    } else {
      this.flipOverrides[this.lastRevealedCardIndex] = false
    }
  }
/*
  componentWillReceiveProps(nextProps) {
    const decks = get(this.props.document, 'data.decks')
    const nextDecks = get(nextProps.document, 'data.decks')

    if (!isEqual(decks, nextDecks)) {
      this.update(nextDecks)
    }
  }

  componentWillMount() {
    this.update(get(this.props.document, 'data.decks'))
  }
*/
  componentWillReact() {
    if (this.isGlowing) {
      return
    }
    this.update()

    if (!this.isStepStarted) {
      return
    }

    const numFlippedCards = this.props.document.data.decks
      .reduce((result, deck) => result + deck
          .cards
          .filter(({isFlipped}) => isFlipped)
          .length,
        0)

    if (0 === numFlippedCards) {
      this.glowEffect()
    }
  }

  setGlowPattern(mask) {
    for (let i = 0; i < this.glowingCards.length; i++) {
      this.glowingCards[i] = (((i % 2) + 1) & mask) > 0
    }
  }

  isGlowing = false
  isStepStarted = false

  async glowEffect() {
    if (this.isGlowing) {
      return
    }

    this.isGlowing = true

    const pattern = [
      1, 2, 1, 2, 1, 2,
      1, 2, 1, 2, 1, 2,
      1, 2, 1, 2, 1, 2,
      1, 2, 1, 2, 1, 2,
      3, 0, 3, 0, 3, 0,
      3, 0, 3, 0, 3, 0
    ]

    for (const state of pattern) {
      this.setGlowPattern(state)
      await sleep(100)
    }

    this.props.document.data.decks
      .forEach(deck => deck.cards
        .forEach(card => card.isFlipped = true))

    this.isGlowing = false
    this.isStepStarted = false
    await sleep(1000)

    this.props.onManageStep()
  }

  render () {
    const { document: doc } = this.props
    let cardIndex = 0

    if (doc && doc.data.step && this.flipOverrides.length) {
      return (
        <Container>
          <Title>{doc.data.hint}</Title>
          {doc.data.decks.map((deck, index) => (
            <Deck key={index}>{deck
              .cards
              .map(card => ({
                ...card,
                isFlipped: card.isFlipped || this.flipOverrides[cardIndex],
                isGlowing: this.glowingCards[cardIndex],
                key: cardIndex++,
              }))
              .map(card => <Card {...card}/>)
            }</Deck>
          ))}
        </Container>)
    }

    return null
  }
}
