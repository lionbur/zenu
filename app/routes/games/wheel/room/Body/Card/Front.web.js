import styled from 'styled-components'

export default styled.div`
  width: 35px;
  height: 35px;
  padding: 5px;
  border-radius: 5px;
  border: 2px solid rgba(0,0,0,.75);
  backface-visibility: hidden;
  background-color: #eee;
  color: rgba(0,0,0,.9);
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
	z-index: 2;
	/* for firefox 31 */
	transform: rotateY(0deg);
`