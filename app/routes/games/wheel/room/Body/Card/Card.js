import React, {Component} from 'react'

import Container from './Container'
import Flipper from './Flipper'
import Front from './Front'
import Back from './Back'
import Text from './Text.web'
import SubText from './SubText.web'

const straightStyle = {}
const flippedStyle = {
  transform: 'rotateY(180deg)',
}
const noGlowStyle = {}
const glowStyles = {
  boxShadow: '0 0 50px yellow',
  backgroundColor: 'yellow',
}

export default class extends Component {
  componentDidMount() {
    setTimeout(() => this.isFlipped = false, Math.random() * 5000)
  }

  render() {
    const {text, subText, isFlipped, isGlowing, onPress} = this.props

    return (
      <Container {...{onPress}}>
        <Flipper style={
          isFlipped ? flippedStyle : straightStyle
        }>
          <Front style={
            isGlowing ? glowStyles : noGlowStyle
          }>
            <Text>{text}</Text>
            <SubText>{subText}</SubText>
          </Front>
          <Back/>
        </Flipper>
      </Container>
    )
  }
}
