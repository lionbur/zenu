import styled from 'styled-components'

export default styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 35px;
  height: 35px;
  padding: 5px;
  border-radius: 5px;
  border: 2px solid rgba(0,0,0,.75);
  backface-visibility: hidden;
  background-color: #404;
  color: rgba(0,0,0,.9);
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
	transform: rotateY(180deg);
`