import styled from 'styled-components'

export default styled.div`
  font-size: 1.0em;
  color: rgba(0,0,0,.75);
  text-align: center;
`
