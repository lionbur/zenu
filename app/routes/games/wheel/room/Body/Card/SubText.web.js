import styled from 'styled-components'

export default styled.div`
  font-size: 0.8em;
  color: rgba(0,0,0,.5);
  text-align: center;
`
