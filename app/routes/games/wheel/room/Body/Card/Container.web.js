import React from 'react'
import styled from 'styled-components/native'

const Static = styled.View``
const Interactive = styled.TouchableOpacity``

export default ({onPress, ...rest}) => onPress
  ? <Interactive {...{onPress}} {...rest}/>
  : <Static {...rest}/>
