import styled from 'styled-components'

export default styled.div`
  transition: 1s;
  transform-style: preserve-3d;
  position: relative;
`