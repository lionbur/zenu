import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.5em;
  align-self: center;  
  color: rgba(0,0,0,.75);
`
