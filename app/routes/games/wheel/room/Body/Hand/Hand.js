import React, { Component } from 'react'
import { observable, toJS } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import {get, isEqual} from 'lodash'

import Container from './Container'
import Title from './Title'
import Deck from '../Deck.web'
import Card from '../Card/index'

import levels from '../../data/index'

const flattenDataDecks = decks => decks
  .reduce((result, deck) => [
    ...result,
    ...deck.cards,
  ], [])

export default @observer class extends Component {
  @observable flipOverrides = []
  step = 0

  update() {
    const hand = get(this.props.document, 'data.hand')

    if (hand && this.props.document.data.step > this.step) {
      this.flipOverrides.length = hand.length

      this.flipOverrides
        .fill(true)

      this.step = this.props.document.data.step
      this.startReveal()
    }
  }

  revealInterval = null
  startReveal = () => {
    this.lastRevealedCardIndex = -1
    this.stopReveal()
    this.revealInterval = setInterval(this.revealNextCard, 250)
  }

  stopReveal = () => {
    clearInterval(this.revealInterval)
    this.revealInterval = null
  }

  lastRevealedCardIndex = -1
  revealNextCard = () => {
    this.lastRevealedCardIndex++

    if (this.lastRevealedCardIndex >= this.flipOverrides.length) {
      this.stopReveal()
    } else {
      this.flipOverrides[this.lastRevealedCardIndex] = false
    }
  }
/*
  componentWillReceiveProps(nextProps) {
    const hand = get(this.props.document, 'data.hand')
    const nextHand = get(nextProps.document, 'data.hand')

    if (!isEqual(hand, nextHand)) {
      this.update(nextHand)
    }
  }

  componentWillMount() {
    this.update(get(this.props.document, 'data.hand'))
  }
*/
  componentWillReact() {
    this.update()
  }

  handleCardPress = card => {
    const decks = toJS(get(this.props.document, 'data.decks'))
    console.log(decks)
    const flippedCards = flattenDataDecks(decks)
      .filter(({isFlipped}) => isFlipped)
    const matchingCard = flippedCards.find(({text, subText}) =>
      text == card.text && subText === card.subText)

    if (matchingCard) {
      matchingCard.isFlipped = false

      this.props.document.update({
        decks,
      })
    }

    const hand = get(this.props.document, 'data.hand')
      .map(handCard => ({
        ...handCard,
        isFlipped: handCard.text === card.text && handCard.subText === card.subText
          ? true
          : handCard.isFlipped,
      }))

    this.props.document.update({
      hand,
    })

    this.props.onAction('flip')
  }

  render () {
    const hand = get(this.props.document, 'data.hand')

    if (hand && this.flipOverrides.length) {
      return (
        <Container>
          <Deck wrap>
            {hand
              .map((card, index) => ({
                ...card,
                isFlipped: card.isFlipped || this.flipOverrides[index],
              }))
              .map((card, index) => <Card
                key={index}
                onPress={() => {
                  if(!card.isFlipped) {
                    this.handleCardPress(card)
                  }
                }}
                {...card}
              />)}
          </Deck>
        </Container>)
    }

    return null
  }
}
