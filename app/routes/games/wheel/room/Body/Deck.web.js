import styled from 'styled-components'

export default styled.div`
  padding: 5px;
  display: flex;
  flex-direction: row;
  perspective: 800px;
  direction: rtl;
  align-items: center;
  justify-content: center;
  ${({wrap}) => wrap ?'flex-wrap: wrap;' :''}
`