import React from 'react'

import Container from './Container'
import Set from './Set'
import Hand from './Hand'

export default props => (
  <Container>
    <Set {...props}/>
    <Hand {...props}/>
  </Container>
)
