import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { get } from 'lodash'

import Container from './Container'
import Title from './Title'
import Password from './Password'

export default @inject('shop') @observer class extends Component {
  render () {
    const { document } = this.props

    return (
      <Container>
        <Title>Password for new players:</Title>
        <Password>{document && document.data.password}</Password>
      </Container>
    )
  }
}
