import styled from 'styled-components/native'

export default styled.View`
  padding: 1px 30px;
  background-color: purple;
  flex-direction: row;
  justify-content: center;
`