import {uniqBy} from 'lodash'

import level1 from './level1'

const decksToCards = decks => decks
  .reduce((result, deck) => [
    ...result,
    ...deck
  ])


const process = data => ({
  steps: data.steps.map(processStep),
  hand: processHand(data.steps),
})

const processHand = steps =>
  uniqBy(steps
    .reduce((result, step) => [
      ...result,
      ...decksToCards(processStep(step).decks)
    ], [])
    .filter(({text}) => 'text' !== ' '), 'text')

function processStep(step) {
  const {hebrew, hibrish, english, numMissingCards, handSize} = step
  const hebrewLetters = Array
    .from(hebrew)
  const hibrishLetters = hibrish
    .replace(/\s/g, '- -')
    .split('-')

  let deck = []
  const decks = []

  hebrewLetters
    .forEach((letter, index) => {
      const card = {
        text: letter,
        subText: hibrishLetters[index],
      }

      if (card.text !== ' ') {
        deck.push(card)
      }
      if ((index >= hebrewLetters.length - 1) || (card.text === ' ')) {
        decks.push(deck)
        deck = []
      }
    })

  return {
    decks,
    english,
    numMissingCards,
    handSize
  }
}

export const flattenDecks = decks => decks
  .reduce((result, deck) => [
    ...result,
    ...deck
  ])

export default [
  process(level1),
]