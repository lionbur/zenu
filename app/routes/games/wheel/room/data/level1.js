const steps = [
  {
    hebrew: 'שלום',
    hibrish: 'SHa-L-O-M',
    english: 'Hello!',
    numMissingCards: 1,
    handSize: 5,
  }, {
    hebrew: 'מלצר',
    hibrish: 'Me-L-TZa-R',
    english: 'Waiter',
    numMissingCards: 1,
    handSize: 8,
  }, {
    hebrew: 'מלצרית',
    hibrish: 'Me-L-TZa-R-I-T',
    english: 'Waitress',
    numMissingCards: 2,
    handSize: 10,
  }, {
    hebrew: 'בתיאבון',
    hibrish: 'Be-T-(e)I-A-V-O-N',
    english: 'Enjoy your meal!',
    numMissingCards: 2,
    handSize: 12,
  }, {
    hebrew: 'תודה רבה',
    hibrish: 'T-O-Da-H Ra-Ba-H',
    english: 'Thank you very much!',
    numMissingCards: 3,
    handSize: 12,
  }, {
    hebrew: 'אין בעד מה',
    hibrish: '(a)-eI-N Be-A-D Ma-H',
    english: 'You\'re welcome!',
    numMissingCards: 3,
    handSize: 15,
  }, {
    hebrew: 'טעים מאוד',
    hibrish: 'Ta-(a)-I-M Me-(a)-O-D',
    english: 'Very tasty!',
    numMissingCards: 4,
    handSize: 15,
  }, {
    hebrew: 'סלח לי',
    hibrish: 'S-La-Kh L-I',
    english: 'Excuse me',
    numMissingCards: 4,
    handSize: 16,
  }, {
    hebrew: 'חשבון בבקשה',
    hibrish: 'KHe-SH-B-O-N Be-Va-Ka-SHa-H',
    english: 'Check please',
    numMissingCards: 5,
    handSize: 16,
  }, {
    hebrew: 'להתראות',
    hibrish: 'Le-Hi-T-R-A-O-T',
    english: 'See you!',
    numMissingCards: 6,
    handSize: 16,
  },
]

export default {
  steps,
}