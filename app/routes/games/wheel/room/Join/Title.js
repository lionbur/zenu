import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.5em;
  font-weight: 600;
  align-self: center;
  color: rgba(0,0,0,.75);
  margin-bottom: 0.5em;
`
