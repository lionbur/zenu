import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { get } from 'lodash'

import Container from './Container'
import Title from './Title'
import Label from './Label'
import PasswordInput from './PasswordInput'

export default @observer class extends Component {
  handlePasswordInput = password => {
    if (this.props.document.data.password == password) {
      this.props.onJoin()
    }
  }

  render () {
    return (
      <Container>
        <Title>Join the Game</Title>
        <Label>Please enter password:</Label>
        <PasswordInput
          onChangeText={this.handlePasswordInput}
        />
      </Container>
    )
  }
}
