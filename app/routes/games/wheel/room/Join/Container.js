import styled from 'styled-components'

export default styled.div`
  padding: 5px 30px 10px 30px;
  background-color: #eee;
  border-radius: 5px;
  box-shadow: 0px 0px 20px rgba(0,0,0,.9);
  display: flex;
  flex-direction: column;
`