import styled from 'styled-components/native'

export default styled.TextInput
  .attrs({
    placeholder: '__',
    keyboardType: 'numeric',
    maxLength: 2,
    returnKeyType: 'done',
  })`
  font-size: 2em;
  text-align: center;
  
  align-self: center;
  
  color: rgba(0,0,0,.75);
  background-color: rgba(0,0,0,.25);
  border-radius: 5px;
  padding: 5px;
  margin-top: 10px;
  max-width: 4em;
`
