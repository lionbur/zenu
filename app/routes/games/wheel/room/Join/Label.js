import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.0em;
  
  font-weight: 600;
  align-self: center;
  
  color: rgba(0,0,0,.75);
`
