import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'

import Container from './Container'
import JoinButton from './JoinButton'
import LeaveButton from './LeaveButton'

export default @observer class extends Component {
  @observable isLocked = false

  handleJoin = () => {
    this.isLocked = true
    this.props.onNewGame()
  }

  render () {
    return (
      <Container>
        <JoinButton
          disabled={this.isLocked}
          onPress={this.handleJoin}
        >New Game</JoinButton>
        <LeaveButton
          disabled={this.isLocked}
        >Leave</LeaveButton>
      </Container>
    )
  }
}
