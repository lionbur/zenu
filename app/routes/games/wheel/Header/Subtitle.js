import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.0em;
  font-weight: 600;
  
  align-self: center;
  
  color: #eee;
  text-shadow-offset: 0px 5px;
  text-shadow-radius: 3px;
  text-shadow-color: rgba(0,0,0,.9);
`
