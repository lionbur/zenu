import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { get } from 'lodash'

import Container from './Container'
import Title from './Title'
import Subtitle from './Subtitle'
import Name from './Name'

export default @inject('shop') @observer class extends Component {
  render () {
    const name = get(this.props.shop, 'data.name')

    return (
      <Container>
        <Subtitle>Play the game</Subtitle>
        <Title>The Wheel of Fortune</Title>
        <Name>{name}</Name>
      </Container>
    )
  }
}
