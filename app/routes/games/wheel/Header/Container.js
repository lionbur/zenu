import styled from 'styled-components/native'

export default styled.View`
  padding: 30px;
  border-bottom: 3px solid rgba(0,0,0,.5);
  background-image: url(${require('../assets/spinning.gif')});
  background-position: center center;
  background-size: cover;
`