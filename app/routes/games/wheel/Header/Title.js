import styled from 'styled-components/native'

export default styled.Text`
  font-size: 2.0em;
  
  font-weight: 600;
  align-self: center;
  
  color: white;
  text-shadow-offset: 0px 10px;
  text-shadow-radius: 0.5em;
  text-shadow-color: rgba(0,0,0,.9);
`
