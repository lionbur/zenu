import styled from 'styled-components/native'

export default styled.View`
  flex: 1;
  padding: 20px 30px;
  background-color: #eee;
`