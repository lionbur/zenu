import styled from 'styled-components/native'

export default styled.Text`
  font-size: 1.2em;
  
  color: rgba(0,0,0,.75);
`
