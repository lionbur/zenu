import styled from 'styled-components/native'

export default styled.TouchableOpacity`
  margin-top: 1em;
  border-radius: 5px;
  padding: 5px 10px;
  border: 1px solid rgba(0,0,0,.5);
  background-color: rgba(128,160,255,.5);
`