import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'

import {Collection} from 'services'
import Container from './Container'
import Name from './Name'
import TimeCreated from './TimeCreated'

export default @observer class extends Component {
  @observable members = null

  async componentWillMount() {
    const members = new Collection([this.props.document.path, 'members'].join('/'))
    this.members = await members.fetch()
  }

  handlePress = () => {
    this.props.onEnterRoom(this.props.document.id)
  }

  render () {
    const { members } = this
    const { document:{data} } = this.props

    if (!members) {
      return null
    }

    return (
      <Container
        onPress={this.handlePress}
      >
        <Name>{members
          .docs
          .map(({data:{displayName}}) => displayName)
          .join(', ')
        }</Name>
        <TimeCreated>Started {moment(data.timeCreated).fromNow()}</TimeCreated>
      </Container>
    )
  }
}
