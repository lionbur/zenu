import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'

import Container from './Container'
import Title from './Title'
import Room from './Room'

export default @observer class extends Component {
  render () {
    const { rooms, onEnterRoom } = this.props

    return (
      <Container>
        <Title>You can join any of the games below:</Title>
        {rooms && rooms.docs.map(document => (
            <Room key={document.id} {...{document, onEnterRoom}}/>
          ))}
      </Container>
    )
  }
}
