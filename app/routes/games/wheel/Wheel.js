import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import { NavigationActions } from 'react-navigation'
import { get, omit } from 'lodash'
import {firestore} from 'firebase'
import {Collection, Document} from 'services'

import store from './store'

import Container from './Container'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'
import newGuest from './newGuest'

const gameName = 'Wheel of Fortune'

export default @inject('shop') @inject('params') @inject('session') @inject('persist') @observer class extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params: { slug } } } = navigation

    return {
      title: `${slug} | ${gameName}`,
    }
  }

  @observable rooms = null

  async cleanup(slug, hoursAgo) {
    const oldRooms = new Collection(
      ['games', 'word', 'wheel', slug, 'rooms'].join('/')
    )
    oldRooms.query = oldRooms.ref
      .where('timeCreated', '<', hoursAgo)
    await oldRooms.fetch()

    const oldMemberColls = await Promise.all(
      oldRooms
        .docs
        .map(doc => new Collection([doc.path, 'members'].join('/')))
        .map(coll => coll.fetch())
    )

    const oldMembers = oldMemberColls
      .reduce((result, coll) => result.concat(coll.docs), [])

    return Promise.all(
      oldRooms
        .docs
        .concat(...oldMembers)
        .map(doc => doc.delete())
    )
  }

  async componentWillMount() {
    const { navigation: { state: { params: { slug } } } } = this.props
    const { params, shop } = this.props

    params.slug = decodeURIComponent(slug)

    if (shop.active) {
      await shop.ready()
    } else {
      await shop.fetch()
    }

    if (typeof document !== 'undefined') {
      document.title = this.props.shop.data
        ?`${this.props.shop.data.name} | ${gameName}`
        : gameName
    }

    store.params.slug = params.slug

    const hoursAgo = new Date(Date.now() - 4 * 60 * 60 * 1000)
//    const hoursAgo = new Date(Date.now() - 60 * 1000)

    await this.cleanup(params.slug, hoursAgo)

    const rooms = new Collection(
      ['games', 'word', 'wheel', params.slug, 'rooms'].join('/')
    )
    rooms.query = rooms.ref
      .where('timeCreated', '>=', hoursAgo)
    await rooms.fetch()

    this.rooms = rooms

    if (!this.rooms.docs.length) {
      await this.createRoom()
    }
  }

  createRoom = async () => {
    let player = get(this.props.persist, 'player')
    if (!player) {
      player = newGuest()
      this.props.persist.player = player
    }

    const room = await this.rooms.add({
      timeCreated: firestore.FieldValue.serverTimestamp(),
      password: 10 + Math.round(Math.random() * 90),
      playerTurn: player.uid,
    })

    const newMember = new Document([room.path, 'members', player.uid].join('/'), 'on')

    await newMember.set({
      timeJoined: firestore.FieldValue.serverTimestamp(),
      ...omit(player, 'uid'),
    })

    return room
  }

  handleEnterRoom = room => {
    const { slug } = this.props.navigation.state.params

    this.props.navigation.navigate('WheelRoom', { slug, room })
  }

  handleNewGame = async () => {
    const {id} = await this.createRoom()
    this.handleEnterRoom(id)
  }

  render () {
    const { rooms } = this

    return (
      <Container>
        <Header/>
        <Body
          {...{rooms}}
          onEnterRoom={this.handleEnterRoom}
        />
        <Footer
          onNewGame={this.handleNewGame}
        />
      </Container>
    )
  }
}
