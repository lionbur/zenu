import React from 'react'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'

export default ({ navigation: { dispatch, state: { params: { slug } } } }) => (
  <Container>
    <Header/>
    <Body {...{slug, dispatch}}/>
    <Footer/>
  </Container>
)
