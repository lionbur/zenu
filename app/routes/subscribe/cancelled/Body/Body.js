import React, {Component} from 'react'
import {Button} from 'react-native'
import { NavigationActions } from 'react-navigation'

import Container from './Container'
import Text from './Text'
import {Link} from 'styled'

export default ({slug, dispatch}) => (
  <Container>
    <Text>
      You have chosen not to subscribe at this stage
    </Text>
    <Button
      title="Go to menu"
      onPress={() => dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Restaurant',
            params: {
              slug
            }
          })
        ]
      }))
      }
    />
  </Container>
)
