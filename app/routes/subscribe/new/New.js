import React, {Component} from 'react'
import {ActivityIndicator} from 'react-native'
import {observer} from 'mobx-react'
import {observable} from 'mobx'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'
import Modal from 'react-native-web-modal'

export default @observer class extends Component {
  @observable isSubmitting = false

  handleSubmit = () => {
    this.isSubmitting = true
  }

  render () {
    const { navigation: { state: { params: { slug } } } } = this.props

    return (
      <Container>
        <Modal visible={this.isSubmitting}>
          <ActivityIndicator/>
        </Modal>
        <Header/>
        <Body {...{slug}} onSubmit={this.handleSubmit}/>
        <Footer/>
      </Container>
    )
  }
}
