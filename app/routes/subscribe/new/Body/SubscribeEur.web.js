import React from 'react'

export default ({slug, onSubmit}) => (
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" {...{onSubmit}}>
    <input type="hidden" name="cmd" value="_s-xclick"/>
    <input type="hidden" name="hosted_button_id" value="SDFVQAGGY299Q"/>
    <input type="hidden" name="on0" value="Select Plan"/>
    <input type="hidden" name="return" value={`https://translated.menu/of/${slug}/subscribe/success`}/>
    <input type="hidden" name="cancel_return" value={`https://translated.menu/of/${slug}/subscribe/cancelled`}/>
    <table>
      <tr>
        <td>
          Select a Plan Please
        </td>
      </tr>
      <tr>
        <td>
          <select name="os0">
            <option value="Monthly">€25.00 EUR - monthly</option>
            <option value="Yearly">€250.00 EUR - yearly</option>
          </select>
        </td>
      </tr>
    </table>
    <input type="hidden" name="currency_code" value="RUB"/>
    <input type="image" src="https://www.paypalobjects.com/en_US/IL/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"/>
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"/>
  </form>

)


