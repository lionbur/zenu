import React, {Component} from 'react'
import {firestore} from 'firebase'
import {observable} from 'mobx'
import {observer} from 'mobx-react'

import Container from './Container'
import SubscribeEur from './SubscribeEur'
import SubscribeRussia from './SubscribeRussia'

const buttons = {
  EUR: SubscribeEur,
  RUB: SubscribeRussia,
}

export default @observer class extends Component {
  @observable currencyCode = null
  button = null

  async componentWillMount() {
    const { slug } = this.props

    const { currencyCode } = (await firestore().doc(['restaurants', slug].join('/')).get()).data()
    this.currencyCode = currencyCode
  }

  render() {
    const { slug, onSubmit } = this.props
    const Button = buttons[this.currencyCode || 'EUR']

    return (
      <Container>
        <Button {...{slug, onSubmit}}/>
      </Container>
    )
  }
}