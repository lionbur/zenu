import React from 'react'
import UserMode from './UserMode'
import {observer, inject} from 'mobx-react'
import {compose} from 'recompose'
import {get} from 'lodash'
import {View, Button} from 'react-native'

const DemoMode = ({t, params, isInDemoMode}) => (
  (isInDemoMode || params.isAdmin)
    ? <View>
      <UserMode>{
        params.isAdmin
          ? t('restaurant:admin')
          : t('restaurant:guest')
      }</UserMode>
      <Button
        color={params.isAdmin
          ? "blue"
          : "green"}
        title={params.isAdmin
          ? t('restaurant:guestButton')
          : t('restaurant:adminButton')}
        onPress={() => {
          params.overrideIsAdmin = !params.overrideIsAdmin
          if (!params.overrideIsAdmin) {
            params.translationLanguageRequest = 'en'
          } else {
            params.translationLanguageRequest = '_'
          }
        }}
      />
    </View>
    : null
)

const enhance = compose(
  inject(({shop, params}) => ({
    params,
    isInDemoMode: get(shop, 'data.isInDemoMode'),
  })),
  observer,
)

export default enhance(DemoMode)