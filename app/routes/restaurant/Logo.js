import styled from 'styled-components/native'

import { ImageField } from 'styled'

export default styled(ImageField)
  .attrs({
    name: 'logo',
    placeholder: ({t}) => t('restaurant:logo'),
  })`
  width: 100px;
  height: 100px;
`
