import React, { Component } from 'react'
import styled from 'styled-components/native'
import { action, observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator, Picker, Text } from 'react-native'
import {get, find} from 'lodash'

import Modal from 'react-native-web-modal'
import Container from './Container'
import Header from './Header'
import HeaderImage from './HeaderImage'
import Name from './Name'
import Categories from './Categories'
import { getLanguages } from 'translate'
import { Label } from 'styled'
import Section from './Section'
import Gallery from './Gallery'
import Row from './Row'
import Column from './Column'
import LogoColumn from './LogoColumn'
import Logo from './Logo'
import Claim from './Claim'
import {StickyBar} from 'common'
import CategoryLinks from './CategoryLinks'
import DemoMode from './DemoMode'
import Translation from './Translation'
import Details from './Details'

let app
if (process.env.CONFIG === 'fb') {
  app = require('../../../app2')
} else {
  app = require('../../../app')
}

const Button = styled.Button.attrs({
  color: ({theme}) => theme.button.backgroundColor,
})``;

const storedLanguage = localStorage.getItem('language')

const absoluteOffsetTop = element => element.offsetParent
  ? absoluteOffsetTop(element.offsetParent) + element.offsetTop
  : element.offsetTop

export default @inject('shop') @inject('params') @inject('profile') @observer class extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params: { slug } } } = navigation

    return {
      title: `${slug} | ${app.displayName}`,
    }
  }

  @observable isTranslationModalVisible = !storedLanguage
  @observable isDetailsModalVisible = false

  componentWillMount() {
    const { navigation: { state: { params: { slug } } } } = this.props

    action(() => {
      this.props.params.slug = decodeURIComponent(slug)
    })()
  }

  componentWillReact() {
    if (typeof document !== 'undefined') {
      document.title = this.props.shop.data
        ?`${this.props.shop.data.name} | ${app.displayName}`
        : app.displayName
    }
  }


  handleLanguageChange = language => {
    this.props.shop.data.language = language
  }

  handleTranslationLanguageChange = translationLanguage => {
    this.props.params.translationLanguageRequest = translationLanguage
    this.props.params.currencyCode = get(find(getLanguages(), { code: translationLanguage}), 'currencyCode')
      || this.props.shop.currencyCode
  }

  @observable currentCategory = null

  handleScroll = event => {
    const { y } = event.nativeEvent.contentOffset
    const categories = Array.from(document.querySelectorAll('.category-container'))

    const categoryIndex = categories
      .sort((a, b) => absoluteOffsetTop(a) - absoluteOffsetTop(b))
      .reduce((result, category, index) =>
          absoluteOffsetTop(category) <= y
          ? index
          : result,
        0)
    this.currentCategory = categories[categoryIndex].id
  }

  handleCategoryChange = categoryId => {
    const element = document.querySelector(`#${categoryId}`)
    if (element) {
      element.scrollIntoView({ block: 'start', inline: 'start' })
    }
  }

  handleLanguagePress = () => {
    this.isTranslationModalVisible = !this.isTranslationModalVisible
  }

  handleDetailsPress = () => {
    this.isDetailsModalVisible = !this.isDetailsModalVisible
  }

  render () {
    const { params, t } = this.props
    const { data, fetching } = this.props.shop
    const { translationLanguageRequest, translationLanguage } = params

    if (!data) {
      return <Container>
        <Modal visible>
          <Claim {...{t}}/>
        </Modal>
      </Container>
    }

    return (
      <Container
        onScroll={this.handleScroll}
        scrollEventThrottle={500}
      >
        <Modal visible={fetching}>
           <ActivityIndicator/>
        </Modal>
        <Modal
          visible={this.isTranslationModalVisible}
          onRequestClose={this.handleLanguagePress}
        >
          <Translation onClose={this.handleLanguagePress}/>
        </Modal>
        <Modal
          visible={this.isDetailsModalVisible}
          onRequestClose={this.handleDetailsPress}
        >
          <Details {...{t}} onClose={this.handleDetailsPress}/>
        </Modal>
        <Modal visible={translationLanguageRequest !== translationLanguage}>
          <ActivityIndicator/>
        </Modal>
        <Column>
          <HeaderImage store={data}/>
          <Header>
            <Row>
              <LogoColumn>
                <Logo store={data} {...{t}}/>
              </LogoColumn>
              <Name store={data}/>
            </Row>
          </Header>
        </Column>
        <Gallery/>
        <Section>
          <StickyBar>
            <CategoryLinks
              currentCategory={this.currentCategory}
              onCategoryChange={this.handleCategoryChange}
              {...{t}}
            />
            <Button
              title="Language"
              onPress={this.handleLanguagePress}
            />
            <Button
              title="..."
              onPress={this.handleDetailsPress}
            />
          </StickyBar>
          <Categories
            onResize={this.handleCategoryResize}
            {...{t}}
          />
        </Section>
      </Container>
    )
  }
}