import React from 'react'
import {Picker} from 'react-native'
import { getLanguages } from 'translate'

import {Label} from 'styled'

export default ({store, t}) => (
  <Label label="Business Language">
    <Picker
      selectedValue={store.language}
      onValueChange={newLanguage => {
        store.language = newLanguage
        t.i18n.changeLanguage(newLanguage)
      }}>{
      getLanguages('Please Select')
        .map(({ name, code}) =>
          <Picker.Item
            key={code}
            label={name}
            value={code}
          />)
    }</Picker>
  </Label>
)