import styled from 'styled-components/native'

export default styled.View`
  box-shadow: 0 0 5px;
  background-color: #eee;
  border-radius: 5px;
  border: 1px solid rgba(0,0,0,.5);
  min-width: 300px;
  max-width: 80vw;
  min-height: 300px;
  padding: 20px;
`