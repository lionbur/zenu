import React, {Component} from 'react'
import {observable, observe} from 'mobx'
import {auth, firestore} from 'firebase'
import {get, find} from 'lodash'
import {getRates} from 'fixer'

import Container from './Container'
import Title from './Title'
import Button from './Button'
import ButtonBar from './ButtonBar'
import BodyText from './BodyText'
import Language from './Language'
import Currency from './Currency'
import { getLanguages } from 'translate'

const language = new URL(window.location.href).searchParams.get('l')
const currencyCode = new URL(window.location.href).searchParams.get('c')

const state = observable({
  inProgress: false,
  language: (language || '_').toLowerCase(),
  currencyCode: (currencyCode || 'EUR').toUpperCase(),
})

const claimShop = async ({isLoggedIn, user, slug}) => {
  state.inProgress = true

  if (!isLoggedIn) {
    user = await auth().signInAnonymously()
  }

  try {
    await firestore()
      .doc(['users', user.uid].join('/'))
      .update({
        adminOf: [slug],
        timeCreated: firestore.FieldValue.serverTimestamp(),
        isAdmin: true,
      })

    await firestore()
      .doc(['restaurants', slug].join('/'))
      .set({
        currencyCode: state.currencyCode,
        language: state.language,
        name: slug,
        timeCreated: firestore.FieldValue.serverTimestamp(),
      })
  } finally {
    state.inProgress = false
  }
}

observe(state, 'language', ({newValue}) => {
  state.currencyCode = get(find(getLanguages(), {code: newValue}), 'currencyCode')
    || state.currencyCode
})

export default class extends Component {
  componentWillMount() {
    if (language) {
      this.props.t.i18n.changeLanguage(language)
    }
    getRates()
  }

  render () {
    const {slug, isLoggedIn, user, t} = this.props

    return (
      <Container>
        <Title>{slug}</Title>
        <BodyText>{t('claim:body', {
          language: getLanguages('(select it below)').find(({code}) => code === state.language).name
        })}</BodyText>
        {!language && <Language store={state} {...{t}}/>}
        {!currencyCode && <Currency store={state}/>}
        <ButtonBar>
          <Button
            isInProgress={state.inProgress}
            {...{t}}
            onPress={() => claimShop({isLoggedIn, slug, user})}
            disabled={state.language === '_' || state.inProgress}
          />
        </ButtonBar>
      </Container>
    )
  }
}
