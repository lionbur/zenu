import styled from 'styled-components/native'

export default styled.Button
  .attrs({
    color: 'green',
    title: ({t, disabled, isInProgress}) => t(disabled
      ? isInProgress
        ? 'claim:wait'
        : 'claim:missing'
      : 'claim:button')
  })`
  margin-top: 20px;
`