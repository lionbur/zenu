import {observer, inject} from 'mobx-react'
import {compose} from 'recompose'

import Claim from './Claim'

const enhance = compose(
  inject(({session:{isLoggedIn, user}, params:{slug}}) => ({
    isLoggedIn,
    user,
    slug,
  })),
  observer,
)

export default enhance(Claim)