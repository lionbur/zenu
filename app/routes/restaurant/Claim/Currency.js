import React from 'react'
import {Picker} from 'react-native'
import { getLanguages } from 'translate'
import getSymbolFromCurrency from 'currency-symbol-map'
import {observer} from 'mobx-react'

import {Label} from 'styled'
import {getCurrencyCodes} from 'fixer'

export default observer(({store}) => (
  <Label label="Business Currency">
    <Picker
      selectedValue={store.currencyCode}
      onValueChange={newCurrencyCode => store.currencyCode = newCurrencyCode}>{
      [
        store.currencyCode,
        ...getCurrencyCodes(),
      ]
        .map(name =>
          <Picker.Item
            key={name}
            label={`${name} ${getSymbolFromCurrency(name)}`}
            value={name}
          />)
    }</Picker>
  </Label>
))
