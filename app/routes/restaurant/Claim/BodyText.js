import styled from 'styled-components/native'

export default styled.Text`
  flex: 1;
  margin-bottom: 10px;
  color: rgba(0,0,0,.9);
`
