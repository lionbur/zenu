import styled from 'styled-components/native'

export default styled.Text`
  font-size: 24px;
  font-weight: 600;
  align-self: center;
  margin-bottom: 10px;
  color: rgba(0,0,0,.8);
`