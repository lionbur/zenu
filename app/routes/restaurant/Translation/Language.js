import React from 'react'
import styled from 'styled-components/native'
import { getLanguages } from 'translate'
import {get, find} from 'lodash'
import {Picker as _Picker} from 'react-native'
import {observer} from 'mobx-react'
import {Label as _Label } from 'styled'

const Picker = styled(_Picker)`
  flex: 1;
`

const Label = styled(_Label)`
  margin-bottom: 5px;
  min-height: 1.5em;
`

const Language =({shop, params}) => (
  <Label label={'Translate into'}>
    <Picker
      selectedValue={params.translationLanguage}
      onValueChange={translationLanguage => {
        params.translationLanguageRequest = translationLanguage
        params.currencyCode = get(find(getLanguages(), {code: translationLanguage}), 'currencyCode')
          || shop.data.currencyCode
      }}>{
      getLanguages('(?)')
        .filter(({ code }) => code !== shop.data.language)
        .filter(({ code }) => shop.data.languages
          ? code === '_' || shop.data.languages.includes(code)
          : true)
        .map(({ name, code }) => name === '(?)'
          ? {
              name: `(${getLanguages().find(({code}) => code === shop.data.language).name})`,
              code
            }
          : { name, code })
        .map(({ name, code}) =>
          <_Picker.Item key={code} label={name} value={code}/>)
    }</Picker>
  </Label>
)

export default observer(Language)