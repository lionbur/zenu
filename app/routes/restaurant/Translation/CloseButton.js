import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  position: absolute;
  bottom: 10px;
  align-self: center;
  left: 20px;
  right: 20px;
`

const Button =  styled.Button``

export default ({onPress}) => (
  <Container>
    <Button title="Close" {...{onPress}}/>
  </Container>
)