import React from 'react'
import {observer, inject} from 'mobx-react'
import {compose} from 'recompose'

import Container from './Container'
import Language from './Language'
import CurrencyCode from './CurrencyCode'
import CloseButton from './CloseButton'

const enhance = compose(
  inject(({params, shop}) => ({
    params,
    shop,
  })),
  observer,
)

const Translation = ({params, shop, onClose}) => (
  <Container>
    <Language
      {...{params, shop}}
    />
    <CurrencyCode
      {...{params, shop}}
    />
    <CloseButton onPress={onClose}/>
  </Container>
)


export default enhance(Translation)