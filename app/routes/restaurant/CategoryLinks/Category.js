import React from 'react'
import styled from 'styled-components/native'

const Container = styled.TouchableOpacity`
  padding: 5px;
  border-radius: 5px;
  box-shadow: 0 0 5px;
  min-width: 100px;
  max-width: 200px;
  flex: 1;
`

const ButtonText = styled.Text
  .attrs({
    numberOfLines: 1,
  })`
`

const Button = ({ title, onPress, ...rest}) => (
  <Container {...{onPress}} {...rest}>
    <ButtonText>{title}</ButtonText>
  </Container>
)

export default ({ data }) => (
  <Button
    title={data.name}
    onPress={() => {}}
  />
)
