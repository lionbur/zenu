import React from 'react'
import {Picker} from 'react-native'
import { Label } from 'styled'

import Container from './Container.web'
import { isValidLanguage } from 'store'

const pickerStyle = {
  width: '100%',
}

export default ({ categories, translatedCategories, translationLanguage, currentCategory, onCategoryChange, t }) => (
  <Container>
    <Picker
      selectedValue={currentCategory || ''}
      onValueChange={onCategoryChange}
      style={pickerStyle}
    >{
    (isValidLanguage(translationLanguage)
      ? translatedCategories.docs
        .map(translated => ({
          original: categories.docs.find(({id}) => id === translated.id),
          main: translated,
        }))
      : categories.docs
        .map(main => ({
          original: main,
          main
        }))
    )
      .sort((a, b) => (a.original || b.main).data.order - (b.original || b.main).data.order)
      .map(({ main: { path, data, id } }) =>
        <Picker.Item
          key={path}
          label={data.name}
          value={id}
        />)
    }</Picker>
  </Container>
)
