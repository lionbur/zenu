import React from 'react'
import Section from '../Section'
import Address from '../Address'
import Email from '../Email'
import Phones from '../Phones/index'

export default ({store, t, isAdmin}) => (
  <Section>
    {(isAdmin || store.address)
      ? <Address {...{store}}/>
      : null}
    {(isAdmin || store.email)
      ? <Email {...{store, t}}/>
      : null}
    <Phones {...{t}}/>
  </Section>
)