import React from 'react'
import {observer, inject} from 'mobx-react'
import {compose} from 'recompose'

import Container from './Container'
import Contacts from './Contacts'
import CloseButton from './CloseButton'

const enhance = compose(
  inject(({params, shop}) => ({
    params,
    shop,
  })),
  observer,
)

const Details = ({t, params, shop, onClose}) => (
  <Container>
    <Contacts store={shop.data} isAdmin={params.isAdmin} {...{t}} />
    <CloseButton onPress={onClose}/>
  </Container>
)


export default enhance(Details)