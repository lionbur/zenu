import { Link, styled } from 'styled'

const Email = styled(Link)
  .attrs({
    name: 'email',
    placeholder: ({t}) => t('restaurant:email'),
    baseHref: 'mailto:',
  })`
  color: blue;
  text-decoration: underline;
`

export default Email