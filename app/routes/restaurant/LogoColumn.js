import styled from 'styled-components/native'

export default styled.View`
  flex-direction: column;
  width: 100px;
  margin-right: 10px;
`