import styled from 'styled-components/native'

export default styled.View`
  padding: 20px;
  align-self: center;
  width: 100%;
  background-color: ${({theme}) => theme.main.backgroundColor};
`