import { Field, styled } from 'styled'

const Name = styled(Field)
  .attrs({
    name: 'name',
    placeholder: 'Business Name'
  })`
  text-align: left;
  font-size: 2em;
  
  line-height: 2rem;
  font-weight: 600;
  color: ${({theme}) => theme.restaurant.name.color};
  text-shadow: 0 0 10px black;
`

export default Name
