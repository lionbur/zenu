import styled from 'styled-components'

export default styled.div`
  background: linear-gradient(transparent, ${({theme}) => theme.main.backgroundColor});
  align-self: center;
  flex-direction: row;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  max-width: 100vw;
`