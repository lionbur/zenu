import { Link, styled } from 'styled'

const Phone = styled(Link)
  .attrs({
    name: 'phoneNumber',
    placeholder: ({t}) => t('restaurant:phone'),
    baseHref: 'tel:',
  })`
  color: green;
  text-decoration: underline;
`

export default Phone