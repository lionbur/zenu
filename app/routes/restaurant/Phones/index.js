import Phones from './Phones'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import {get} from 'lodash'

const enhance = compose(
  inject(store => ({
    isAdmin: get(store, 'params.isAdmin'),
    shop: get(store, 'shop'),
  })),
  observer,
)

export default enhance(Phones)