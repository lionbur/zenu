import styled from 'styled-components/native'

export default styled.View`
  margin-top: 5px;
  margin-bottom: 5px;
  max-width: 100vw;
`