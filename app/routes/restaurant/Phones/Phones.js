import React from 'react'

import Container from './Container'
import Phone from './Phone'
import { isValidLanguage } from 'store'

export default ({ shop, isAdmin, t }) => (
  shop.sub.phones
    ? <Container>{
      (isAdmin
        ? shop.sub.phones.docsWithNew
        : shop.sub.phones.docs)
      .sort((a ,b) => a.data.order - b.data.order)
      .map(({path, data}) =>
        <Phone
          key={path}
          store={data}
          {...{t}}
        />)
    }</Container>
  : null
)
