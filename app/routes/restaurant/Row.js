import styled from 'styled-components/native'

export default styled.View`
  flex-direction: row;
  flex: 1;
  align-items: center;
`