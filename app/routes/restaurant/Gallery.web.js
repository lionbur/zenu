import React from 'react'
import Coverflow from '../../Coverflow/Coverflow'
import styled from 'styled-components/native'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import { uniq } from 'lodash'
import Carousel from '../../react-3d-carousel'

/*
      ...categories
        .docs
        .reduce((result, category) => [
          ...result,
          ...category
            .sub
            .items
            .docs
            .filter(({data}) => data.thumbnail)
            .map(({data}) => data.thumbnail)
        ], [])

 */

const Gallery = ({shop}) => {
  const images = uniq([
    ...shop
      .sub
      .gallery
      .docs
      .map(({data}) => data.image),
  ])

  if (!images.length) {
    return null
  }

  return (<Carousel
    width={267}
    height={150}
    {...{images}}
    ease='sineOut'
    duration={400}
    layout="prism"
  />)
}

const enhance = compose(
  inject('shop'),
  inject('categories'),
  observer,
)

export default enhance(Gallery)