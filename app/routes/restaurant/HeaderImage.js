import styled from 'styled-components/native'

import { ImageField } from 'styled'

export default styled(ImageField)
  .attrs({
    name: 'headerImage',
  })`
  height: 100px;
  background-color: ${({theme}) => theme.header.backgroundColor};
`
