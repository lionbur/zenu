import { compose } from 'recompose'
import { observer, inject } from 'mobx-react'
import Items from './Items'

const enhance = compose (
  inject('params'),
  inject('profile'),
  inject('shop'),
  observer,
)

export default enhance(Items)