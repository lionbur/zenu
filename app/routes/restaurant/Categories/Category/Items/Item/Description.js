import { Field, styled } from 'styled'
import {get} from 'lodash'

export default styled(Field)
  .attrs({
    name: 'description',
    placeholder: ({t}) => t('restaurant:description'),
    multiline: true,
  })`
  white-space: pre;
  ${({theme}) => `
      color: ${get(theme, 'item.description.color') || get(theme, 'item.name.color') || theme.main.color};
      background-color: ${get(theme, 'item.description.backgroundColor') || get(theme, 'item.name.backgroundColor') || 'transparent'};
    `};
`