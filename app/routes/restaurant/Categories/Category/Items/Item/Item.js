import React, { Component } from 'react'
import styled from 'styled-components/native'
import {
  FacebookShareCount,
  GooglePlusShareCount,
  PinterestShareCount,
  TumblrShareCount,
  VKShareCount,
  OKShareCount,
  RedditShareCount,

  FacebookShareButton,
  WhatsappShareButton,

  FacebookIcon,
  WhatsappIcon,
} from 'react-share'
import {Text} from 'react-native'

import Container from './Container'
import Row from './Row'
import Column from './Column'
import NameAndPrice from './NameAndPrice'
import Name from './Name'
import OriginalName from './OriginalName'
import Price from './Price'
import Description from './Description'
import Thumbnail from './Thumbnail'
import ThumbsUp from './ThumbsUp'
import directionByLanguage from "styled/directionByLanguage";

const parsePath = path => {
  const exp = /(categories\/(.*?)\/items\/(.*?))$/
  const match = exp.exec(path)

  return match
    ? {
      category: match[2],
      item: match[3],
    }
    : null
}

const pathToId = path => {
  const parsed = parsePath(path)

  return parsed
    ? `${parsed.category}_${parsed.item}`
    : null
}

const hash = decodeURIComponent(location.hash).replace(/^#/, '')
const supportsShareAPI = typeof navigator.share === 'function'

const Button = styled.Button.attrs({
  color: ({theme}) => theme.button.backgroundColor,
})``;

export default class extends Component {
  id = null
  isShared = false

  componentWillMount() {
    this.id = pathToId(this.props.path)
    this.isShared = hash === this.id
  }

  componentDidMount() {
    if (this.isShared) {
      const element = document.querySelector(`#${this.id}`)

      if (element) {
        element.scrollIntoView({ block: 'center' })
        setTimeout(() => element.scrollIntoView({ block: 'center' }), 1000)
      }
    }
  }

  gotFacebookShareCount = false
  handleFacebookShareCount = facebook => {
    const { original } = this.props

    if (original.data) {
      original.ref.update({
        shares: {
          ...original.data.shares,
          facebook,
        }
      })
      this.gotFacebookShareCount = true
    }
  }

  handleBeforeShare = network => () => new Promise(resolve => {
    const { original:{data} } = this.props

    if (data) {
      data.shares = {
        ...data.shares,
        [network]: (data.shares[network] || 0) + 1,
      }
    }
    resolve()
  })

  webShare = async ({title, text, url}) => {
    const res = await navigator.share({title, text, url})
  }

  render() {
    const { data, isAutoTranslated, original, translationLanguage, t } = this.props
    const { id, isShared } = this
    const url = `https://translated.menu${location.pathname}#${id}`

    const {
      facebook = 0,
      whatsapp = 0,
      pinterest = 0,
      tumblr = 0,
      telegram = 0,
      twitter = 0,
      google = 0,
      vk = 0,
      ok = 0,
      email = 0,
      reddit = 0,
    } = (original && original.data.shares) || {}

    return (
      <Container {...{id, isShared}} style={{direction: directionByLanguage(translationLanguage)}}>
        <Row>
          <Thumbnail store={((data && data.thumbnail) || !original) ? data : original}/>
          <Column fullWidth>
            <NameAndPrice>
              <Column fullWidth>
                <Name store={data} {...{isAutoTranslated, isShared, t}}/>
                {this.isShared && <ThumbsUp/>}
              </Column>
              <Price store={original ? original.data : data} {...{t}}/>
            </NameAndPrice>
            {original && <OriginalName store={original.data}/>}
            <Description store={data} {...{isAutoTranslated, t}}/>
          </Column>
          <Column style={{zIndex:1000, paddingLeft: 5}}>
            {supportsShareAPI
              ? <Button
                  title="👍"
                  onPress={() => this.webShare({
                    title: data.name,
                    text: `${data.name}\n${data.description}`,
                    url,
                  })}
                />
              : [<FacebookShareButton
                {...{url}}
                beforeOnClick={this.handleBeforeShare('facebook')}
                quote={data.name}
                hashtag="#delicious"
              >
                <FacebookIcon size={32} />
              </FacebookShareButton>,
              <WhatsappShareButton
                {...{url}}
                beforeOnClick={this.handleBeforeShare('whatsapp')}
                title={data.name}
              >
                <WhatsappIcon size={32} />
              </WhatsappShareButton>]}
          </Column>
        </Row>
      </Container>
    )
  }
}

/*
            {original && !this.gotFacebookShareCount && <FacebookShareCount {...{url}}>
              {this.handleFacebookShareCount}
            </FacebookShareCount>}
 */