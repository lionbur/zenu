import React from 'react'
import {inject} from 'mobx-react'
import getSymbolFromCurrency from 'currency-symbol-map'
import {get} from 'lodash'

import {styled} from 'styled'
import { Field } from 'styled'
import {tryConvertPriceFormatted} from 'fixer'

const Container = styled.div`
  text-align: right;
  margin-left: 10px;
`

const PriceInput = styled(Field)
  .attrs({
    name: 'price',
    placeholder: ({t}) => t('restaurant:price'),
  })`
  text-align: right;
  width: 70px;
  margin-left: 10px;
  color: ${({theme}) => get(theme, 'item.price.color') || get(theme, 'item.name.color') || theme.main.color};
`

const PriceText = styled.div`
  color: ${({theme}) => get(theme, 'item.price.color') || get(theme, 'item.name.color') || theme.main.color};
`

const Currency = styled.div`
`

const ShopPriceText = styled.div`
  color: ${({theme}) => theme.item.originalName.color};
  font-style: italic;
`

const displayPrice = price => (
  Math.round(price)
)

const PriceWithCurrency = ({ convertedPrices, shopCurrencyCode, price, ...props}) => (
  convertedPrices.length
    ? <Container>
        <PriceText>
          {convertedPrices[0].currencyCode !== shopCurrencyCode ? '~' :''}
          {getSymbolFromCurrency(convertedPrices[0].currencyCode) || convertedPrices[0].currencyCode}
          {convertedPrices
            .map(({amount, currencyCode}) => displayPrice(amount))
            .join(' / ')}
        </PriceText>
        <ShopPriceText>
          {getSymbolFromCurrency(shopCurrencyCode) || shopCurrencyCode}
          {price}
        </ShopPriceText>
      </Container>
    : null
)

const PriceInputWithCurrency = ({ store, shopCurrencyCode, t, ...props}) => (
  <Container>
    <PriceInput {...{store, t}} {...props}/>
    <Currency>{getSymbolFromCurrency(shopCurrencyCode) || shopCurrencyCode}</Currency>
  </Container>
)

const enhance = inject(
  ({ shop:{data}, params:{isAdmin, currencyCode} }) => ({
    isAdmin,
    currencyCode,
    shopCurrencyCode: get(data, 'currencyCode'),
  })
)

export default enhance(({ store, isAdmin, currencyCode, shopCurrencyCode, t, ...props }) => (
  isAdmin
    ? <PriceInputWithCurrency
      {...props}
      {...{store, shopCurrencyCode, t}}
    />
    : <PriceWithCurrency
      price={store.price}
      {...props}
      {...{shopCurrencyCode, t}}
      convertedPrices={tryConvertPriceFormatted({
        amount: store.price || '',
        toCurrencyCode: currencyCode,
      })}
    />
))