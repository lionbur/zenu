import { Field, styled } from 'styled'

export default styled(Field)
  .attrs({
    name: 'name',
    placeholder: ({t}) => t('restaurant:item'),
  })`
  font-weight: 600;
  font-size: 120%;
  flex: 1;
  ${({ isAutoTranslated }) => isAutoTranslated ? 'font-style: italic;' : ''}
  ${({isShared}) => isShared ? 'text-shadow: 0 0 20px yellow;' : ''}
  color: ${({theme}) => theme.item.name.color};
  min-width: 70%;
`
