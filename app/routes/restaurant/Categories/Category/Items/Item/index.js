import Item from './Item'
import { observer } from 'mobx-react'
import { compose } from 'recompose'
import { injectIsAutoTranslated } from 'store'

const enhance = compose(
  injectIsAutoTranslated,
  observer,
)

export default enhance(Item)