import styled from 'styled-components/native'

export default styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-end;
`
