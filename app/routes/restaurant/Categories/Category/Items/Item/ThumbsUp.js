import React from 'react'
import styled from 'styled-components/native'

const Text = styled.Text`
  flex: 1;
  align-self: center;
  font-size: 120%;
`

export default () => <Text>😊😊😊😊😊</Text>