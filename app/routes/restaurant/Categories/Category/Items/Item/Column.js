import styled from 'styled-components/native'

export default styled.View`
  flex-direction: column;
  ${({fullWidth}) => fullWidth ?'flex: 1; min-width: 70%' :'margin-left: 5px'};  
`
