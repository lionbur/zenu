import styled from 'styled-components/native'
import { DataText } from 'styled'

export default styled(DataText)
  .attrs({
    name: 'name',
  })`
  flex: 1;
  color: ${({theme}) => theme.item.originalName.color};
  font-style: italic;
`
