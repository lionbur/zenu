import styled from 'styled-components/native'

export default styled.View`
  border: 1px solid rgba(0,0,0,.1);
  ${({isShared}) => isShared ? 'font-size: 120%;' : ''}
  border-radius: 5px;
  margin: 5px;
`