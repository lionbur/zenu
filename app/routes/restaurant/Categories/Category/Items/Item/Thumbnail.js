import styled from 'styled-components/native'
import { ImageField } from 'styled'

const size = 75

export default styled(ImageField)
  .attrs({
    name: 'thumbnail',
    placeholder: 'Thumbnail',
  })`
  flex: 0;
  min-width: ${size}px;
  min-height: ${size}px;
  max-width: ${size}px;
  max-height: ${size}px;
  margin-right: 10px;
  border-radius: 10px;
`
