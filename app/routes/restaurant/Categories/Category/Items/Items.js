import React from 'react'

import Container from './Container'
import Item from './Item'
import { isValidLanguage } from 'store'

const getTranslationLanguage = (translationLanguage, shopLanguage) => (
  isValidLanguage(translationLanguage) ? translationLanguage : shopLanguage
)

export default ({ items, originalItems, params, profile, shop, t }) => (
  <Container>{
    (isValidLanguage(params.translationLanguage)
      ? items.docs
        .map(translated => ({
          main: translated,
          original: originalItems ? originalItems.docs.find(({id}) => id === translated.id) : translated,
        }))
      : (profile.data && profile.data.isAdmin
          ? items.docsWithNew
          : items.docs)
        .map(main => ({main}))
    )
      .sort((a, b) => (a.original || a.main).data.order - (b.original || b.main).data.order)
      .map(({ main: { path, data, sub }, original }) =>
        <Item
          key={path}
          {...{path, data, sub, original, t}}
          translationLanguage={getTranslationLanguage(params.translationLanguage, shop.data.language)}
        />)
  }</Container>
)
