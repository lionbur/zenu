import React from 'react'
import styled from 'styled-components/native'
import { DataText } from 'styled'

const Container = styled.View`
  padding-left: 40px;
  padding-right: 40px;
  padding-bottom: 10px;
  margin-bottom: 10px;
`

const Name = styled(DataText)
  .attrs({
    name: 'name',
  })`
  font-size: 1.0em;
  color: ${({theme}) => theme.category.originalName.color};
`

export default props => (
  <Container>
    <Name {...props}/>
  </Container>
)
