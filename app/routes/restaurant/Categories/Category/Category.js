import React, { Component } from 'react'
import { ActivityIndicator  } from 'react-native'
import {observable} from 'mobx'
import {observer} from 'mobx-react'

import Container from './Container'
import Name from './Name'
import Description from './Description'
import OriginalName from './OriginalName'
import Items from './Items'
import ItemsPlaceholder from './ItemsPlaceholder'

const hash = decodeURIComponent(location.hash).replace(/^#/, '')
const hashCategory = hash.split('_')[0]

const absoluteScrollTop = element => element.parentElement
  ? absoluteScrollTop(element.parentElement) + element.scrollTop
  : element.scrollTop

export default
@observer
class extends Component {
  @observable isVisible = false
  @observable isLoading = false

  handleBecomeVisible = async () => {
    const { sub } = this.props

    if (sub && sub.items) {
      this.isLoading = true
      if (sub.items.active) {
        await sub.items.ready()
      } else {
        await sub.items.fetch()
      }
      this.isLoading = false
    }

    this.isVisible = true
  }

  async componentWillMount() {
    if (hashCategory === this.props.id) {
      await this.handleBecomeVisible()
    }
  }

  element = null
  interval = null

  componentDidMount() {
    if (hashCategory !== this.props.id) {
      this.element = document.getElementById(this.props.id)

      if (this.element) {
        this.interval = setInterval(this.watchUntilVisible, 100)
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
    this.interval = null
  }

  watchUntilVisible = async () => {
    const parent = this.element.offsetParent
    const height = window.innerHeight
    const top = absoluteScrollTop(parent) - height * 0.5
    const bottom = top + height + height * 0.5
    const y0 = this.element.offsetTop
    const y1 = y0 + this.element.offsetHeight

    if (((y0 >= top) && (y0 <= bottom))
      || ((y1 >= top) && (y1 <= bottom))
      || ((y0 < top) && (y1 > bottom))) {
      clearInterval(this.interval)
      this.interval = null
      await this.handleBecomeVisible()
    }
  }

  render () {
    const { id, isAutoTranslated, data, sub, original, t, ...rest } = this.props
    const { isLoading, isVisible } = this

    return <Container
      className="category-container"
      {...{id}}
      {...rest}
    >
      <Name store={data} {...{isAutoTranslated, t}}/>
      <Description store={data} {...{isAutoTranslated, t}}/>
      {isLoading && <ActivityIndicator/>}
      {!isLoading && original && <OriginalName store={original.data}/>}
      {!isLoading && sub && isVisible
        ? <Items
            items={sub.items} originalItems={original && original.sub.items}
            {...{t}}
          />
        : <ItemsPlaceholder/>}
    </Container>
  }
}
