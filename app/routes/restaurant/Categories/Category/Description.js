import { Field, styled } from 'styled'
import {get} from 'lodash'

const Description = styled(Field)
  .attrs({
    name: 'description',
    placeholder: ({t}) => t('restaurant:description'),
  })`
  white-space: pre;
  padding: 0px 5px;
  ${({theme}) => `
    color: ${get(theme, 'category.description.color') || get(theme, 'category.name.color') || theme.main.color};
    background-color: ${get(theme, 'category.description.backgroundColor') || get(theme, 'category.name.backgroundColor') || 'transparent'};
  `};
`

export default Description
