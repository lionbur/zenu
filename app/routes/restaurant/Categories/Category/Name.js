import React from 'react'
import { Field, styled } from 'styled'

const Name = styled(Field)
  .attrs({
    name: 'name',
    placeholder: ({t}) => t('restaurant:category')
  })`
  padding: 10px 40px;
  font-size: 2em;
  font-weight: 600;
  ${({theme}) => theme.category.css || ''};
  color: ${({theme}) => theme.category.name.color};
`

export default Name
