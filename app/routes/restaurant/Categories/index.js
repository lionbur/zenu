import Categories from './Categories'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'

const enhance = compose(
  inject(({categories, translatedCategories, params:{isAdmin, translationLanguage}}) => ({
    categories,
    translatedCategories,
    isAdmin,
    translationLanguage
  })),
  observer,
)

export default enhance(Categories)