import React from 'react'

import Container from './Container'
import Category from './Category'
import { isValidLanguage } from 'store'

export default ({ categories, translatedCategories, isAdmin, translationLanguage, t }) => (
  <Container>{
    (isValidLanguage(translationLanguage)
      ? translatedCategories.fetching
        ? []
        : translatedCategories.docs
          .map(translated => ({
            main: translated,
            original: categories.docs.find(({id}) => id === translated.id)
          }))
      : (isAdmin
          ? categories.docsWithNew
          : categories.docs)
        .map(main => ({main}))
    )
      .sort((a, b) => (a.original || a.main).data.order - (b.original || b.main).data.order)
      .map(({ main: { path, data, sub, id }, original }, index) => (
        <Category
          {...{id, data, sub, original, t}}
        />
      ))
  }</Container>
)
