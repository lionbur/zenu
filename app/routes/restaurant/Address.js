import styled from 'styled-components/native'
import { Field } from 'styled'

export default styled(Field)
  .attrs({
    name: 'address',
    placeholder: 'Add your business address here',
    multiline: true,
  })`

  line-height: 1.5rem;
  font-weight: 600;
`
