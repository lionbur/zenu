import React from 'react'
import {firestore} from 'firebase'
import {inject} from 'mobx-react'
import {getIdFromName} from 'store/helpers'
import {transcribe} from 'translate'
import {storage} from 'firebase'
import {get} from 'lodash'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'
import {Collection, Document} from 'services'

const getSlug = name => transcribe(getIdFromName(name))

const storageUrlFromUrl = async ({ url, path }) => {
  if (/^data:/.test(url)) {
    return ''
  }
  if (!url) {
    return url
  }

  try {
    console.log('Downloading', url)

    const response = await fetch(url, {mode: 'cors'})
    const blob = await response.blob()

    console.log('Uploading', blob, path)

    return (await storage()
      .ref(path)
      .put(blob, {
        contentType: blob.type,
        customMetadata: {
          url,
          size: blob.size,
          timeCreated: (new Date).toISOString(),
        },
      }))
      .downloadURL
  } catch (error) {
    console.log(error)
    return url
  }
}

const handleCreate = async (session, { name, slug, language, currencyCode, menu }) => {
  console.log({name, slug, language, menu})

  /*
  if (get(session, 'user.uid')) {

    const user = new Document(['users', session.user.uid].join('/'), {mode: 'on'})
    await user.ready()
  }*/

  const restaurants = new Collection('restaurants')
  await restaurants.ref.doc(slug).set({
    name,
    language,
    timeCreated: firestore.FieldValue.serverTimestamp(),
    headerImage: 'https://media.giphy.com/media/S1PNogIEHI4aQ/giphy.gif',
    currencyCode,
  })

  const categories = new Collection([restaurants.path, slug, 'categories'].join('/'))
  const translatedCategories = new Collection([
    restaurants.path,
    slug,
    'translations',
    'en',
    'categories'
  ].join('/'))

  const operations = []

  menu
    .forEach(({ category, description = '', englishCategory, items }, index) => {
      const id = getSlug(englishCategory || category)
      operations.push(
        categories.ref.doc(id).set({
          name: category,
          description,
          timeCreated: firestore.FieldValue.serverTimestamp(),
          order: index + 1,
        }))
      if (englishCategory) {
        operations.push(
          translatedCategories.ref.doc(id).set({
            name: englishCategory,
            description,
            timeCreated: firestore.FieldValue.serverTimestamp(),
            timeLastModified: firestore.FieldValue.serverTimestamp(),
            order: index + 1,
          }))
      }

      const itemsColl = new Collection([categories.path, id, 'items'].join('/'))
      const translatedItems = new Collection([translatedCategories.path, id, 'items'].join('/'))
      const numberExp = /^\d+\.\d+|[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)?$/

      items
        .forEach(({ name, englishName, description = '', englishDescription = '', price = '0', image = '' }, index) => {
          const id = getSlug(englishName || name)
          let variations
          /*
          if (price.includes('/')) {
            const parts = price
              .split('/')
              .map(part => part
                .trim())

            const annoymousDescriptions = [
              [ ],
              [ 'M' ],
              [ 'M', 'L' ],
              [ 'S', 'M', 'L' ],
              [ 'S', 'M', 'L', 'XL' ],
              [ 'S', 'M', 'L', 'XL', 'XXL' ],
              [ 'XS', 'S', 'M', 'L', 'XL', 'XXL' ],
            ]

            const prices = parts
              .filter(part => numberExp.test(part))
            const descriptions = parts
              .filter(part => !prices.includes(part))
            const addParentesis = text => /\(.*?\)/.test(text)
              ? ` ${text}`
              : ` (${text})`
            const alternativeDescriptions = annoymousDescriptions[prices.length]
              || prices
                .map((_, index) => `No${index}`)

            variations = prices
              .map((price, index) => ({
                id: `${id}-${index+1}`,
                name: name
                + addParentesis(descriptions[index] || alternativeDescriptions[index]),
                price,
              }))
          } else
          */
          {
            variations = [{
              id,
              name,
              price,
            }]

            if (price && !numberExp.test(price)) {
              description = description
                ? `${description} (${price})`
                : price
            }
          }

          variations
            .forEach(({
                        id,
                        name,
                        price,
                      }) => {
              console.log(name, price, description, image)
              operations
                .push((async () =>
                  itemsColl.ref.doc(id).set({
                    name,
                    description,
                    price: price
                      .trim(),
                    thumbnail: await storageUrlFromUrl({
                      url: image,
                      path: [itemsColl.path, id].join('/')
                    }),
                    timeCreated: firestore.FieldValue.serverTimestamp(),
                    order: index + 1,
                  }))()
                )
              if (englishName) {
                operations
                  .push((async () =>
                    translatedItems.ref.doc(id).set({
                      name: englishName,
                      description: englishDescription,
                      price: parseFloat(price
                        .replace(/,/g, '')),
                      thumbnail: await storageUrlFromUrl({
                        url: image,
                        path: [itemsColl.path, id].join('/')
                      }),
                      timeCreated: firestore.FieldValue.serverTimestamp(),
                      timeLastModified: firestore.FieldValue.serverTimestamp(),
                      order: index + 1,
                    }))()
                  )
              }
            })
        }) // items
    }) // categories

  console.log(operations)
  await Promise.all(operations)
}

const enhance = inject('session')

export default enhance(({ session, navigation: { navigate }, t }) => (
  <Container>
    <Header/>
    <Body onCreate={async params => {
      await handleCreate(session, params)
      navigate('Restaurant', { slug: params.slug })
    }} {...{t}}/>
    <Footer/>
  </Container>
))