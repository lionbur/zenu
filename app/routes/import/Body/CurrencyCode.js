import React from 'react'
import styled from 'styled-components/native'
import { getLanguages } from 'translate'
import {get, find} from 'lodash'
import {Picker as _Picker} from 'react-native'
import getSymbolFromCurrency from 'currency-symbol-map'
import {observer} from 'mobx-react'

import {getCurrencyCodes} from 'fixer'
import { Label as _Label } from 'styled'

const Picker = styled(_Picker)`
  flex: 1;
  min-height: 20px;
`

const Label = styled(_Label)`
  min-height: 1.5em;
`

const CurrencyCode = ({store}) => (
  <Label label="Your Currency">
    <Picker
      selectedValue={store.currencyCode}
      onValueChange={currencyCode => store.currencyCode = currencyCode}>{
      [
        store.currencyCode || 'USD',
        ...getCurrencyCodes(),
      ]
        .map(name =>
          <_Picker.Item
            key={name}
            label={`${name} ${getSymbolFromCurrency(name)}`}
            value={name}
          />)
    }</Picker>
  </Label>
)

export default observer(CurrencyCode)