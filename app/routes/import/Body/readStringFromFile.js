export default file => new Promise(resolve => {
  const reader = new FileReader()

  reader.onload = ({ target:{result} }) => {
    resolve(result)
  }

  reader.readAsText(file)
})
