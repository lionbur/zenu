import React, {Component} from 'react'
import {observable, toJS} from 'mobx'
import {observer, inject} from 'mobx-react'
import {get, omit} from 'lodash'
import locale2 from 'locale2'
import csvjson from 'csvjson'

import Container from './Container'
import Name from './Name'
import Row from './Row'
import Text from './Text'
import AlreadyTakenText from './AlreadyTakenText'
import Langauge from './Langauge'
import CurrencyCode from './CurrencyCode'
import LinkText from './LinkText'
import CreateButton from './CreateButton'
import {Field} from 'styled'
import {getIdFromName} from 'store/helpers'
import {transcribe} from 'translate'
import {Document} from 'services'
import {getRates} from 'fixer'
import handleNdJson from './handleNdJson'
import handleCsv from './handleCsv'
import handleTextFile from './handleText'

let locale = process.env.NODE_ENV === 'production' ? locale2 : 'en'
const getLocale = () => process.env.CONFIG === 'fb' ? locale : 'en'

const getSlug = name => transcribe(getIdFromName(name))

export default @inject("session") @observer class extends Component {
  @observable data = {
    name: '',
    language: getLocale(),
    currencyCode: 'USD',
    isAlreadyTaken: null,
    menu: null,
  }
  lastSlug = null

  componentWillMount() {
    getRates()
  }

  async componentWillReact() {
    const { name } = this.data

    if (name) {
      const slug = getSlug(name)

      if (this.lastSlug !== slug) {
        this.data.isAlreadyTaken = null

        const restaurant = new Document(['restaurants', slug].join('/'), {mode: 'on'})
        await restaurant.ready()

        this.data.isAlreadyTaken = !!restaurant.data
        this.lastSlug = slug
      }
    } else {
      this.data.isAlreadyTaken = false
    }
  }

  handleFileChange = event => {
    const { files } = event.nativeEvent.target

    Array
      .from(files)
      .forEach(async file => {
        this.data.menu = /\.json$/.test(file.name)
          ? await handleNdJson(file)
          : /\.csv$/.test(file.name)
            ? await handleCsv(file)
            : await handleTextFile(file)
      })
  }

  render() {
    const { onCreate, t, session } = this.props
    const { data } = this
    const { name, language, menu, currencyCode } = data
    const slug = getSlug(name)
    const displayName = get(session, 'user.displayName') || ''

    return (
      <Container>
        <Text>{t('new:help', {displayName})}</Text>
        <Name store={data}/>
        <Langauge {...{t}} store={data}/>
        <CurrencyCode {...{t}} store={data}/>
        <Text>{t('new:linkWillBe')}</Text>
        <input type="file" onChange={this.handleFileChange}/>
        <LinkText>translated.menu/of/{slug}</LinkText>
        {data.isAlreadyTaken && <AlreadyTakenText>{t('new:alreadyTaken')}</AlreadyTakenText>}
        <CreateButton
          disabled={!menu || !name}
          onPress={() => onCreate({ name, slug, language, currencyCode, menu: toJS(menu) })}
          {...{t}}
        />
      </Container>
    )
  }
}


