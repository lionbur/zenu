import readStringFromFile from './readStringFromFile'

const expHasPrice = /^((.*?)\s+(RUB|USD|EUR|GBP|ILS)\s*([\d,.\/\s]+))$|^((.*?)\s+([\d,.\/\s]+)\s*([₽$₪]|₪‬))$/

export const handleText = lines => {
  const categories = []
  let isCategory = true
  let category = null

  for (const line of lines) {
    if (!line) {
      isCategory = true
      continue
    }

    if (isCategory) {
      category = {
        category: line,
        items: [],
      }
      categories.push(category)
      isCategory = false
    } else if (expHasPrice.test(line)) {
      let matches = expHasPrice.exec(line)
      const item = {
        name: matches[6].trim(),
        price: matches[7],
      }
      category.items.push(item)
    } else if (category.items.length) {
      if (category.items[category.items.length-1].description) {
        category.items[category.items.length-1].description += '\n'
      } else {
        category.items[category.items.length-1].description = ''
      }
      category.items[category.items.length-1].description += line
    } else {
      if (category.description) {
        category.description += '\n'
      } else {
        category.description = ''
      }
      category.description += line
    }
  }

  console.log(categories)

  return categories
}


export default async file => {
  const text = (await readStringFromFile(file))

  return handleText(text
    .replace(/\n\r/g, '\n')
    .replace(/\r\n/g, '\n')
    .replace(/\r/g, '\n')
    .split('\n')
    .map(line => line.trim()))
}
