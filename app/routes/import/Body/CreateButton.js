import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
  margin-top: 20px;
  align-self: center;
`

const Button = styled.Button
  .attrs({
    title: ({ t }) => t('new:create'),
  })`
`

export default ({ onPress, disabled, t, ...rest }) => (
  <Container {...rest}>
    <Button {...{onPress, disabled, t}}/>
  </Container>
)