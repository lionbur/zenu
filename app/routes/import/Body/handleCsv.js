import readStringFromFile from './readStringFromFile'

const handleCsvText = text => {
  const rows = []
  let row = []
  let isInQoute = false
  let value = ''

  for (let i = 0; i <= text.length; i++) {
    const ch = text.charAt(i)

    if (ch === '"') {
      if (isInQoute) {
        if (text.charAt(i + 1) === '"') {
          value += '"'
          i++
        } else {
          isInQoute = false
        }
      } else {
        isInQoute = true
      }
    } else if (!isInQoute && (ch === '\n' || i === text.length)) {
      if (i < text.length || text[i - 1] !== '\n') {
        row.push(value)
        rows.push(row)
      }
      row = []
      value = ''
    } else if (!isInQoute && ch === ',') {
      row.push(value)
      value = ''
    } else {
      value += ch
    }
  }

  return rows
}

export default async file => {
  const text = (await readStringFromFile(file))

  const lines = handleCsvText(text
    .replace(/\n\r/g, '\n')
    .replace(/\r\n/g, '\n')
    .replace(/\r/g, '\n'))

  const fields = lines.shift()
//  const hasEnglish = (fields && fields.includes('englishName'))

  const json = lines
    .map(row => row
      .reduce((rowObj, value, index) => ({
        ...rowObj,
        [fields[index]]: value,
      }), {})
    )

  if (fields && fields.includes('order')) {
    json
      .forEach(item => {
        if (item.category) {
          item.order = item.category
        }
      })

    const order = json
      .map(({ order }) => order)

    json
      .sort((a, b) =>
        order.indexOf(a.name || a.category) - order.indexOf(b.name || b.category))
  }

  let category = null
  const categories = {}
  const categoryDescriptions = {}
  const englishCategories = {}

  for (const item of json) {
    item.category = item.category.replace(/\n/g, ' ').trim()

    if ((item.category || item.englishCategory)
      && (!item.name || item.category !== category)) {
      category = item.category || item.englishCategory
      if (item.description || item.englishDescription) {
        categoryDescriptions[category] = item.description || item.englishDescription
      }
      if (item.englishCategory) {
        englishCategories[category] = item.englishCategory
      }
    } else if (!item.category) {
      item.category = category
    }
    item.image = item.image || item['image-src']
    item.name = (item.name || '')
      .replace(item.price, '')
      .trim()
    item.price = (item.price || '')
      .replace('р.', '')
      .replace(' руб.', '')
      .replace(' рублей', '')
      .trim()
  }

  const hasResultIndex = fields && fields.includes('resultIndex')
  if (hasResultIndex) {
    json
      .sort((a, b) => parseInt(a.resultIndex, 10) - parseInt(b.resultIndex, 10))
  }

  json
    .filter(({ name }) => name)
    .forEach((item, index) =>{
      let category = categories[item.category] ||
        (categories[item.category] = {
          category: item.category,
          description: categoryDescriptions[item.category],
          englishCategory: englishCategories[item.category],
          items: [],
          index,
        })

      category.items.push(item)
    })

  return Object
    .values(categories)
    .sort((a, b) => hasResultIndex ? b.index - a.index : a.index - b.index)
}
