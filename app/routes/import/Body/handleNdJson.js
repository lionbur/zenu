import {omit} from 'lodash'

import readStringFromFile from './readStringFromFile'

export default async file =>
  (await readStringFromFile(file))
    .split('\n')
    .filter(line => line)
    .map(line => {
      const json = JSON.parse(line)

      const { data } = json.result.extractorData

      const category = data[0].group[0].Category[0].text
      const items = (data[1]
        ? data[1].group
        : [omit(data[0].group[0], 'Phone')])
        .map(item => Object
          .entries(item)
          .reduce((result, [key,value]) => ({
            ...result,
            [key]: value[0].text
          }), {}))

      return {
        category,
        items
      }
    })
