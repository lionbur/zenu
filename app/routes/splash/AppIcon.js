import styled from 'styled-components/native'

export default styled.Image
  .attrs({
    source: require('../../../images/appIcon.png'),
    resizeMode: 'contain',
  })`
  flex: 1;
`