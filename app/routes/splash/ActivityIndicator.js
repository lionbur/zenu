import styled from 'styled-components/native'

export default styled.ActivityIndicator
  .attrs({
    size: 'large',
  })`
  position: absolute;
  align-self: center;
  top: 100px;
`