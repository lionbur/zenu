import React, { Component } from 'react'
import {observer, inject} from 'mobx-react'
import {get} from 'lodash'

import Container from './Container'
import AppIcon from './AppIcon'
import ActivityIndicator from './ActivityIndicator'
export default
@inject(
  ({ session:{isLoggedIn}, profile }) =>
    ({ isLoggedIn, profile }))
@observer class extends Component {
  async componentWillReceiveProps(nextProps) {
    if (this.props.isLoggedIn !== nextProps.isLoggedIn) {
      const {navigation: {navigate}, profile} = this.props

      if (!profile.fetching) {
        await profile.fetch()
      } else {
        await profile.ready()
      }

      if (nextProps.isLoggedIn) {
        if (profile.data.isGod || (get(profile.data.adminOf, 'length') > 1)) {
          navigate('God')
        } else if (get(profile.data.adminOf, 'length') === 1) {
          navigate('Restaurant', {slug: profile.data.adminOf[0]})
        } else if (!profile.fetching) {
          navigate('NewUser')
        }
      } else {
        navigate('Home')
      }
    }
  }

  render() {
    return (
      <Container>
        <ActivityIndicator/>
        <AppIcon/>
      </Container>
    )
  }
}