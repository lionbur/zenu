import styled from 'styled-components'

export default styled.h1`
  flex: 1;
  align-self: center;
  text-align: center;
  color: #444;
`