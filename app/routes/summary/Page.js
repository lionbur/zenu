import styled from 'styled-components'

export default styled.div`
  width: 28.7cm;
  height: 19.5cm;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  position: relative;
`