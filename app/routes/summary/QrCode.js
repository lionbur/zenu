import React from 'react'
import styled from 'styled-components'

const Frame = styled.img
  .attrs({
    alt: 'Frame',
    src: require('./MiniMenu/flags/frame.png'),
  })`
  flex: 0;
  width: ${({size}) => /^large$/i.test(size) ? 3.5 :2.5}cm;
  height: ${({size}) => /^large$/i.test(size) ? 3.5 :2.5}cm;
`

const CodeContainer = styled.div`
  position: absolute;
  left: 2.5%;
  top: 0;
  right: 2.5%;
  bottom: 5%;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Code = styled.img
  .attrs({
    src: ({slug}) => `https://api.qrserver.com/v1/create-qr-code/?data=https://translated.menu/of/${slug}`,
    alt: 'QR Code',
  })`
  width: 65%;
  height: 65%;
`

const Container = styled.div`
  position: relative;
  margin-right: 0.2cm;
`

export default ({slug, size}) => (
  <Container>
    <CodeContainer>
      <Code {...{slug}}/>
    </CodeContainer>
    <Frame {...{size}}/>
  </Container>
)

