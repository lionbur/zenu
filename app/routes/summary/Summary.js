import React, { Component } from 'react'
import { action, observable, toJS } from 'mobx'
import { observer, inject } from 'mobx-react'
import {get, find} from 'lodash'
import getSymbolFromCurrency from 'currency-symbol-map'

import { getLanguages, translate } from 'translate'
import Page from './Page'
import Section from './Section'
import Title from './Title'
import MiniMenu from './MiniMenu'
import QrCode from './QrCode'
import Row from './Row'
import Link from './Link'
import Logo from './Logo'

let app
if (process.env.CONFIG === 'fb') {
  app = require('../../../app2')
} else {
  app = require('../../App')
}

const storedLanguage = localStorage.getItem('language')

export default @inject('shop') @inject('params') @inject('profile') @observer class extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params: { slug } } } = navigation

    return {
      title: `${slug} | ${app.displayName}`,
    }
  }

  @observable menuLabels = []

  componentWillMount() {
    const { navigation: { state: { params: { slug } } } } = this.props

    action(async () => {
      this.props.params.slug = decodeURIComponent(slug)

      const languages = getLanguages('')
      languages
        .forEach(async ({code, currencyCode}) => {
          const label = await translate('Menu', {
            from: 'en',
            to: code,
          })

          this.menuLabels.push(<div
            style={{
              position: 'absolute',
              fontSize: `${Math.round(75 + 75 * Math.random())}%`,
              opacity: 0.05 + 0.1 * Math.random(),
              left: `${Math.round(5 + 85 * Math.random())}%`,
              top: `${Math.round(5 + 90 * Math.random())}%`,
              transform: `rotate(${-45 + Math.round(90 * Math.random())}deg)`,
            }}
          >
            {label} {getSymbolFromCurrency(currencyCode)} {currencyCode}
          </div>)
        })
    })()
  }

  componentWillReact() {
    if (typeof document !== 'undefined') {
      document.title = this.props.shop.data
        ?`${this.props.shop.data.name} | ${app.displayName}`
        : app.displayName
    }
  }

  render () {
    const { profile, params, params: {slug}, t } = this.props
    const { data, fetching } = this.props.shop
    const { translationLanguageRequest, translationLanguage } = params

    if (!data) {
      return null
    }

    const langsLeft = [
      'en', 'fr', 'ar', 'es'
//      'zh-CN', 'es', 'en', 'hi', 'ar', 'pt', 'bn', 'ru', 'ja', 'pa',
    ]
    const langsRight = [
      'de', 'zh-CN', 'ja', 'hi'
//      'de', 'jw', 'ms', 'te', 'vi', 'ko', 'fr', 'mr', 'ta', 'ur',
    ]

    return (
      <div>
        <style type="text/css" media="print">{`
          @page {
            size: landscape;
          }
          .evenPage {
            transform: rotate(180deg);
          }
        `}</style>
        <Page>
          <Section isLeft>
            <Row>
              <Logo src={data.logo}/>
              <Title>{data.name}</Title>
            </Row>
            {langsLeft.map(lang =>
              <MiniMenu {...{slug, lang}} key={lang} />
            )}
          </Section>
          <Section>
            <Row>
              <QrCode {...{slug}}/>
              <Link {...{slug}}/>
            </Row>
            {langsRight.map(lang =>
              <MiniMenu {...{slug, lang}} key={lang} />
            )}
          </Section>
        </Page>
        <Page className="evenPage">
          {toJS(this.menuLabels)}
          <Section isLeft>
          </Section>
          <Section>
            <div style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
              <Logo src={data.logo} fill/>
              <QrCode {...{slug}} size="large"/>
              <Link {...{slug}}/>
            </div>
          </Section>
        </Page>
      </div>
    )
  }
}