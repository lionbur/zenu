import styled from 'styled-components'

export default styled.div`
  ${({isLeft}) => isLeft ? 'margin-right: 1cm;' : 'margin-left: 1cm;'}
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 0 1cm;
  justify-content: space-around;
`