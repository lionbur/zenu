import React from 'react'
import styled from 'styled-components'

const Text = styled.span`
  font-size: 0.5cm;
  align-self: center;
  color: blue;
  text-decoration: underline;
  background-color: rgba(255,255,255,.75);
  z-index: 1;
`

export default ({slug}) =>
  <Text>
    {`https://translated.menu/of/${slug}`}
  </Text>