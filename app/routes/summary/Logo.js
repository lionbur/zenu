import styled from 'styled-components'

export default styled.img
  .attrs({
    alt: 'Logo',
  })`
  ${({fill}) => fill ? 'width: 80%;' : 'height: 2cm'};
  margin-bottom: 0.2cm;
`