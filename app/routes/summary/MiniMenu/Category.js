import styled from 'styled-components'

export default styled.span`
  color: black;
  margin-right: 0.2cm;
`