import {inject} from 'mobx-react'

import MiniMenu from './MiniMenu'

const getCategories = ({shop: {language}}) => ({
  shopLanguage: language,
})

export default inject(getCategories)(MiniMenu)


