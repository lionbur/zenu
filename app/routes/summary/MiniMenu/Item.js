import styled from 'styled-components'

export default styled.span`
  color: #444;
  font-size: 75%;
`