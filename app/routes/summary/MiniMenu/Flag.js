import styled from 'styled-components'

const flags = {
  'zh-CN': require('./flags/zh-CN.png'),
  'es': require('./flags/es.png'),
  'en': require('./flags/en.png'),
  'hi': require('./flags/hi.png'),
  'ar': require('./flags/ar.png'),
  'pt': require('./flags/pt.png'),
  'bn': require('./flags/bn.png'),
  'ru': require('./flags/ru.png'),
  'ja': require('./flags/ja.png'),
  'pa': require('./flags/pa.png'),
  'de': require('./flags/de.png'),
  'jw': require('./flags/jw.png'),
  'ms': require('./flags/ms.png'),
  'te': require('./flags/te.jpg'),
  'vi': require('./flags/vi.png'),
  'ko': require('./flags/ko.png'),
  'fr': require('./flags/fr.png'),
  'mr': require('./flags/mr.png'),
  'ta': require('./flags/ta.png'),
  'ur': require('./flags/ur.png'),
}

export default styled.img
  .attrs({
    src: ({lang}) => flags[lang],
    alt: ({lang}) => lang,
  })`
  
  height: 1.5cm;
  margin-right: 0.2cm;
`