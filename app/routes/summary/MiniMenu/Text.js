import styled from 'styled-components'

export default styled.div`
  flex: 1;
  direction: ${({lang}) => ['ar', 'he', 'ur'].includes(lang) ? 'rtl' :'ltr'};
  display: -webkit-box;
  -webkit-line-clamp: 10;
  -webkit-box-orient: vertical; 
  overflow: hidden;
  max-height: 4.0cm;
  line-height: 0.5cm;
`