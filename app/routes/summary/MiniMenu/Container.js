import styled from 'styled-components'

export default styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  max-height: 3.5cm;
  border-radius: 0.5cm;
  border: 0.025cm solid #ccc;
  overflow: hidden;
  padding-right: 0.2cm;
`