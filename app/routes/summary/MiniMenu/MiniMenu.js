import React, {Component} from 'react'
import {observable} from 'mobx'
import {observer} from 'mobx-react'
import {firestore} from 'firebase'

import Text from './Text'
import Container from './Container'
import FlagContainer from './FlagContainer'
import Flag from './Flag'
import FlagText from './FlagText'
import Category from './Category'
import Item from './Item'
import translateMenu from 'store/translateMenu'
import {translate} from 'translate'

export default @observer class extends Component {
  @observable summary = ''
  @observable menuLabel = ''

  async componentWillMount() {
    const {slug, shopLanguage, lang} = this.props
    const path = lang === shopLanguage
      ? [ 'restaurants', slug, 'categories'].join('/')
      : [ 'restaurants', slug, 'translations', lang, 'categories'].join('/')

    this.menuLabel = await translate('Menu', { from: 'en', to: lang})

    const snapshot = (await firestore().collection(path)
//      .orderBy('order')
      .get())

    if (snapshot.empty) {
      await translateMenu(lang)
    }

    const categories = []

    snapshot
      .forEach(doc => {
        categories.push((async () => {
          const items = (await firestore()
            .collection([doc.ref.path, 'items'].join('/'))
//            .orderBy('order')
            .get())

          if (!items.empty) {
            const names = items
              .docs
              .map(doc => doc.data().name)
              .join(', ')
//            const data = items.docs[0].data()
 //           const {name} = data

            return {
              category: doc.data().name,
              item: names
            }
          } else {
            await translateMenu(lang)
          }

          return 'MISSING'
        })())
      })


    this.summary = (await Promise.all(categories))
      .filter(line => line)
      .map(({category, item}, index) => (<span>
        {index ? <Item> | </Item> : null}
        <Category>{category}:</Category>
        <Item>{item}</Item>
      </span>))
  }

  render() {
    const {lang} = this.props

    return (
      <Container>
        <FlagContainer>
          <Flag {...{lang}}/>
          <FlagText>{this.menuLabel}</FlagText>
        </FlagContainer>
        <Text {...{lang}}>
          {this.summary}
        </Text>
      </Container>
    )
  }
}
