import styled from 'styled-components'

export default styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  text-align: center;
  color: #448;
  text-shadow: 0.05cm 0.05cm 0.1cm black;
`