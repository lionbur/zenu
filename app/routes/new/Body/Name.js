import styled from 'styled-components/native'

import { DataTextInput } from 'styled'

export default styled(DataTextInput)
  .attrs({
    name: 'name',
    placeholder: 'Name of my business',
    rapidUpdate: true,
  })`
  border: 1px solid rgba(0,0,0,.5);
  border-radius: 5px;
  padding: 5px;
  align-self: stretch;
  margin-left: 10%;
  margin-right: 10%;
  margin-top: 20px;
  margin-bottom: 20px;
  max-width: 400px;
  background-color: rgba(255,255,255,.5);
  box-shadow: 0 0 5px;
`
