import styled from 'styled-components/native'

export default styled.Text`
  color: #444;
  font-family: arial;
  font-size: 16px;
  align-self: flex-start;
`