import React, {Component} from 'react'
import {observable} from 'mobx'
import {observer, inject} from 'mobx-react'
import {get} from 'lodash'

import Container from './Container'
import Name from './Name'
import Text from './Text'
import AlreadyTakenText from './AlreadyTakenText'
import Langauge from './Langauge'
import LinkText from './LinkText'
import CreateButton from './CreateButton'
import {getIdFromName} from 'store/helpers'
import {Document} from 'services'
import {transcribe, detectLocale} from 'translate'

const getSlug = name => transcribe(getIdFromName(name))

export default @inject("session") @observer class extends Component {
  @observable data = {
    name: '',
    language: detectLocale(),
    isAlreadyTaken: null,
  }
  lastSlug = null

  async componentWillReact() {
    const { name } = this.data

    if (name) {
      const slug = getSlug(name)

      if (this.lastSlug !== slug) {
        this.data.isAlreadyTaken = null

        const restaurant = new Document(['restaurants', slug].join('/'), {mode: 'on'})
        await restaurant.ready()

        this.data.isAlreadyTaken = !!restaurant.data
        this.lastSlug = slug
      }
    } else {
      this.data.isAlreadyTaken = false
    }
  }

  render() {
    const { onCreate, t, session } = this.props
    const { data } = this
    const { name, language } = data
    const slug = getSlug(name)
    const displayName = get(session, 'user.displayName') || ''

    return (
      <Container>
        <Text>{t('new:help', {displayName})}</Text>
        <Name store={data}/>
        <Langauge {...{t}} store={data}/>
        <Text>{t('new:linkWillBe')}</Text>
        <LinkText>translated.menu/of/{slug}</LinkText>
        {data.isAlreadyTaken && <AlreadyTakenText>{t('new:alreadyTaken')}</AlreadyTakenText>}
        <CreateButton
          disabled={!name || data.isAlreadyTaken !== false}
          onPress={() => onCreate({ name, slug, language })}
          {...{t}}
        />
      </Container>
    )
  }
}


