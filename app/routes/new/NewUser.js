import React from 'react'
import {firestore} from 'firebase'
import {inject} from 'mobx-react'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'
import {Collection, Document} from 'services'

const handleCreate = async (session, { name, slug, language }) => {
  const restaurant = new Document(['restaurants', slug].join('/'), {mode:'on'})
  await restaurant.ready()

  if (!restaurant.data) {
    const restaurants = new Collection('restaurants')
    console.log(session)
    const user = new Document(['users', session.user.uid].join('/'), {mode:'on'})
    await user.ready()

    await restaurants.ref.doc(slug).set({
      name,
      language,
      timeCreated: firestore.FieldValue.serverTimestamp(),
      headerImage: 'https://media.giphy.com/media/S1PNogIEHI4aQ/giphy.gif',
    })

    await user.update({
      adminOf: [
        slug,
      ],
      timeLastModified: firestore.FieldValue.serverTimestamp(),
    })
  }
}

const enhance = inject('session')

export default enhance(({ session, navigation: { navigate }, t }) => (
  <Container>
    <Header/>
    <Body onCreate={async params => {
      await handleCreate(session, params)
      navigate('Restaurant', { slug: params.slug })
    }} {...{t}}/>
    <Footer/>
  </Container>
))