import React, {Component} from 'react'
import {observable} from 'mobx'
import {observer, inject} from 'mobx-react'
import {get, omit, uniqBy} from 'lodash'
import locale2 from 'locale2'

import Container from './Container'
import Langauge from './Langauge'
import CreateButton from './CreateButton'
import {Field} from 'styled'
import {getIdFromName} from 'store/helpers'
import {transcribe} from 'translate'
import {Document} from 'services'

let locale = process.env.NODE_ENV === 'production' ? locale2 : 'en'
const getLocale = () => process.env.CONFIG === 'fb' ? locale : 'en'

const getSlug = name => transcribe(getIdFromName(name))

const readStringFromFile = file => new Promise(resolve => {
  const reader = new FileReader()

  reader.onload = ({ target:{result} }) => {
    resolve(result)
  }

  reader.readAsText(file)
})

const handleCsvText = text => {
  const rows = []
  let row = []
  let isInQoute = false
  let value = ''

  for (let i = 0; i <= text.length; i++) {
    const ch = text.charAt(i)

    if (ch === '"') {
      if (isInQoute) {
        if (text.charAt(i + 1) === '"') {
          value += '"'
          i++
        } else {
          isInQoute = false
        }
      } else {
        isInQoute = true
      }
    } else if (!isInQoute && ch === '\n' || i === text.length) {
      if (i < text.length || text[i - 1] !== '\n') {
        row.push(value)
        rows.push(row)
      }
      row = []
      value = ''
    } else if (!isInQoute && ch === ',') {
      row.push(value)
      value = ''
    } else {
      value += ch
    }
  }

  return rows
}

const services = [
  'Air conditioner',
  'TV',
  'Wifi',
  'Water purifier',
  'Gas stove',
  'Hair dryer',
  'Table',
  'Refrigerator',
  'Kitchen supplies',
  'Washing tool',
  'Smoking',
  'Breakfast',
  'Pickup',
  'Wheelchair',
  'ramp',
  'Cuddle puddle',
  'Coffee pot',
  'Seminar room',
  'Parking lot',
  'Fan',
  'Sauna',
  'Closets',
  'Elevator',
  'Bidet',
  'Store',
  'PC',
]

const serviceIcons = {
  'Air conditioner': 'http://stay.gn.go.kr/image/sub/pictogram/aircon.png',
  'TV': 'http://stay.gn.go.kr/image/sub/pictogram/tv.png',
  'Wifi': 'http://stay.gn.go.kr/image/sub/pictogram/wifi.png',
  'Water purifier': 'http://stay.gn.go.kr/image/sub/pictogram/warter.png',
  'Gas stove': 'http://stay.gn.go.kr/image/sub/pictogram/fire.png',
  'Hair dryer': 'http://stay.gn.go.kr/image/sub/pictogram/dryer.png',
  'Table': 'http://stay.gn.go.kr/image/sub/pictogram/table.png',
  'Refrigerator': 'http://stay.gn.go.kr/image/sub/pictogram/fridge.png',
  'Kitchen supplies': 'http://stay.gn.go.kr/image/sub/pictogram/kitchen.png',
  'Washing tool': 'http://stay.gn.go.kr/image/sub/pictogram/wash.png',
  'Smoking': 'http://stay.gn.go.kr/image/sub/pictogram/smoke.png',
  'Breakfast': 'http://stay.gn.go.kr/image/sub/pictogram/breakfast.png',
  'Pickup': 'http://stay.gn.go.kr/image/sub/pictogram/pickup.png',
  'Wheelchair ramp': 'http://stay.gn.go.kr/image/sub/pictogram/wheelchair.png',
  'Cuddle puddle': 'http://stay.gn.go.kr/image/sub/pictogram/cuddle.png',
  'Coffee pot': 'http://stay.gn.go.kr/image/sub/pictogram/coffeepot.png',
  'Seminar room': 'http://stay.gn.go.kr/image/sub/pictogram/seminar.png',
  'Parking lot': 'http://stay.gn.go.kr/image/sub/pictogram/parking.png',
  'Fan': 'http://stay.gn.go.kr/image/sub/pictogram/electric-fan.png',
  'Sauna': 'http://stay.gn.go.kr/image/sub/pictogram/sauna.png',
  'Closets': 'http://stay.gn.go.kr/image/sub/pictogram/closet.png',
  'Elevator': 'http://stay.gn.go.kr/image/sub/pictogram/elevator.png',
  'Bidet': 'http://stay.gn.go.kr/image/sub/pictogram/bidet.png',
  'Store': 'http://stay.gn.go.kr/image/sub/pictogram/cafeteria.png',
  'PC': 'http://stay.gn.go.kr/image/sub/pictogram/pc.png',
}

const koreanTranslations = {
  'Refrigerator': '냉장고',
  'Air conditioner': '에어컨',
  'Water purifier': '정수기',
  'Gas stove': '가스레인지',
  'Hair dryer': '드라이기',
  'Parking lot': '주차장',
  'Coffee pot': '커피포트',
  'Kitchen supplies': '주방용품',
  'Closets': '옷장',
  'Washing tool': '세면도구',
  'Fan': '선풍기',
  'Store': '매점',
  'Breakfast': '조식',
  'Pickup': '픽업',
  'Table': '식탁',
  'Bidet': '비데',
  'Whole': '전체',
  'Bed room': '침대 ',
  'Ondol room)': '온돌',
  'Smoking': '흡연',
}

const extractTokens = (text, tokens) =>  {
  const result = []

  text = text
    .replace('null', '')

  tokens
    .forEach(service => {
      const nextText = text.replace(service, '')
      if (nextText !== text) {
        result.push(service)
        text = nextText
      }
    })

  if (text) {
    console.log('ERROR', text)
  }

  return result
}

const extractServices = text => extractTokens(text, services)

const extractLanguages = text => extractTokens(text, [
  'English',
  'Pусский',
  '中文',
  '日本語',
  'Español',
  'Français',
  'Deutsch',
])

const extractRooms = (text, minPrice, maxPrice) => {
  const match = text.match(/^(.*)\((.*)\)$/)
  const counters = match[1]
    .split(/\s*\/\s*/)
  const names = match[2]
    .split(/\s*\/\s*/)
  const numTypes = counters
    .filter(counter => counter != '0')
    .length

  return counters
    .filter(counter => counter !== '0')
    .map((counter, index) => ({
      counter: parseInt(counter),
      name: names[index],
      price: maxPrice - (maxPrice - minPrice) * index / (numTypes - 1),
    }))
}

const extractPrice = text => {
  const exp = /^(.*) won$/

  if (!exp.test(text)) {
    console.log('ERROR', text)
  }

  return parseInt(text
    .match(exp)[1]
    .replace(/,/g, ''))
}

const extraServices = [
  [/주차장/, 'Parking lot' ],
  [/세탁서비스/, 'Laundry Service'],
  [/바베큐장/, 'Barbecue Zone'],
]

const extractExtraServices = services =>
  services
    .split(', ')
    .map(service => extraServices
      .reduce((result, [exp, name]) => exp.test(service)
        ? { name, description: service }
        : result, null))
    .filter(service => service)

const cleanNotes = notes =>
  notes
    .split(', ')
    .filter(note => !extraServices
      .some(([exp]) => exp.test(note)))


const extractSlugFromUrl = text =>  {
  if (!text) {
    return text
  }

  const url = new URL(text)

  const slug = url
    .hostname
    .split('.')
    .filter(part => !['www', 'com', 'net', 'org', 'co', 'me', 'blog', 'naver', 'booking', 'cafe', 'or', 'ne', 'm',
      "go","friendshome", "modoo", "at","healingyearn","kpbeach","gplake","topbest","alpspension","analog76",
      "imagineahouse","awoolim","santabeach","haepoomdal","santorinips","okqr","badahyanggi-minbak","sogeumgang",
      "xn--2q1bs79a8gbj0p3kg","solguest","ssig","gadunji","pinepalace","yatravel","bbeach","davincimotel","herapension",
      "instagram","darkgreen","pensiondao","hyooro","gplacvert","scenic94pension", "wp"].includes(part))
    .slice(-2)
    .join('-')

  return slug.length > 4
    ? slug
    : ''
}

function renderImage (url, scale = 0.3) {
  let img = new Image()

  img.onload = () => {
    const style = `
      display: block !important;
      margin: 10px 0;
      font-size: ${img.height * scale}px;
      padding: ${Math.floor(img.height * scale/2)}px ${Math.floor(img.width * scale/2)}px;
      background: url(${url});
      background-size: ${img.width * scale}px ${img.height * scale}px;
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
    `
    console.log('%c', style)
  }

  img.src = url
}

console.image = renderImage

const extractLogo = async url => {
  if (!url) {
    return url
  }

  try {
    const response = await fetch(url, {mode: 'cors'})
    const html = await response.text()
    const exp = /src=["'](.*?)["'][\s>]/g
    let match

    while (match = exp.exec(html)) {
      if (/logo(.*?)\.(png|jpg|jpeg)/i.test(match[1])) {
        const src = new URL(match[1], url)
        if (src.hotname === url.hostname) {
          return src.href
        }
      }
    }
  } catch (err) {}

  return ''
}

const handleCsv = async file => {
  const text = (await readStringFromFile(file))

  const lines = handleCsvText(text
    .replace(/\n\r/g, '\n')
    .replace(/\r\n/g, '\n')
    .replace(/\r/g, '\n'))

  const fields = lines.shift()

  const json = await Promise.all(lines
//    .slice(0, 10)
    .map(row => row
      .reduce((rowObj, value, index) => ({
        ...rowObj,
        [fields[index]]: value,
      }), {})
    )
    .map(async row => ({
      ...row,
      gallery: JSON.parse(row.gallery)
        .map((galleryItem => galleryItem['gallery-src']))
        .map(uri => new URL(uri, row['link-href']).href),
      services: extractServices(row.services)
        .map(name => ({name})),
      specialNote: cleanNotes(row.specialNote),
      rooms: extractRooms(row.rooms, extractPrice(row.lowestPrice), extractPrice(row.highestPrice)),
      phones: [row.contact1, row.contact2]
        .filter(contact => contact !== '-  -')
        .map(contact => '+82-' + contact
          .replace(/^0/, '')
          .replace(/\s+-\s+/g, '-'),
        ),
      languages: extractLanguages(row.languages),
      lowestPrice: extractPrice(row.lowestPrice),
      highestPrice: extractPrice(row.highestPrice),
      slug: extractSlugFromUrl(row['website-href']) || getSlug(row.englishName),
      extraServices: extractExtraServices(row.specialNote),
      website: row['website-href'],
      logo: extractSlugFromUrl(row['website-href']) && await extractLogo(row['website-href']),
    })))

  const slugs = json.map(({slug}) => slug)
  const hist = slugs
    .reduce((result, slug) => ({
      ...result,
      [slug]: (result[slug] || 0) + 1,
    }), {})
  const dup = Object
    .entries(hist)
    .filter(([_,count]) => count > 1)
    .map(([slug]) => slug)
  console.log(JSON.stringify(dup.filter(slug => slug).map(slug => slug.replace(/-kr$/, ''))))

  console.log(json.filter(({extraServices}) => extraServices.length))

  const result = json
    .map(({
      englishName: name,
      koreanName: nativeName,
      services,
      extraServices,
      gallery,
      specialNote,
      category,
      rooms,
      lowestPrice,
      highestPrice,
      contact1,
      contact2,
      link,
      languages,
      checkinTime,
      checkoutTime,
      ...rest
    }) => ({
      name,
      description: specialNote.join('\n'),
      nativeName,
      headerImage: gallery[0],
      gallery,
      currencyCode: 'KRW',
      categories: [
        {
          name: '방',
          items: rooms
            .map(({name, price, counter }, index) => ({
              name: koreanTranslations[name] || name,
              price,
              description: `${counter} 실\n입실시간 ${checkinTime}\n퇴실시간 ${checkoutTime}`,
              image: gallery[(index + 1) % gallery.length],
            }))
        }, {
          name: '서비스/편의시설',
          description: `${name}`,
          items: [
            ...uniqBy([...extraServices, ...services], 'name')
              .map(({name, description}) => ({
                name: koreanTranslations[name] || name,
                description,
                image: serviceIcons[name] || '',
              }))
          ],
        }, {
          name: '외국어',
          items: languages
            .map(name => ({name}))
        }, services.find(({name}) => name == 'Breakfast')
          ? { name: '아침 식사' }
          : null,
        services.find(({name}) => name == 'Cafe')
          ? { name: '조식' }
          : null,
        services.find(({name}) => name == 'Store')
          ? { name: '매점' }
          : null,
        services.find(({name}) => name == 'TV')
          ? { name: 'TV 채널' }
          : null,
      ]
        .filter(category => category),
      checkinTime,
      checkoutTime,
      ...rest
    }))

  console.log(result)

  return result
}

export default @inject("session") @observer class extends Component {
  @observable data = {
    language: 'ko',
    menus: null,
  }

  handleFileChange = event => {
    const { files } = event.nativeEvent.target

    Array
      .from(files)
      .forEach(async file => {
        this.data.menus = await handleCsv(file)
      })
  }

  render() {
    const { onCreate, t, session } = this.props
    const { data } = this
    const { language, menus } = data

    return (
      <Container>
        <Langauge {...{t}} store={data}/>
        <input type="file" onChange={this.handleFileChange}/>
        <CreateButton
          disabled={!menus}
          onPress={() => onCreate({ language, menus })}
          {...{t}}
        />
      </Container>
    )
  }
}


