import React from 'react'
import { Picker } from 'react-native'
import styled from 'styled-components/native'

import { getLanguages } from 'translate'
import { Label } from 'styled'

const Container = styled.View`
  align-self: center;
  border: 1px solid rgba(0,0,0,.5);
  border-radius: 5px;
  padding: 3px 5px;
  align-self: stretch;
  margin-left: 10%;
  margin-right: 10%;
  margin-bottom: 20px;
  max-width: 400px;
  background-color: rgba(255,255,255,.5);
  box-shadow: 0 0 5px;
`

export default ({ t, store, name }) => (
  <Container>
    <Label label={t('new:language')}>
      <Picker
        selectedValue={store.language}
        onValueChange={value => { store.language = value }}>{
        getLanguages('Please Select')
          .map(({ name, code}) =>
            <Picker.Item key={code} label={name} value={code}/>)
      }</Picker>
    </Label>
  </Container>
)