import styled from 'styled-components/native'

export default styled.Text`
  color: blue;
  font-family: arial;
  font-size: 18px;
  align-self: center;
  text-decoration: underline;
  margin-top: 10px;
`