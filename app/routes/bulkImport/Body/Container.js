import styled from 'styled-components/native'

export default styled.View`
  flex: 1;

  background-color: #eee;
  padding: 20px;
`