import React from 'react'
import {firestore} from 'firebase'
import {inject} from 'mobx-react'
import {getIdFromName} from 'store/helpers'
import {transcribe} from 'translate'
import {storage} from 'firebase'

import Container from './Container'
import Header from 'common/Header'
import Footer from 'common/Footer'
import Body from './Body'
import {Collection, Document} from 'services'

const getSlug = name => transcribe(getIdFromName(name))

const storageUrlFromUrl = async ({ url, path }) => {
  if (/^data:/.test(url)) {
    return ''
  }
  if (!url) {
    return url
  }

  try {
    console.log('Downloading', url)

    const response = await fetch(url, {mode: 'cors'})
    const blob = await response.blob()

    console.log('Uploading', blob, path)

    return (await storage()
      .ref(path)
      .put(blob, {
        contentType: blob.type,
        customMetadata: {
          url,
          size: blob.size,
          timeCreated: (new Date).toISOString(),
        },
      }))
      .downloadURL
  } catch (error) {
    console.log(error)
    return url
  }
}

const deleteAll = collection => Promise.all(
  collection
    .docs
    .map(doc => doc.delete())
)

const handleCreate = async (session, { language, menus }) => {
//  const restaurant = new Document(['restaurants', slug].join('/'), {mode:'on'})
//  await restaurant.ready()

//  if (!restaurant.data) {
  const restaurants = new Collection('restaurants')
  const operations = []

  menus.
    forEach(async menu => {
    await restaurants.ref.doc(menu.slug).set({
      name: menu.name,
      nativeName: menu.nativeName,
      checkinTime: menu.checkinTime,
      checkoutTime: menu.checkoutTime,
      currencyCode: menu.currencyCode,
      headerImage: menu.headerImage,
      address: menu.address,
      email: menu.email,
      website: menu.website,
      language,
      timeCreated: firestore.FieldValue.serverTimestamp(),
    })

    const translations = new Collection([restaurants.path, menu.slug, 'translations'].join('/'), {mode:'on'})
    await translations.ready()
    await deleteAll(translations)

/*
    const phones = new Collection([restaurants.path, menu.slug, 'phones'].join('/'), {mode:'on'})
    await phones.ready()
//    await deleteAll(phones)
    menu.phones
      .forEach(phoneNumber =>
        operations.push(
          phones.add({
            phoneNumber
          })
        ))

    const gallery = new Collection([restaurants.path, menu.slug, 'gallery'].join('/'), {mode:'on'})
    await gallery.ready()
//    await deleteAll(gallery)
    menu.gallery
      .forEach(image =>
        operations.push(
          gallery.add({
            image
          })
        ))
*/
    const categories = new Collection([restaurants.path, menu.slug, 'categories'].join('/'), {mode:'on'})
    await categories.ready()
    await deleteAll(categories)

    menu
      .categories
      .forEach(({name, items = []}, index) => {
        const id = getSlug(name)
        operations.push(
          categories.ref.doc(id).set({
            name,
            timeCreated: firestore.FieldValue.serverTimestamp(),
            order: index + 1,
          }))

        const itemsColl = new Collection([categories.path, id, 'items'].join('/'))
        const numberExp = /^\d+\.\d+|[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)?$/

        items
          .forEach(({name, description = '', price = 0, image = ''}, index) => {
            const id = getSlug(name)
            let variations

            if (typeof price === 'string' && price.includes('/')) {
              const parts = price
                .split('/')
                .map(part => part
                  .trim())

              const annoymousDescriptions = [
                [],
                ['M'],
                ['M', 'L'],
                ['S', 'M', 'L'],
                ['S', 'M', 'L', 'XL'],
                ['S', 'M', 'L', 'XL', 'XXL'],
                ['XS', 'S', 'M', 'L', 'XL', 'XXL'],
              ]

              const prices = parts
                .filter(part => numberExp.test(part))
              const descriptions = parts
                .filter(part => !prices.includes(part))
              const addParentesis = text => /\(.*?\)/.test(text)
                ? ` ${text}`
                : ` (${text})`
              const alternativeDescriptions = annoymousDescriptions[prices.length]
                || prices
                  .map((_, index) => `No${index}`)

              variations = prices
                .map((price, index) => ({
                  id: `${id}-${index + 1}`,
                  name: name
                  + addParentesis(descriptions[index] || alternativeDescriptions[index]),
                  price,
                }))
            } else {
              variations = [{
                id,
                name,
                price,
              }]

              if (!numberExp.test(price)) {
                description = description
                  ? `${description} (${price})`
                  : price
              }
            }

            variations
              .forEach(({
                          id,
                          name,
                          price,
                        }) => {
                console.log(name, price, description, image)
                operations
                  .push((async () =>
                    itemsColl.ref.doc(id).set({
                      name,
                      description,
                      price: parseFloat(typeof price === 'string'
                        ? price
                          .replace(/,/g, '')
                        : price),
                      thumbnail: image ,
                      timeCreated: firestore.FieldValue.serverTimestamp(),
                      order: index + 1,
                    }))()
                  )
              })
          }) // items
      }) // categories
  }) // menus
/*
await storageUrlFromUrl({
                        url: image,
                        path: [itemsColl.path, id].join('/')
                      })
* */
  const table = menus
    .map(({name,phones,email,website,address,slug}) =>
      `${name}\t${phones.join(', ')}\t${email}\thttps://translated.menu/of/${slug}\t${website}\t${address}`
    )
    .join('\n')
  console.log('List of generated venues')
  console.log(table)

    console.log(operations)
    await Promise.all(operations)
//  }
}

const enhance = inject('session')

export default enhance(({ session, navigation: { navigate }, t }) => (
  <Container>
    <Header/>
    <Body onCreate={async params => {
      await handleCreate(session, params)
      navigate('Restaurant', { slug: params.menus[0].slug })
    }} {...{t}}/>
    <Footer/>
  </Container>
))