import styled from 'styled-components/native'
import { Field } from 'styled'

export default styled(Field)
  .attrs({
    name: 'name',
    placeholder: 'Business Name'
  })`
  font-size: 1.25em;
  
  line-height: 2rem;
  font-weight: 600;
`
