import React from 'react'
import styled from 'styled-components/native'

const Button = styled.Button`
  font-size: 1.25em;
  text-align: left;
  line-height: 2rem;
  font-weight: 600;
`

const Container = styled.View`
  margin-top: 5px;
  margin-bottom: 5px;  
`

export default props => (
  <Container>
    <Button {...props}/>
  </Container>
)