import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { ActivityIndicator } from 'react-native'
import { NavigationActions } from 'react-navigation'

import Container from './Container'
import Name from './Name'
import Button from './Button'

export default @inject('profile') @inject('params') @inject('shop') @inject('shops') @observer class extends Component {
  @observable isFetching = false

  render () {
    const { shops, navigation: { dispatch }, profile } = this.props

    return (
      <Container>
        {this.isFetching && <ActivityIndicator/>}
        {shops
          .docsWithNew
            .filter(({ id }) => profile.data
              && (profile.data.isGod
                || (profile.data.adminOf && profile.data.adminOf.includes(id)))
            )
            .map(shop => shop.id
              ? <Button
                  key={shop.path}
                  title={shop.data.name}
                  onPress={() => {
                    this.isFetching = true
                    this.props.params.slug = shop.id
                    this.props.shop.fetch().then(_ => {
                      dispatch(NavigationActions.reset({
                        index: 0,
                        actions: [
                          NavigationActions.navigate({
                            routeName: 'Restaurant',
                            params: {
                              slug: shop.id
                            }
                          })
                        ]
                      }))
                      this.isFetching = false
                    })
                  }}
                />
              : <Name
                  key={shop.path}
                  store={shop.data}
                />
              )}
      </Container>
    )
  }
}
