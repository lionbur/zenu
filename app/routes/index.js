import Splash from './splash'
import God from './god'
import Home from './home'
import SignUp from './signup'
import NewUser from './new'
import Import from './import'
import BulkImport from './bulkImport'
import RestaurantView from './restaurant'
import Summary from './summary'
import NotFound from './NotFound'
import {New, Cancelled, Success} from './subscribe'
import Wheel from './games/wheel'
import WheelRoom from './games/wheel/room'

import {session} from 'store'

let app
if (process.env.CONFIG === 'fb') {
  app = require('../../app2')
} else {
  app = require('../../app')
}


export default {
  Splash: {
    screen: Splash,
    path: '',
    navigationOptions: {
      title: `${app.displayName}`,
    }
  },
  Home: {
    screen: Home,
    path: 'home',
    navigationOptions: {
      title: `${app.displayName}`,
    }
  },
  SignUp: {
    screen: SignUp,
    path: 'signup',
    navigationOptions: {
      title: `Sign In/Up to ${app.displayName}`,
    }
  },
  NewUser: {
    screen: NewUser,
    path: 'new',
    navigationOptions: {
      title: `Welcome to ${app.displayName}`,
    }
  },
  BulkImport: {
    screen: BulkImport,
    path: 'bulk',
    navigationOptions: {
      title: `Bulk Import | ${app.displayName}`,
    }
  },
  Import: {
    screen: Import,
    path: 'import',
    navigationOptions: {
      title: `Import | ${app.displayName}`,
    }
  },
  God: {
    screen: God,
    path: 'god',
    navigationOptions: {
      title: `${app.displayName}`,
    }
  },
  NewSubscription: {
    screen: New,
    path: 'of/:slug/subscribe/new',
    navigationOptions: {
      title: 'Subscribe to Translated Menu',
    },
  },
  CancelledSubscription: {
    screen: Cancelled,
    path: 'of/:slug/subscribe/cancelled',
    navigationOptions: {
      title: 'Translated Menu',
    },
  },
  DoneSubscription: {
    screen: Success,
    path: 'of/:slug/subscribe/success',
    navigationOptions: {
      title: 'Thank you!',
    },
  },
  Restaurant: {
    screen: RestaurantView,
    path: 'of/:slug',
  },
  Summary: {
    screen: Summary,
    path: 'of/:slug/summary',
  },
  Wheel: {
    screen: Wheel,
    path: 'of/:slug/wheel',
  },
  WheelRoom: {
    screen: WheelRoom,
    path: 'of/:slug/wheel/:room',
  },
  NotFound: {
    screen: NotFound,
    navigationOptions: {
      title: `Not Found | ${app.displayName}`,
    }
  }
}
