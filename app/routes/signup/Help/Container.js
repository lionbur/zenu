import styled from 'styled-components/native'

export default styled.View`
  position: fixed;
  bottom: 0px;
  flex-direction: row;
  padding-bottom: 10px;
`