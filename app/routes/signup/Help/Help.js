import React from 'react'

import Container from './Container'
import Link from '../../../common/Link'
import InfoText from './InfoText'

const infoText = 'Ⓘ Having troubles logging in?'

export default () => (
  <Container>
    <InfoText>{infoText}</InfoText>
    <Link href="mailto:lion@translated.menu?subject=I need help with signing up!">Send us an email!</Link>
  </Container>
)