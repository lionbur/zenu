import React, { Component } from 'react'
import { FirebaseAuth } from 'react-firebaseui'
import firebase from 'firebase'

import Container from './Container'
import Help from './Help'

const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: 'popup',
  // Redirect to / after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: '/',
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID
  ]
};

export default class extends Component {
  render() {
    return (
      <Container>
        <FirebaseAuth {...{uiConfig}} firebaseAuth={firebase.auth()}/>
        <Help/>
      </Container>
    )
  }
}