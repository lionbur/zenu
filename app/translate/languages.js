import { getNativeName as _getNativeName } from 'iso-639-1'

const extraNativeNames = {
  iw: 'עברית',
}

const getNativeName = (code, name) => {
  const nativeName = _getNativeName(code)

  if (nativeName === name) {
    return name
  }
  return nativeName || extraNativeNames[code] || name
}

const languages = [
  [ 'Afrikaans', 'af' ],
  [ 'Albanian', 'sq' ],
  [ 'Amharic', 'am' ],
  [ 'Arabic', 'ar' ],
  [ 'Armenian', 'hy' ],
  [ 'Azzeerbaijani', 'az' ],
  [ 'Basque', 'eu' ],
  [ 'Belarusian', 'be' ],
  [ 'Bengali', 'bn' ],
  [ 'Bosnian', 'bs' ],
  [ 'Bulgarian', 'bg' ],
  [ 'Catalan', 'ca' ],
  [ 'Cebuano', 'ceb' ],
  [ 'Chinese (Simplified)', 'zh-CN', 'CNY' ],
  [ 'Chinese (Traditional)', 'zh-TW', 'CNY' ],
  [ 'Corsican', 'co' ],
  [ 'Croatian', 'hr', 'EUR' ],
  [ 'Czech', 'cs', 'EUR' ],
  [ 'Danish', 'da', 'EUR' ],
  [ 'Dutch', 'nl', 'EUR' ],
  [ 'English', 'en', 'USD' ],
  [ 'Esperanto', 'eo', 'EUR' ],
  [ 'Estonian', 'et', 'EUR' ],
  [ 'Finnish', 'fi', 'EUR' ],
  [ 'French', 'fr', 'EUR' ],
  [ 'Frisian', 'fy' ],
  [ 'Galician', 'gl' ],
  [ 'Georgian', 'ka', 'RUB' ],
  [ 'German', 'de', 'EUR' ],
  [ 'Greek', 'el', 'EUR' ],
  [ 'Gujarati', 'gu' ],
  [ 'Haitian Creole', 'ht' ],
  [ 'Hausa', 'ha' ],
  [ 'Hawaiian', 'haw' ],
  [ 'Hebrew', 'he', 'ILS' ],
  [ 'Hindi', 'hi', 'INR' ],
  [ 'Hmong', 'hmn' ],
  [ 'Hungarian', 'hu', 'EUR' ],
  [ 'Icelandic', 'is', 'EUR' ],
  [ 'Igbo', 'ig' ],
  [ 'Indonesian', 'id' ],
  [ 'Irish', 'ga', 'EUR' ],
  [ 'Italian', 'it', 'EUR' ],
  [ 'Japanese', 'ja', 'JPY' ],
  [ 'Javanese', 'jw' ],
  [ 'Kannada', 'kn' ],
  [ 'Kazakh', 'kk' ],
  [ 'Khmer', 'km' ],
  [ 'Korean', 'ko', 'KRW' ],
  [ 'Kurdish', 'ku' ],
  [ 'Kyrgyz', 'ky' ],
  [ 'Lao', 'lo' ],
  [ 'Latin', 'la' ],
  [ 'Latvian', 'lv' ],
  [ 'Lithuanian', 'lt' ],
  [ 'Luxembourgish', 'lb' ],
  [ 'Macedonian', 'mk' ],
  [ 'Malagasy', 'mg' ],
  [ 'Malay', 'ms' ],
  [ 'Malayalam', 'ml' ],
  [ 'Maltese', 'mt' ],
  [ 'Maori', 'mi' ],
  [ 'Marathi', 'mr' ],
  [ 'Mongolian', 'mn' ],
  [ 'Myanmar (Burmese)', 'my' ],
  [ 'Nepali', 'ne' ],
  [ 'Norwegian', 'no' ],
  [ 'Nyanja (Chichewa)', 'ny' ],
  [ 'Pashto', 'ps' ],
  [ 'Persian', 'fa', 'IRR' ],
  [ 'Polish', 'pl', 'EUR' ],
  [ 'Portuguese (Portugal, Brazil)', 'pt', 'BRL' ],
  [ 'Punjabi', 'pa' ],
  [ 'Romanian', 'ro', 'EUR' ],
  [ 'Russian', 'ru', 'RUB' ],
  [ 'Samoan', 'sm' ],
  [ 'Scots Gaelic', 'gd' ],
  [ 'Serbian', 'sr', 'EUR' ],
  [ 'Sesotho', 'st' ],
  [ 'Shona', 'sn' ],
  [ 'Sindhi', 'sd' ],
  [ 'Sinhala (Sinhalese)', 'si' ],
  [ 'Slovak', 'sk', 'EUR' ],
  [ 'Slovenian', 'sl', 'EUR' ],
  [ 'Somali', 'so' ],
  [ 'Spanish', 'es', 'EUR' ],
  [ 'Sundanese', 'su' ],
  [ 'Swahili', 'sw' ],
  [ 'Swedish', 'sv', 'EUR' ],
  [ 'Tagalog (Filipino)', 'tl' ],
  [ 'Tajik', 'tg' ],
  [ 'Tamil', 'ta' ],
  [ 'Telugu', 'te' ],
  [ 'Thai', 'th', 'HTB' ],
  [ 'Turkish', 'tr', 'EUR' ],
  [ 'Ukrainian', 'uk', 'UAH' ],
  [ 'Urdu', 'ur' ],
  [ 'Uzbek', 'uz' ],
  [ 'Vietnamese', 'vi', 'VND' ],
  [ 'Welsh', 'cy' ],
  [ 'Xhosa', 'xh' ],
  [ 'Yiddish', 'yi' ],
  [ 'Yoruba', 'yo' ],
  [ 'Zulu', 'zu' ],
]

export default emptyLabel => [
  [ emptyLabel, '_' ],
  ...languages,
]
  .map(([ name, code, currencyCode ]) => ({
    name: getNativeName(code, name),
    code,
    currencyCode
  }))
