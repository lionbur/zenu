import locale2 from 'locale2'

let locale = process.env.NODE_ENV === 'production' ? locale2 : 'en'
export default () => process.env.CONFIG === 'fb' ? locale : 'en'
