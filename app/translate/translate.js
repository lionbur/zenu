import queryParams from './queryParams'

const uri = 'https://translate.googleapis.com/translate_a/single?'

export default async (text, { from, to }) => {
  if (!text) {
    return text
  }
  
  const response = await fetch(
    `${uri}${queryParams({
      client: 'gtx',
      dt: 't',
      sl: from || 'auto',
      tl: to,
      q: text,
    })}`, {
    mode: 'cors',
  })
  const json = await response.json()

  const translation = json[0]
    .map(part => part[0])
    .join('')

  console.log({
    [from]: text,
    [to]: translation,
  })

  return translation
}