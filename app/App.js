import { createRouter, AppFrame } from './router'
import routes from './routes'

export default createRouter(routes)(AppFrame)