import {observable} from 'mobx'
import fallback from './fallback'
import {firestore} from 'firebase'

let localCurrencyCode = null
let rates = null
let currencyCodes = observable([])

const leadingZero = (value, length = 2) => String(value).length < length
  ? leadingZero(`0${value}`, length)
  : value

export async function getRates(baseCurrencyCode = 'USD') {
  if (localCurrencyCode !== baseCurrencyCode) {
    localCurrencyCode = baseCurrencyCode

    try {
      const forex = firestore().doc(`forex/${baseCurrencyCode}`)
      const cache = (await forex.get())

      if (!cache.exists || !cache.data().timeExpires || Date.now() >= cache.data().timeExpires) {
        const response = await fetch(`https://api.fixer.io/latest?base=${baseCurrencyCode}`)
        const json = await response.json()
        rates = json.rates

        let baseDate
        if ((new Date()).getUTCHours() < 15) { /// 4PM CET = 3PM UTC
          baseDate = new Date()
        } else {
          baseDate = new Date(Date.now() + 24 * 60 * 60 * 1000)
        }

        const expiresString = `${baseDate.getUTCFullYear()}-${leadingZero(baseDate.getUTCMonth()+1)}-${leadingZero(baseDate.getUTCDate())}T16:00:00+01:00`
        const timeExpires = new Date(expiresString)

        forex.set({
          timeExpires,
          rates
        })
      } else {
        rates = cache.data().rates
      }
    } catch (error) {
      fallback.rates[fallback.base] = 1
      const baseRate = fallback.rates[baseCurrencyCode]

      rates = Object
        .entries(fallback.rates)
        .reduce((result, [to, rate]) => ({
          ...result,
          [to]: rate / baseRate,
        }), {})
    }
    currencyCodes.length = 0
    currencyCodes.push(...Object.keys(rates))
  }

  return rates
}

export const getCurrencyCodes = () => currencyCodes

export function tryConvertPrice(price) {
  if (!price || !rates) {
    return price
  }

  const { amount, toCurrencyCode } = price

  if (!rates[toCurrencyCode]) {
    return { amount, currencyCode: localCurrencyCode }
  }

  return {
    amount: amount * rates[toCurrencyCode],
    currencyCode: toCurrencyCode,
    original: price,
  }
}

export const tryConvertPriceFormatted = ({amount, toCurrencyCode}) =>
  amount
    .split('/')
    .map(value => value.trim())
    .map(amount => amount.replace(/,(\d\d\d)/g, '$1'))
    .map(parseFloat)
    .map(value => tryConvertPrice({amount: value, toCurrencyCode}))
